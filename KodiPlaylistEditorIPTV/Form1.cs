﻿//  MIT License
//  Copyright (c) 2018 github/isayso
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
//  files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy,
//  modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using PlaylistEditor.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static PlaylistEditor.ClassDataset;
using static PlaylistEditor.ClassHelp;

namespace PlaylistEditor
{
    public partial class Form1 : Form
    {
        private Stack<object[][]> undoStack = new Stack<object[][]>();
        private Stack<object[][]> redoStack = new Stack<object[][]>();

        private bool ignore = false;
        private CancellationTokenSource tokenSource;

        private player player;

        private bool isModified = false;

        private string fullRowContent = "",
                       _sort = "",
                       fileHeader = "#EXTM3U",  //for #EXTM3U tags
                       line;

        public static string tvhead1 = "pipe://ffmpeg -loglevel fatal -i ";
        public static string tvhead2 = " -vcodec copy -acodec copy -metadata service_provider=IPTV-EN" +
            " -mpegts_service_type advanced_codec_digital_hdtv -f mpegts pipe:1";



        public string path;
        public static string myCulture;

        private bool _isIt = true,
                     _found = false,
                     _isSingle = false,
                     _endofLoop = false; //loop of move to top finished

        private bool IsReferrerCol = false,
            IsAgentCol = false,
            IsInputStreamCol = false,
            IsManifestCol = false,
            IsManiConfigCol = false,
            IsLicenseCol = false,
            IsLicenseKeyCol = false,
            IsKodiPropCol = false;




        //duplicate select lists
        //private List<DataGridViewRow> dlist = new List<DataGridViewRow>();
        //private List<DataGridViewRow> dlist2 = new List<DataGridViewRow>();
        //private List<DataGridViewRow> clist = new List<DataGridViewRow>();
        //private List<DataGridViewRow> clist2 = new List<DataGridViewRow>();
        //private List<DataGridViewRow> clist3 = new List<DataGridViewRow>();
        public int _dlistAB = 0, _dlistERR = 0;

        private ObservableCollection<PaintDupCells> paintDupCells = new ObservableCollection<PaintDupCells>();

        private List<DataGridViewRow>[] colorList = new List<DataGridViewRow>[3];
        private List<DupList>[] duplicateList = new List<DupList>[2];

        private const int mActionHotKeyID = 1;  //var for key hook listener

        //zoom of fonts
        public float zoomf = 1F;

        private const float FONTSIZE = 9.163636F;

        private DataSet ds = new DataSet();
        private DataTable dt = new DataTable();
        private DataTable dt2; //= new DataTable();
      //  private DataRow dr;

        //  private string[] linktypes = new[] { "ht", "plugin", "rt", "ud", "mm" }; //Types of links in Column "Link"

        public List<string> columnNames1 = new List<string>();  //stores all used column names dt
        public List<string> columnNames2 = new List<string>();  //stores all used column names dt2

        public List<string> cNameArr = new List<string>();  //search box
        public int colswitch = 0;

        private Rectangle dragBoxFromMouseDown;
        private int rowIndexFromMouseDown;
        private int colIndexFromMouseDown;
        private int rowIndexOfItemUnderMouseToDrop;

        private bool IsDataGrid { get { return dataGridView1.RowCount > 0 ? true : false; } }
        private bool NoDataRow { get { return dataGridView1.Rows.Count == 0 ? true : false; } }
      //  private bool IsShiftKey { get { return (Control.ModifierKeys & Keys.Shift) == Keys.Shift; } }
        private bool IsControlKey { get { return (Control.ModifierKeys & Keys.Control) == Keys.Control; } }
        private bool IsLinkChecked { get { return button_check.BackColor == Color.LightSalmon ? true : false; } }
        private bool IsDupChecked { get { return button_dup.BackColor == Color.LightSalmon ? true : false; } }
        private bool IsCompareChecked { get { return btnTemplate.BackColor == Color.LightSalmon ? true : false; } }
        private bool IsTemplate { get { return tabControl1.TabPages.Contains(tabPage2) ? true : false; } }

        Encoding encoding;

        public Form1()
        {
            myCulture = Settings.Default.localize;
            if (string.IsNullOrEmpty(myCulture)) myCulture = "en-US";

            // myCulture = "ru-RU";

            Thread.CurrentThread.CurrentCulture = new CultureInfo(myCulture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(myCulture);

            m_ClassPlayer = new ClassPlayer();

            InitializeComponent();

            this.Text = String.Format("PlaylistEditor TV " + " v{0}", Assembly.GetExecutingAssembly().GetName().Version.ToString().Substring(0, 5));

#if DEBUG
            //  Clipboard.Clear();
            this.Text = String.Format("PlaylistEditor TV DEBUG" + " v{0}", Assembly.GetExecutingAssembly().GetName().Version.ToString().Substring(0, 7));
#endif

            if (Settings.Default.UpgradeRequired)
            {
                //Settings.Default.Reset();
                Settings.Default.Upgrade();
                Settings.Default.UpgradeRequired = false;
                Settings.Default.Save();
            }

            var spec_key = Settings.Default.specKey;  //for key listener
            var hotlabel = Settings.Default.hotkey;

            //Modifier keys codes: Alt = 1, Ctrl = 2, Shift = 4, Win = 8  must be added
            //   RegisterHotKey(this.Handle, mActionHotKeyID, 1, (int)Keys.Y);  //ALT-Y
            if (Settings.Default.hotkey_enable)
                NativeMethods.RegisterHotKey(this.Handle, mActionHotKeyID, spec_key, hotlabel);  //ALT-Y

            ClassCheckPlayer.SetPlayerPath();  //set Path of vlc or SMPlayer

#if DEBUG
            //if (!MyFileExists(smppath + "\\" + "smplayer.exe", 5000))  
            //{
            //    smppath = GetSMPPath();
            //}
#endif

            ///TVheadend
            tvhead1 = Settings.Default.tvhead1.Trim() + " ";
            tvhead2 = " " + Settings.Default.tvhead2.Trim();

            // check for external progs
            //check for vlc.exe
            //if (!MyFileExists(vlcpath + "\\" + "vlc.exe", 5000))  // vlcpath + "\\" + "vlc.exe";
            //{
            ////    vlcpath = GetVlcPath();
            //}

            plabel_Filename.Text = "";
            button_revert.Visible = false;

            //  dataGridView1.AllowUserToAddRows = true;

            dataGridView1.ShowCellToolTips = false;  //to show my own
            dataGridView1.DoubleBuffered(true);
            dataGridView2.DoubleBuffered(true);
            // dataGridView1.BringToFront();
            //    dataGridView1.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridView1.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
          //  dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;

            DataGridStyle(dataGridView1);
            DataGridStyle(dataGridView2);
            dataGridView2.ReadOnly = true;
            tabControl1.TabPages.Remove(tabPage2);
            tabControl1.TabPages.Remove(tabPage3);

#if !DEBUG  //template
            btnTemplate.Visible = false;
            
#endif
            // context menu 3 options
            cm3Scrollbar.Checked = Settings.Default.scrollbar;
            cm3EditF2.Checked = Settings.Default.F2_edit;

            if (Settings.Default.F2_edit)
                dataGridView1.EditMode = DataGridViewEditMode.EditProgrammatically;

            //  SetHorizontalBar(); //1

            this.SizeChanged += Form1_SizeChanged;
            // tabControl1.SelectedIndexChanged += TabControl1_SelectedIndexChanged;

            colorList[0] = new List<DataGridViewRow>();
            colorList[1] = new List<DataGridViewRow>();
            colorList[2] = new List<DataGridViewRow>();
            duplicateList[0] = new List<DupList>();
            duplicateList[1] = new List<DupList>();

            dt.TableName = "IPTV";
            dataGridView1.DataSource = dt;


            //command line arguments [1]
            string[] args = Environment.GetCommandLineArgs();  //issue #6 CB

            if (args.Length > 1) //drag drop
            {
                plabel_Filename.Text = args[1];
                ImportDataFile(args[1]);
                button_revert.Visible = true;
            }

        }

        private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Form1_SizeChanged(sender, e);
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            ///autoscale of DataGridView width
            foreach (DataGridView dgv in this.GetAllChildren().OfType<DataGridView>())
            {
                int VisibleColCount = 0, VScrollBarWidth = 0;

                if (dgv.RowCount > 0)
                {
                    if (dgv.Controls.OfType<VScrollBar>().First().Visible)
                        VScrollBarWidth = SystemInformation.VerticalScrollBarWidth;
                    
                    foreach (DataGridViewColumn c in dgv.Columns)
                        if (dgv.Columns[c.HeaderText].Visible) VisibleColCount++;
                    
                    foreach (DataGridViewColumn c in dgv.Columns)
                    {
                        dgv.Columns[c.HeaderText].AutoSizeMode = DataGridViewAutoSizeColumnMode.None; //1
                                                                                                                //  dataGridView1.Columns[c.ColumnName].Width = 60;
                        dgv.Columns[c.HeaderText].Width =
                            (dgv.Width - dgv.RowHeadersWidth - VScrollBarWidth)
                            / VisibleColCount/*dt.Columns.Count*/;

                    }

                }
            }
        }

        private void SetHorizontalBar()
        {
            if (Settings.Default.scrollbar)
            {
                // dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
              //  dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells; //1
                // dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;

            }
            else
            {
              //  dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;  //1
              //  dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.ColumnHeader;  //1
                // dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
            }

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            if (Settings.Default.filestart && !Settings.Default.nostart && Environment.GetCommandLineArgs() == null)  //nostart for ctrl-N  //issue #6 CB
            {
                plabel_Filename.Text = Settings.Default.startfile;
                //check if path exist
                if (MyFileExists(plabel_Filename.Text, 5000))
                {
                   // NewDataTable();
                    ImportDataFile(plabel_Filename.Text);
                    button_revert.Visible = true;

                    if (Settings.Default.autoplayer)
                    {
                        button_vlc.PerformClick();
                    }
                }
            }

            if (Settings.Default.F2Size.Width == 0 || Settings.Default.F2Size.Height == 0
                || Settings.Default.nostart)
            {
                // first start
                this.Size = new Size(1140, 422);
            }
            else
            {
                if (Settings.Default.ZoomFactor != 0) ZoomGrid(Settings.Default.ZoomFactor);
                this.Location = Settings.Default.F2Location;
                this.Size = Settings.Default.F2Size;
            }

            button_kodi.Enabled = Settings.Default.enableKodi;


           // if (IsDataGrid)
           //     dataGridView1.ContextMenuStrip = contextMenuStrip1;

            Settings.Default.nostart = false;
            Settings.Default.Save();

        }

        /// <summary>
        /// listener to hotkey for import of links from clipboard
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x0312 && m.WParam.ToInt32() == mActionHotKeyID)
            {
                NotificationBox.Show(Mess.List_import, 1500, NotificationMsg.DONE);

                button_import.PerformClick();
            }
            base.WndProc(ref m);
        }

        /// <summary>
        /// set contextmenu to all column headers
        /// </summary>
        //private void SetHeaderContextMenu()
        //{
        //    return;
        //    foreach (DataGridViewColumn column in dataGridView1.Columns)
        //    {
        //        column.HeaderCell.ContextMenuStrip = contextMenuStrip3;
        //        // column.SortMode = DataGridViewColumnSortMode.NotSortable;
        //    }
        //}


        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {

            if (IsControlKey)
            {
                switch (e.KeyCode)
                {
                    case Keys.B:
                        MoveLineBottom();
                        break;

                    case Keys.C:
                        if (dataGridView1.SelectedRows.Count > 0)  //Full row
                        {
                            contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = true;
                            CopyFullRow();
                        }
                        else toolStripCopy.PerformClick();
                        break;

                    case Keys.V:
                        contextMenuStrip1.Items["toolStripPaste"].Enabled = true;
                        toolStripPaste.PerformClick();   //#35
                        break;

                    case Keys.I:
                        //if (dataGridView1.SelectedRows.Count > 0 || dataGridView1.Rows.Count == 0
                        //    || (string.IsNullOrEmpty(fullRowContent) && CheckClipboard()))
                        //    contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = true;  //paste add  ????

                        pasteRowMenuItem.PerformClick();
                        break;

                    case Keys.X:
                        if (dataGridView1.SelectedRows.Count > 0)
                        {
                           // contextMenuStrip1.Items["cutRowMenuItem"].Enabled = true;
                            contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = true;
                            cutRowMenuItem.PerformClick();
                        }
                        break;

                    case Keys.Z:
                        UndoButton.PerformClick();
                        break;

                    case Keys.T:  //move line to top
                        MoveLineTop();
                        break;

                    case Keys.F:
                        button_search.PerformClick();
                        break;

                    case Keys.N:
                        Settings.Default.nostart = true;
                        Settings.Default.Save();
                        var deffile = new ProcessStartInfo(Application.ExecutablePath);
                        Process.Start(deffile);
                        break;

                    case Keys.P:
                        playToolStripMenuItem.PerformClick();
                        break;

                    case Keys.S:
                        SaveFile(true);
                        break;

                    case Keys.Add:    //change font size
                        zoomf += 0.1F;
                        ZoomGrid(zoomf);
                        break;

                    case Keys.Oemplus:      //change font size
                        zoomf += 0.1F;
                        ZoomGrid(zoomf);
                        break;

                    case Keys.Subtract:    //change font size
                        zoomf -= 0.1F;
                        ZoomGrid(zoomf);
                        break;

                    case Keys.OemMinus:     //change font size
                        zoomf -= 0.1F;
                        ZoomGrid(zoomf);
                        break;

                    case Keys.D1:
                        MoveLine(-1);
                        break;

                    case Keys.D2:
                        MoveLine(1);
                        break;
                }
            }
            if (e.KeyCode == Keys.Delete && dataGridView1.IsCurrentCellInEditMode == false)
            {
                //todo test if tabpage1 is active otherwise ignore
                button_delLine.PerformClick();

                e.Handled = true;
                e.SuppressKeyPress = true;

            }

            if (e.KeyCode == Keys.F2)
            {
                _endofLoop = true;
                dataGridView1.BeginEdit(true);
            }

            if (e.KeyCode == Keys.Escape && textBox_find.Visible == true)
            {
                button_clearfind.PerformClick();
                button_search.PerformClick();

                e.Handled = true;
                e.SuppressKeyPress = true;

            }
            // e.Handled = true;

            //}
        }

        /// <summary>
        /// change font size of datagrid
        /// </summary>
        /// <param name="f">change factor float</param>
        public void ZoomGrid(float f)
        {
            dataGridView1.Font = new Font(dataGridView1.Font.FontFamily,
                                         FONTSIZE * f, dataGridView1.Font.Style);

            Settings.Default.ZoomFactor = f;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isModified && IsDataGrid)
            {
                DialogResult dialogSave = MessageBox.Show(Mess.Do_you_want_to_save_your_current_playlist, Mess.Save_Playlist,
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dialogSave == DialogResult.Yes)
                {
                    button_save.PerformClick();
                    isModified = false;
                }
                if (dialogSave == DialogResult.Cancel) e.Cancel = true;
            }

            NativeMethods.UnregisterHotKey(this.Handle, mActionHotKeyID);

            //Settings.Default.F2Location = this.Location;
            //Settings.Default.F2Size = this.Size;

            if (this.WindowState == FormWindowState.Normal)
            {
                // save location and size if the state is normal
                Properties.Settings.Default.F2Location = this.Location;
                Properties.Settings.Default.F2Size = this.Size;
            }
            else
            {
                // save the RestoreBounds if the form is minimized or maximized!
                Properties.Settings.Default.F2Location = this.RestoreBounds.Location;
                Properties.Settings.Default.F2Size = this.RestoreBounds.Size;
            }

            ///reset duplicate combobox
            Settings.Default.colDupli2 = 0;  

            Settings.Default.Save();
        }

        #region menu buttons

        private void button_search_Click(object sender, EventArgs e)
        {
            cNameArr.Clear();

            foreach (DataColumn c in dt.Columns)
            {
                cNameArr.Add(c.ColumnName);
            }

            cNameArr.Add("All");

            panel_Find.Show();
            panel_Find.BringToFront();

            if (Settings.Default.findresult == 0) 
                lblRowCheck.Text = "Row";
            else lblRowCheck.Text = "Cell";

            lblColCheck.Text = cNameArr[cNameArr.Count - 1];
            colswitch = cNameArr.Count - 1;

            if (_isIt)
            {
                _isIt = !_isIt;
                panel_Find.Show();
                this.ActiveControl = textBox_find;
            }
            else  //close textbox_find
            {
                _isIt = !_isIt;
                panel_Find.Hide();

            }
        }

        private void label_click(object sender, EventArgs e)
        {
            int playswitch = Settings.Default.findresult;
        //    int colswitch = Settings.Default.colSearch;  //TODO
            //int colswitch = 1;  //TODO

            Label obj = sender as Label;

            if (obj.Name == "lblRowCheck")
            {
                switch (playswitch)
                {
                    case 0:
                        lblRowCheck.Text = "Cell";
                        playswitch = 1;
                        break;

                    case 1:
                        lblRowCheck.Text = "Row";
                        playswitch = 0;
                        break;
                }
                Settings.Default.findresult = playswitch;

                textBox_find_TextChange(sender, e);
            }

            if (obj.Name == "lblColCheck")
            {
                colswitch++; 
                if (colswitch >= cNameArr.Count) colswitch = 0;

                lblColCheck.Text = cNameArr[colswitch];
           //     Settings.Default.colSearch = colswitch;

                textBox_find_TextChange(sender, e);
            }
        }

        private void OpenFile(bool append)
        {

            if (isModified && IsDataGrid)
            {
                DialogResult dialogSave = MessageBox.Show(Mess.Do_you_want_to_save_your_current_playlist,
                Mess.Save_Playlist, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dialogSave == DialogResult.Yes)
                {
                    button_save.PerformClick();
                    isModified = false;
                }
                if (dialogSave == DialogResult.Cancel) return;
            }

            Cursor.Current = Cursors.WaitCursor;

            var filename = ClassOpenFile.OpenFileUI(Settings.Default.openpath, append);

            if (filename != null)
            {
                SaveButtonUI(NeedSave.Reset);
                ImportDataFile(filename, append);
                button_revert.Visible = true;

                fillPlayer(); //send list to player

                if (IsLinkChecked) button_check.PerformClick();
                if (IsDupChecked) button_dup.PerformClick();

            }
            Cursor.Current = Cursors.Default;

        }
        private void button_open_Click(object sender, EventArgs e)
        {
            if (NoDataRow)
            {
                OpenFile(false);
                return;
            }
            cm7_Open.Show(button_open, MENUPOINT);
           // cm7_Open.Show(button_open, button_open.PointToClient(Cursor.Position));
        }

        private void button_Info_Click(object sender, EventArgs e)
        {
            using (AboutBox1 a = new AboutBox1())
            {
                a.ShowDialog();   //  ShowDialog gets focus, Show not
                                  //centre position on Infoform
            }
        }

        private void button_settings_Click(object sender, EventArgs e)
        {

            using (settings s = new settings(columnNames1))
            {
                s.ShowDialog();

                if (Settings.Default.findresult == 0) lblRowCheck.Text = "Row";
                else lblRowCheck.Text = "Cell";

            }

            //scrollbar change
            cm3Scrollbar.Checked = Settings.Default.scrollbar;

            cm3EditF2.Checked = Settings.Default.F2_edit;

            if (Settings.Default.enableKodi) button_kodi.Enabled = true;
            else button_kodi.Enabled = false;

        }


        /// <summary>
        /// import of playlist entries
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="append">false/true for append</param>

        public void ImportDataFile(string filename, bool append = false)
        {
            if (filename == null) return;

            Cursor.Current = Cursors.WaitCursor;
            string fullTxt = "";
            string[] fileRows;

            encoding = ClassOpenFile.GetEncoding(filename);

            if (encoding != null ) lblEncoding.Text = encoding.BodyName.Substring(0, 5).ToUpper();
            else lblEncoding.Text = "";

            fullTxt = File.ReadAllText(filename, encoding);

            fileRows = fullTxt.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            if (fileRows[0].StartsWith("#EXTM3U"))
            {
                fileHeader = fileRows[0];
            }
            else if (fileRows[0].StartsWith("#EXTCPlayListM3U::M3U"))
            {
                MessageBox.Show(Mess.File_has_wrong_format_or_does_not_exist_);
                Cursor.Current = Cursors.Default;

                return;

            }


            dataGridView1.SuspendLayout();

            if (!append)  //append false 
            {
                dt.Clear();  // row clear
                dt.Columns.Clear();  // col clear
//#if DEBUG
//                this.Text = String.Format("PlaylistEditor TV DEBUG" + " v{0}", Assembly.GetExecutingAssembly().GetName().Version.ToString().Substring(0, 7)
//                    + "  :"+ encoding.ToString().Split('.').Last().Replace("Encoding", ""));

//#endif

                plabel_Filename.Text = filename;
            }

            CreateDataTable(SeekFileElements(fullTxt));

            columnNames1 = ClassImportData.GetColumnNames(dt);

            dt = ClassImportData.ImportData(dt, fileRows);


            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ResumeLayout();

            if (dt.Rows.Count == 0)
            {
                MessageBox.Show(Mess.Wrong_data_structure, Mess.File_Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor.Current = Cursors.Default;

                return;
            }

            dataGridView1.Rows[0].Selected = true;

            //  dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);  //1

            Form1_SizeChanged(null, null);

            label_central.SendToBack();

            Cursor.Current = Cursors.Default;
        }


        private void button_delLine_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                // foreach (DataGridViewRow row in dataGridView1.InvSelectedRows())
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    dt.Rows.RemoveAt(row.Index);
                }
                SaveButtonUI();
            }
            else  //delete cells only
            {
                Int32 selectedCellCount = dataGridView1.GetCellCount(DataGridViewElementStates.Selected);
                if (selectedCellCount > 0)
                {
                    for (int i = 0; i < selectedCellCount; i++)
                    {
                        dataGridView1.Rows[dataGridView1.SelectedCells[i].RowIndex]
                            .Cells[dataGridView1.SelectedCells[i].ColumnIndex].Value = string.Empty;
                    }
                }

                SaveButtonUI();
            }
        }

        private void SaveFile(bool save_now = false)
        {
            Cursor.Current = Cursors.WaitCursor;

          //  saveFileDialog1.FileName = plabel_Filename.Text;

             List<ColList> test = SetColumnFlags(); //set flags for special lines


            if (Test4HiddenColumns(test))
            {
                DialogResult dialogHidden = MessageBox.Show(Mess.Hidden_Columns_will_not_be_saved, Mess.Proceed,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dialogHidden == DialogResult.Yes)
                {
                    isModified = false;
                }
                else if (dialogHidden == DialogResult.No) return;

            }


            if (save_now && !string.IsNullOrEmpty(plabel_Filename.Text)
                && MyDirectoryExists(Path.GetDirectoryName(plabel_Filename.Text), 4000))
            {
                saveFileDialog1.FileName = plabel_Filename.Text;

                ClassWriteData.Write(dt, test, saveFileDialog1.FileName, fileHeader, encoding);

                //undoStack.Clear(); redoStack.Clear(); ShowReUnDo(0); toSave(false);
                SaveButtonUI(NeedSave.Reset);
                button_revert.Visible = true;

                NotificationBox.Show(this, Mess.Playlist_Saved, 1500, NotificationMsg.OK, Position.Parent);
            }
            else  //open file dialog
            {
                using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
                { 
                    saveFileDialog1.FileName = plabel_Filename.Text;
                    saveFileDialog1.Filter = "Text (*.m3u)|*.m3u|Text UTF-8|*.m3u8";
                    saveFileDialog1.DefaultExt = "m3u";
                    saveFileDialog1.AddExtension = true;

                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)  //open file dialog
                    {
                        ClassWriteData.Write(dt, test, saveFileDialog1.FileName, fileHeader, encoding);
                       
                       // if (saveFileDialog1.FilterIndex < 3)
                            plabel_Filename.Text = saveFileDialog1.FileName;

                        if (saveFileDialog1.FilterIndex == 2)
                        {
                            encoding = ClassOpenFile.GetEncoding(saveFileDialog1.FileName);

                            if (encoding != null) lblEncoding.Text = encoding.BodyName.Substring(0,5).ToUpper();
                            else lblEncoding.Text = "";

                        }

                    }


                }
                ;


                //undoStack.Clear(); redoStack.Clear(); ShowReUnDo(0); toSave(false);
                SaveButtonUI(NeedSave.Reset);
                button_revert.Visible = true;
                Cursor.Current = Cursors.Default;
            }

        }

        private void button_save_Click(object sender, EventArgs e)
        {
            if (NoDataRow) return;
            cm6_Save.Show(button_save, MENUPOINT);
           // cm6_Save.Show(button_save,button_save.PointToClient(Cursor.Position));
        }

        private bool TestHiddenColumns()
        {
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                if (dataGridView1.Columns[dataGridView1.Columns[i].HeaderText].Visible == false) 
                    return true;
            }
            return false;
        }
        private bool Test4HiddenColumns(List<ColList> colList)
        {
            foreach (var item in colList)
            {
                if (!item.Visible) return true;
            }
            return false;
        }

        private void button_moveUp_Click(object sender, EventArgs e)
        {
            if (IsControlKey)
            {
                MoveLineTop();
            }
            else
            {
                MoveLine(-1);
            }
        }

        private void button_moveDown_Click(object sender, EventArgs e)
        {
            if (IsControlKey)
            {
                MoveLineBottom();
            }
            else
            {
                MoveLine(1);
            }
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            DataRow dr = dt.NewRow();

            if (NoDataRow && dataGridView1.ColumnCount > 0) //delete all
            {
                dt.Clear();
                dt.Columns.Clear();
                SaveButtonUI(NeedSave.No);
                plabel_Filename.Text = "";
                button_revert.Visible = false;
            }

            if (IsDataGrid)
            {
                if (dataGridView1.SelectedRows.Count == 0) return;

                int a = dataGridView1.SelectedCells[0].RowIndex;  // row index in a datatable

                DataRow newBlankRow = dt.NewRow();
                dt.Rows.InsertAt(newBlankRow, a);

            }
            else  //Grid empty
            {
                dt.TableName = "IPTV";

                dt.Columns.Add("group-title"); dt.Columns.Add("tvg-logo");
                dt.Columns.Add("Name2"); dt.Columns.Add("Link");
                dt.Rows.InsertAt(dr, 0);

                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;

               // SetHeaderContextMenu();
            }
            label_central.SendToBack();

            SaveButtonUI();
        }

        private void button_vlc_Click(object sender, EventArgs e)
        {
            if (NoDataRow) return;

            string vlclink = dataGridView1.CurrentRow.Cells["Link"].Value.ToString();
            //   if (!vlclink.StartsWith("http") && !vlclink.StartsWith("rtmp")) return; //issue #32

            if (Properties.Settings.Default.vlcplayer)
            {
                if (string.IsNullOrEmpty(vlcpath))
                {
                    vlcpath = ClassCheckPlayer.GetVlcPath();
                    if (string.IsNullOrEmpty(vlcpath))
                        NotificationBox.Show(this, Mess.VLC_player_not_found, 3000, NotificationMsg.ERROR, Position.Parent);

                    return;
                }

            }
            else if (Properties.Settings.Default.smplayer)
            {
                if (string.IsNullOrEmpty(smppath))
                {
                    smppath = ClassCheckPlayer.GetSMPPath();
                    if (string.IsNullOrEmpty(smppath))
                        NotificationBox.Show(this, Mess.SM_Player_not_found, 3000, NotificationMsg.ERROR, Position.Parent);

                    return;
                }

            }

            if (vlclink.StartsWith("plugin"))
            {
                NotificationBox.Show(this, Mess.Plugin_links_only_work_in_Kodi, 3000, NotificationMsg.ERROR, Position.Parent);

                return;  //#18
            }
            else if (vlclink.StartsWith("rtmp"))
            {
                NotificationBox.Show(this, Mess.Plugin_links_only_work_in_Kodi, 3000, NotificationMsg.ERROR, Position.Parent);

                return;  //#61
            }
            else if (vlclink.Contains("|User"))
            {
                NotificationBox.Show(this, Mess.User_Agent_links_only_work_in_Kodi, 3000, NotificationMsg.ERROR, Position.Parent);

                return;  //#18
            }


            if (player == null)
            {
                CreatePlayer();
            }
            else
            {
                fillPlayer();
            }

            if (IsDataGrid)
            {
                player.comboBox1.SelectedIndex = dataGridView1.CurrentRow.Index;  //trigger eventHandler
            }
        }


        private void button_del_all_Click(object sender, EventArgs e)
        {
            if (IsLinkChecked) button_check.PerformClick();

            if (IsDataGrid)
            {
                switch (MessageBox.Show(Mess.Delete_List, Mess.Warning, MessageBoxButtons.YesNo, MessageBoxIcon.None))
                {
                    case DialogResult.Yes:

                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            string header = dt.Columns[j].ToString();
                            dataGridView1.Columns[header].Visible = false;
                        }

                        dt.Clear();
                        dt.Columns.Clear();
                        //undoStack.Clear(); redoStack.Clear(); ShowReUnDo(0); toSave(false);
                        SaveButtonUI(NeedSave.Reset);
                        plabel_Filename.Text = "";
                        button_revert.Visible = false;
                        label_central.BringToFront();
                       // dataGridView1.ContextMenuStrip = contextMenuStrip1;
                        break;

                    case DialogResult.No:

                        break;
                }
            }
        }

        private void button_revert_Click(object sender, EventArgs e)
        {
            //message box -> delete all -> open filename
            if (plabel_Filename.Text == "") return;

            switch (MessageBox.Show(Mess.Reload_File, Mess.Warning, MessageBoxButtons.YesNo, MessageBoxIcon.None))
            {
                case DialogResult.Yes:
                    ImportDataFile(plabel_Filename.Text);
                    //undoStack.Clear(); redoStack.Clear(); ShowReUnDo(0); toSave(false);
                    SaveButtonUI(NeedSave.Reset);
                    if (IsLinkChecked) button_check.PerformClick();
                    if (IsDupChecked) button_dup.PerformClick();

                    break;

                case DialogResult.No:

                    break;
            }
        }

        private void AddHashColum(string scolID)
        {
            if (!dt.Columns.Contains("hash"))
            dt.Columns.Add("hash", typeof(int));

            for (int row = 0; row < dataGridView1.Rows.Count; row++)
            {
               //  int cell1_H = dt.Rows[row][scolID].GetHashCode();
                 int cell1_H = dt.Rows[row][scolID].GetHashCode();
                dataGridView1.Rows[row].Cells["hash"].Value = cell1_H;
            }
        }

        private void FindDuplicates()
        {
            int colD; string scolID = "";
            try
            {
                colD = Settings.Default.colDupli2;
                scolID = columnNames1[colD];
            }
            catch
            {
                colD = 0;
                scolID = columnNames1[colD];
            }

            //      AddHashColum(scolID);

            if (colorList[0].Count > 0)
            {
                clearCellColor(dataGridView1);
                colorList[0].Clear(); colorList[1].Clear();
            }
            if (duplicateList[0].Count > 0)
            {
                clearCellColor(dataGridView1);
                duplicateList[0].Clear(); duplicateList[1].Clear();
            }

            dataGridView1.ClearSelection();
            int rowcount = dataGridView1.RowCount;

            if (rowcount > 0)
            {
                Cursor.Current = Cursors.WaitCursor;

                for (int row = 0; row < rowcount; row++)
                {
                    string mastercell = dt.Rows[row].Field<string>(scolID);
                    if (!string.IsNullOrEmpty(mastercell))
                    {
                        for (int a = 1; a < rowcount - row; a++)
                        {
                           // string slavecell = dt.Rows[row + a].Field<string>(scolID);
                            if (mastercell == dt.Rows[row + a].Field<string>(scolID))  //compare dt 2 times faster
                            {
                                dataGridView1.Rows[row + a].Selected = true;

                                //duplicateList[0].Add(new DupList { CellName = mastercell, ColName = scolID, Master = true });
                                //duplicateList[1].Add(new DupList { CellName = slavecell, ColName = scolID, Master = false });
                               
                                colorList[0].Add(dataGridView1.Rows[row]);

                                colorList[1].Add(dataGridView1.Rows[row + a]);

                            }
                        }
                    }
                }
                Cursor.Current = Cursors.Default;
            }


            DefineCellColors();

            _dlistAB = 2;
            SwitchDupLists();

            _dlistAB = 0;
            //  dt.Columns.Remove("hash");

            if (colorList[0].Count == 0) NotificationBox.Show(Mess.No_duplicates, 2000, NotificationMsg.DONE);


            //if (IsShiftKey)
            //{
            //    button_delLine.PerformClick();
            //}

        }
        private void button_dup_Click(object sender, EventArgs e)
        {
            if (NoDataRow) return;
            if (IsLinkChecked) button_check.PerformClick();

            if (IsDupChecked)
            {
                button_dup.BackColor = Color.MidnightBlue;
                clearCellColor(dataGridView1);

            }
            else
            {
              //  button_dup.BackColor = Color.LightSalmon;
              //  cm5_Duplicate.Show(button_dup, button_dup.PointToClient(Cursor.Position));
                cm5_Duplicate.Show(button_dup, new Point(0,41));
            }
            //_ = button_dup.BackColor == Color.MidnightBlue 
            //    ? button_dup.BackColor = Color.LightSalmon : button_dup.BackColor = Color.MidnightBlue;

        }

        private void clearCellColor(DataGridView dgv)
        {
            dgv.SuspendLayout();

            foreach (DataGridViewRow row in dgv.Rows)
                foreach (DataGridViewCell cell in row.Cells)
                    cell.Style.BackColor = Color.Empty;
            dgv.ResumeLayout();
            //dgv.ClearSelection()
        }
        private void button_clearfind_Click(object sender, EventArgs e)
        {
            textBox_find.Clear();
            textBox_find.Focus();
        }

        private async void button_check_Click(object sender, EventArgs e)
        {
            if (NoDataRow) return;

            if (IsDupChecked)
            {
                button_dup.PerformClick();
            }

            bool _altpressed = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;


            if (checkList.Count > 0 && !IsLinkChecked && !_altpressed
                && Int32.TryParse(checkList[0].Url, out int xx) && xx >= dataGridView1.Rows.Count)
            {
                PaintRows();
                button_check.BackColor = Color.LightSalmon;
                return;
            }

            if (!IsLinkChecked)
            {
                button_check.BackColor = Color.LightSalmon;
            }
            else if (IsLinkChecked)
            {
                button_check.BackColor = Color.MidnightBlue;
                clearCellColor(dataGridView1);
                return;
            }

            if (CheckINetConn("http://www.google.com") != 0)
            {
                MessageBox.Show(Mess.No_internet_connection_found);
                return;
            }

            dataGridView1.ClearSelection();

            button_check.Enabled = false;

            if (dataGridView1.Rows.Count > 0)
            {
                clearCellColor(dataGridView1);


                popup popup = new popup();

                popup.FormClosed += new FormClosedEventHandler(FormP_Closed);

                var x = Location.X + (Width - popup.Width) / 2;
                var y = Location.Y + (Height - popup.Height) / 2;
                popup.Location = new Point(Math.Max(x, 0), Math.Max(y, 0));
                popup.StartPosition = FormStartPosition.Manual;
                popup.Owner = this;  //child over parent

                popup.Show();

                Progress<string> progress = new Progress<string>();
                progress.ProgressChanged += (_, text) =>
                    popup.updateProgressBar(text);

                tokenSource = new CancellationTokenSource();
                var token = tokenSource.Token;

                //run check loop
                await RunStreamCheck2(token, progress);

                popup.Close();

                tokenSource.Cancel();
                tokenSource.Dispose();
                tokenSource = null;
            }

            UseWaitCursor = false;
            Cursor.Current = Cursors.Default;
            //  MessageBox.Show(" Check done");
            PaintRows();

            button_check.Enabled = true;
        }

        private void PaintRows()  //only linkcheck
        {

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                for (int j = 1; j < checkList.Count; j++)
                {
                    if (checkList[j].ErrorCode != 0)
                    {
                        string sLink = dt.Rows[i].Field<string>("Link");
                        string qLink = checkList[j].Url;

                        if (sLink.StartsWith("pipe")) qLink = tvhead1 + qLink + tvhead2;

                        if (sLink.Equals(qLink))
                       // if (dt.Rows[i].Field<string>("Link") == checkList[j].Url)
                        {
                            for (int k = 0; k < dataGridView1.ColumnCount; k++)
                            {
                                switch (checkList[j].ErrorCode)
                                {
                                    case 0:
                                        dataGridView1.Rows[i].Cells[k].Style.BackColor = Color.Empty;
                                        break;

                                    case 403:
                                    case 404:
                                        dataGridView1.Rows[i].Cells[k].Style.BackColor = Settings.Default.Error403;
                                        break;

                                    case 410:
                                        dataGridView1.Rows[i].Cells[k].Style.BackColor = SystemColors.InactiveCaption; //  Color.LightGray;
                                        break;

                                    case int _: // n when (n != 0 && n != 403 && n != 410):
                                        dataGridView1.Rows[i].Cells[k].Style.BackColor = Color.LightSalmon;
                                        break;
                                }
                            }
                            break;
                        }

                    }                }
            }
        }

#endregion menu buttons

        #region context menu

        private async void playToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0) return;

            dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Selected = true;
            string jLink = dataGridView1.CurrentRow.Cells["Link"].Value.ToString();

            //json string Kodi
            jLink = "{ \"jsonrpc\":\"2.0\",\"method\":\"Player.Open\",\"params\":{ \"item\":{ \"file\":\"" + jLink + "\"} },\"id\":0}";

            await ClassKodi.RunOnKodi(jLink);
        }

        /// <summary>
        /// copy full row
        /// </summary>
        private void CopyFullRow()
        {
            try
            {
                ///get full rows from datagrid
                string rows4copy = ClassCopyPaste.GetRows(dataGridView1, dt).ToString();

                ///copy to Clipboard
                Clipboard.SetText(rows4copy);
                fullRowContent = rows4copy;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                MessageBox.Show(Mess.The_Clipboard_could_not_be_accessed__Please_try_again);
                Clipboard.Clear();
            }



#if DEBUG
            Console.WriteLine("Copy: " + Clipboard.GetText());
#endif
        }

        private void pasteRowMenuItem_Click(object sender, EventArgs e)  //CTRL-I
        {
            // bool _dtEmpty = false;
            if (!CheckClipboard()) return;

            ///option 1: empty grid after crtl-N
            if (dataGridView1.RowCount == 0 && dataGridView1.ColumnCount == 0)  //after ctrl-N
            {
                PasteFullRow();
                label_central.SendToBack();
                return;
            }

            DataObject o = (DataObject)Clipboard.GetDataObject();
            string clipText = o.GetData(DataFormats.UnicodeText).ToString();

            ///option 2: insert
            PasteMethod(clipText, true);


#if DEBUG
            Console.WriteLine(Clipboard.GetText());
#endif

        }

        /// <summary>
        /// paste full rows
        /// </summary>
        /// <param name="clipText">clipboard content</param>
        /// <param name="insert">insert/overwrite</param>
        /// <param name="emptyGrid">new table</param>
        private void PasteMethod(string clipText, bool insert, bool emptyGrid = false)
        {
            int a = 0;
            DataRow dr = null;


            try
            {
                CreateDataTable(SeekFileElements(clipText));
               
                columnNames1 = ClassImportData.GetColumnNames(dt);


                fullRowContent = clipText.RemoveFirstLines(1);  //remove header info line 0

                if (!emptyGrid) a = dataGridView1.SelectedCells[0].RowIndex; ;  //start row to copy

                string[] pastedRows = Regex.Split(fullRowContent.TrimEnd("\r\n".ToCharArray()), "\r\n");

                foreach (string pastedRow in pastedRows)
                {
                    string[] pastedRowCells = pastedRow.Split(new char[] { '\t' });

                    dr = dt.NewRow();

                    for (int i = 0; i < pastedRowCells.Length; i++)
                    {
                        dr[i] = pastedRowCells[i];
                    }
                    if (emptyGrid)
                    {
                        dt.Rows.Add(dr);

                    }
                    else
                    {
                        if (!insert) dt.Rows.RemoveAt(a);       //overwrite
                        dt.Rows.InsertAt(dr, a);
                        a++;

                    }

                }

                SaveButtonUI();

            }
            catch (Exception ex)
            {
                MessageBox.Show(Mess.Paste_operation_failed + ex.Message, Mess.Copy_Paste, MessageBoxButtons.OK, MessageBoxIcon.None);
            }

        }



        /// <summary>
        /// paste fullrow from clipboard with overwrite
        /// </summary>
        private void PasteFullRow()
        {

            DataObject o = (DataObject)Clipboard.GetDataObject();
            string clipText = o.GetData(DataFormats.UnicodeText).ToString();


            ///option 1: empty grid after crtl-N
            if (dataGridView1.RowCount == 0 && dataGridView1.ColumnCount == 0) //after ctrl-N
            {
                dataGridView1.DataSource = dt;
                label_central.SendToBack();

                PasteMethod(clipText, false, true);
            }

            ///option 2: with overwrite of existing cells
            else
            {
                PasteMethod(clipText, false);

            }

        }

        private void cutRowMenuItem_Click(object sender, EventArgs e)   //CTRL-X
        {
            if (dataGridView1.CurrentCell.Value != null && dataGridView1.GetCellCount(DataGridViewElementStates.Selected) > 0)
            {
                dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Selected = true;

                try
                {
                    CopyFullRow();

                    button_delLine.PerformClick();
#if DEBUG
                    Console.WriteLine(Clipboard.GetText());
#endif
                    //del line
                    foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                    {
                        int selectedRow = dataGridView1.SelectedRows[0].Index;

                        dt.Rows.RemoveAt(selectedRow);
                    }
                }
                catch (System.Runtime.InteropServices.ExternalException)
                {
                    MessageBox.Show(Mess.The_Clipboard_could_not_be_accessed__Please_try_again);
                    Clipboard.Clear();
                }
            }
        }

        private void toolStripCopy_Click(object sender, EventArgs e) //  CTRL-C
        {
            if (dataGridView1.SelectedRows.Count > 0)  //Full rows
            {
                contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = true;

                CopyFullRow();
                return;
            }

            //check selection range and set active cell to min values

            Int32 selectedCellCount = dataGridView1.GetCellCount(DataGridViewElementStates.Selected);
            if (selectedCellCount > 1)
            {
                int minRow = dataGridView1.CurrentCell.RowIndex;
                int minCol = dataGridView1.CurrentCell.ColumnIndex;
                int maxRow = 0, maxCol = 0;
                int x, y;   //rows, columns
                int[] array = Array.Empty<int>();
                int j = 0;

                for (int i = 0; i < selectedCellCount; i++)
                {
                    x = dataGridView1.SelectedCells[i].RowIndex;
                    if (x < minRow) minRow = x;
                    if (x > maxRow) maxRow = x;

                    y = dataGridView1.SelectedCells[i].ColumnIndex;
                    if (y < minCol) minCol = y;
                    if (y > maxCol) maxCol = y;

                    Array.Resize(ref array, array.Length + 2);

                    array[i + j] = x;
                    array[i + 1 + j] = y;
                    j += 1;
                }

                //set active cell
                dataGridView1.CurrentCell = dataGridView1.Rows[minRow].Cells[minCol];

                for (int i = 0; i <= array.Length - 2; i += 2)
                {
                    x = array[i];

                    y = array[i + 1];

                    dataGridView1.Rows[x].Cells[y].Selected = true;
                    //for manual clipboard
                    // fullCopyContent += dataGridView1.Rows[x].Cells[y].Value.ToString();
                }

                StringBuilder cpString = new StringBuilder();

                for (int i = minRow; i <= maxRow; i++)
                {
                    for (int k = minCol; k <= maxCol; k++)
                    {
                        cpString.Append(dataGridView1.Rows[i].Cells[k].Value.ToString().Trim());
                        if (k < maxCol) cpString.Append("\t");
                    }
                    cpString.Append("\r\n");
                }

                Clipboard.SetText(cpString.ToString());
            }
            else if (selectedCellCount == 1)
            {
                Clipboard.SetText(dataGridView1.SelectedCells[0].Value.ToString() + "\r\n");
            }
        }

        private void toolStripPaste_Click(object sender, EventArgs e)   //ctrl+v
        {
            label_central.SendToBack();   //TODO 

            if (CheckClipboard())  //full rows
            {
                PasteFullRow();
                return;
            }
            else if (dataGridView1.SelectedCells.Count > 1)  //no rows just many cells
            {
                FillCells();
            }

            int leftshift = Settings.Default.leftshift;
            try
            {
                string s = Clipboard.GetText();

                string[] lines = Regex.Split(s.TrimEnd("\r\n".ToCharArray()), "\r\n");

                int iRow = dataGridView1.CurrentCell.RowIndex;
                int iCol = dataGridView1.CurrentCell.ColumnIndex;
                DataGridViewCell oCell;
                DataRow dr = null;


                if (iRow + lines.Length > dataGridView1.Rows.Count - 1)  //true on last line
                {
                    bool bFlag = false;
                    foreach (string sEmpty in lines)
                    {
                        if (sEmpty == "")
                        {
                            bFlag = true;
                        }
                    }

                    dr = dt.NewRow();
                    int iNewRows = iRow + lines.Length - dataGridView1.Rows.Count;
                    if (iNewRows > 0)
                    {
                        if (bFlag)
                            dt.Rows.Add(iNewRows);
                        else
                            dt.Rows.Add(iNewRows + 1);
                    }
                    else if (iNewRows == 0 && iRow != dataGridView1.Rows.Count - 1)
                        dt.Rows.Add(iNewRows + 1);
                }
                foreach (string line in lines)
                {
                    if (iRow < dataGridView1.Rows.Count && line.Length > 0)
                    {
                        string[] sCells = line.Split('\t');
                        for (int i = 0; i < sCells.GetLength(0); ++i)
                        {
                            if (iCol + i < this.dataGridView1.ColumnCount)
                            {
                                oCell = dataGridView1[iCol + i, iRow];
                                oCell.Value = Convert.ChangeType(sCells[i]/*.Replace("\r", "")*/.Remove(0, leftshift), oCell.ValueType);
                            }
                            else
                            {
                                break;
                            }
                        }
                        iRow++;
                    }
                    else
                    {
                        break;
                    }
                }
                // Clipboard.Clear();
            }
            catch (FormatException)
            {
                MessageBox.Show(Mess.The_data_you_pasted_is_in_the_wrong_format_for_the_cell);
                return;
            }

            SaveButtonUI();
        }

        private void FillCells()
        {
            if (!CheckClipboard())
            {
                string s = Clipboard.GetText();
                DataGridViewCell oCell;

                foreach (DataGridViewCell cell in dataGridView1.SelectedCells)
                {
                    oCell = dataGridView1[cell.ColumnIndex, cell.RowIndex];
                    oCell.Value = Convert.ChangeType(s.Trim(), oCell.ValueType);  //#35
                }
                SaveButtonUI();
            }
        }

        private void newWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Default.nostart = true;
            Settings.Default.Save();
            var deffile = new ProcessStartInfo(Application.ExecutablePath);
            Process.Start(deffile);
        }

        private void cm1_Number_Click(object sender, EventArgs e)
        {
            DataGridViewCell oCell;
            Int32 n = 1; bool chknum = true;

            foreach (DataGridViewCell cell in dataGridView1.InvSelectedCells())
            {
                oCell = dataGridView1[cell.ColumnIndex, cell.RowIndex];

                if (chknum)
                {
                    var isNumeric = int.TryParse(oCell.Value.ToString(), out Int32 z);
                    if (isNumeric) n = z;
                    chknum = false;
                }

                oCell.Value = Convert.ChangeType(n.ToString(), oCell.ValueType);
                //oCell.Value = Convert.ChangeType(Convert.ToInt32(n), oCell.ValueType);  //test for sort
                n += 1;
            }
            SaveButtonUI();

        }


        private void cm3_EditF2_CheckStateChanged(object sender, EventArgs e)
        {
            if (cm3EditF2.Checked)
                dataGridView1.EditMode = DataGridViewEditMode.EditProgrammatically;
            else
                dataGridView1.EditMode = DataGridViewEditMode.EditOnF2;

            Settings.Default.F2_edit = cm3EditF2.Checked;
            dataGridView1.Refresh();
        }

        private void cm3_Scrollbar_CheckStateChanged(object sender, EventArgs e)
        {
            if (cm3Scrollbar.Checked)
            {
               // dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;  //1
               // dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);   //1
                //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridView1.AllowUserToResizeColumns = true;
            }
            else
            {              
              //  dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;  //1
              //  dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);

            }


            Settings.Default.scrollbar = cm3Scrollbar.Checked;
            dataGridView1.Refresh();
        }

        #endregion context menu

        private void textBox_find_TextChange(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(textBox_find.Text))
            {
                dataGridView1.ClearSelection();
                dataGridView1.Refresh(); return;
            }

            if (textBox_find.Text.Length < 2) return;


          //  var colS = Settings.Default.colSearch;
            int selectResult = Settings.Default.findresult;  //0: Row   1: Cell

            if (IsDataGrid)
            {
                dataGridView1.ClearSelection();
                _found = false;

                string _name = "";
                List<string> _searchlist = new List<string>();

                if (textBox_find.Text.ToLower().Contains(' '))
                {
                    string[] _search = textBox_find.Text.ToLower().Split(' ');

                    for (int i = 0; i < _search.Length; i++)
                        if (!string.IsNullOrEmpty(_search[i])) _searchlist.Add(_search[i].Trim());
                }
                else
                {
                    _searchlist.Add(textBox_find.Text.ToLower().Trim());
                }
                
              //  Thread.Sleep(200); //test  

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (colswitch == dataGridView1.ColumnCount )  //if search in all cells
                    {
                        for (int i = 0; i < dataGridView1.ColumnCount; i++)
                        {
                            _name = dt.Rows[row.Index].Field<string>(i).ToLowerInvariant();

                            if (!string.IsNullOrEmpty(_name))
                            {
                                if (_searchlist.All(x => _name.Contains(x)))  //logical AND
                                {
                                    if (selectResult == 0) //sel Rows
                                        dataGridView1.Rows[row.Index].Selected = true;
                                    else
                                        dataGridView1.Rows[row.Index].Cells[i].Selected = true;

                                    // dataGridView1.FirstDisplayedScrollingRowIndex = row.Index;

                                    _found = true;
                                    textBox_find.ForeColor = SystemColors.WindowText; //Color.Black;
                                }
                            }
                        }
                    }
                    else
                    {
                        _name = dt.Rows[row.Index].Field<string>(colswitch).ToLowerInvariant();

                        if (!string.IsNullOrEmpty(_name))
                        {
                            if (_searchlist.All(x => _name.Contains(x)))  //logical AND
                            {
                                if (selectResult == 0)
                                    dataGridView1.Rows[row.Index].Selected = true;                              
                                else
                                    dataGridView1.Rows[row.Index].Cells[colswitch].Selected = true;

                                //  dataGridView1.FirstDisplayedScrollingRowIndex = row.Index;

                                _found = true;
                                textBox_find.ForeColor = SystemColors.WindowText; //Color.Black;
                            }
                        }
                    }
                }
                if (!_found)//text red
                    textBox_find.ForeColor = Color.Red;
            }

            dataGridView1.Refresh();
        }

        private void dataGridView1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(DataGridViewRow)))
            {

                // The mouse locations are relative to the screen, so they must be 
                // converted to client coordinates.
                Point clientPoint = dataGridView1.PointToClient(new Point(e.X, e.Y));

                rowIndexOfItemUnderMouseToDrop =
                    dataGridView1.HitTest(clientPoint.X, clientPoint.Y).RowIndex;


                // If the drag operation was a copy then add the row to the other control.
                if (e.Effect == DragDropEffects.Move)
                {
                    if (rowIndexOfItemUnderMouseToDrop < 0)
                    {
                        return;  //otherwise line will disappear 
                    }

                    DataRow dr = dt.NewRow();

                    for (int i = 0; i < dataGridView1.ColumnCount; i++)
                    {
                        dr[i] = dataGridView1[i, rowIndexFromMouseDown].Value.ToString();
                    }

                    dt.Rows.RemoveAt(rowIndexFromMouseDown);
                    dt.Rows.InsertAt(dr, rowIndexOfItemUnderMouseToDrop);


                    SaveButtonUI();
                }
            }
            else if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string dirName, shortName, driveName, extName;

                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
                foreach (string fileName in files)
                {
                    this.path = fileName;

                    dirName = Path.GetDirectoryName(fileName);
                    shortName = Path.GetFileName(fileName);
                    driveName = Path.GetPathRoot(fileName);
                    extName = Path.GetExtension(fileName);

                    if (extName.Equals(".m3u") || extName.Equals(".m3u8"))
                    {
                        button_revert.Visible = true;

                        if (dataGridView1.RowCount == 0)
                        {
                            ImportDataFile(fileName);
                            break;
                        }
                        else  //imoprt and add
                        {
                            ImportDataFile(fileName, true);
                            SaveButtonUI();
                            break;
                        }
                    }
                    label_central.SendToBack();

                    SaveButtonUI();
                }
            }
        }

        private void dataGridView1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }

        /// <summary>
        /// move the selected line up or down
        /// </summary>
        /// <param name="direction">-1 up 1 down</param>
        public void MoveLine(int direction)
        {
            if (IsLinkChecked) button_check.PerformClick();

            dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Selected = true;

            int i;
            for (i = 0; i < dataGridView1.ColumnCount; i++)
            {
                if (dataGridView1.Columns[dataGridView1.Columns[i].HeaderText].Visible) break;
            }

            if (dataGridView1.SelectedCells.Count > 0 && dataGridView1.SelectedRows.Count > 0)  //whole row must be selected
            {
                DataGridViewRow row = dataGridView1.SelectedRows[0];
                var maxrow = dataGridView1.RowCount - 1;

                if (row != null
                    && !((row.Index == 0 && direction == -1) || (row.Index == maxrow && direction == 1)))
                {
                    DataGridViewRow swapRow = dataGridView1.Rows[row.Index + direction];

                    object[] values = new object[swapRow.Cells.Count];

                    foreach (DataGridViewCell cell in swapRow.Cells)
                    {
                        values[cell.ColumnIndex] = cell.Value;
                        cell.Value = row.Cells[cell.ColumnIndex].Value;
                    }

                    foreach (DataGridViewCell cell in row.Cells)
                        cell.Value = values[cell.ColumnIndex];

                    dataGridView1.Rows[row.Index + direction].Selected = true;
                    dataGridView1.Rows[row.Index].Selected = false;

                    //get first not hidden col and scroll to it  //issue #12

                    dataGridView1.CurrentCell = dataGridView1.Rows[row.Index + direction].Cells[i];  //scroll automatic to cell
                }
            }
            SaveButtonUI();
        }

        /// <summary>
        /// move the selected rows to top of DataTable dt
        /// </summary>
        public void MoveLineTop()
        {
            _endofLoop = false;
            button_check.BackColor = Color.MidnightBlue;
            clearCellColor(dataGridView1);

            dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Selected = true;

            if (dataGridView1.SelectedCells.Count > 0 && dataGridView1.SelectedRows.Count > 0)  //whole row must be selected
            {
                string rows4move = ClassCopyPaste.GetRows(dataGridView1, dt).ToString().RemoveFirstLines(1);

                ///delete rows
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    dt.Rows.RemoveAt(row.Index);
                }

                ///paste intsert rows to top
                string[] pastedRows = Regex.Split(rows4move.TrimEnd("\r\n".ToCharArray()), "\r\n");

                InsertRowsAt(0, pastedRows);

                _endofLoop = true;
                SaveButtonUI();
            }
        }

        /// <summary>
        /// inserts full rows at position in DataTable dt
        /// </summary>
        /// <param name="index">position to insert</param>
        /// <param name="rows">full  rows</param>
        private void InsertRowsAt(int index, string[] rows )
        {
            
            DataRow dr = null;

            foreach (string pastedRow in rows)
            {
                string[] pastedRowCells = pastedRow.Split(new char[] { '\t' });

                dr = dt.NewRow();

                for (int i = 0; i < pastedRowCells.Length; i++)
                {
                    dr[i] = pastedRowCells[i];
                }
                dt.Rows.InsertAt(dr, index);
                index++;

            }

        }

        /// <summary>
        /// moves selected rows to bottom of DataTable dt
        /// </summary>
        public void MoveLineBottom()
        {
            _endofLoop = false;
            button_check.BackColor = Color.MidnightBlue;
            clearCellColor(dataGridView1);

            dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Selected = true;

            if (dataGridView1.SelectedCells.Count > 0 && dataGridView1.SelectedRows.Count > 0)  //whole row must be selected
            {
                string rows4move = ClassCopyPaste.GetRows(dataGridView1, dt).ToString().RemoveFirstLines(1);

                ///delete rows
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    dt.Rows.RemoveAt(row.Index);
                }

                ///paste intsert rows to top
                string[] pastedRows = Regex.Split(rows4move.TrimEnd("\r\n".ToCharArray()), "\r\n");

                InsertRowsAt(dataGridView1.Rows.Count, pastedRows);

                _endofLoop = true;
                SaveButtonUI();
            }
        }

        /// <summary>
        /// change icon and flag for saving file
        /// </summary>
        /// <param name="hasChanged">true if grid modified vs file</param>
        /// <param name="reset">reset undo/redo stack</param>
        public void SaveButtonUI(NeedSave modified = NeedSave.Yes)
        {
            switch (modified)
            {
                case NeedSave.Yes:
                    button_save.Image = Resources.content_save_modified_r;
                    isModified = true;
                    DataGridView1_CellValidated(null, null);
                    break;

                case NeedSave.No:
                    isModified = false;
                    button_save.Image = Resources.content_save_r;
                    break;

                case NeedSave.Reset:
                    isModified = false;
                    undoStack.Clear(); redoStack.Clear(); ShowReUnDo(0);
                    button_save.Image = Resources.content_save_r;
                    break;
            }

        }

        /// <summary>
        /// fills combobox of player form with data
        /// </summary>
        private void fillPlayer()
        {
            if (player != null)
            {
                player.comboBox1.Items.Clear();
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    player.comboBox1.Items.Add(dt.Rows[i]["Name2"]);
                }
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!_endofLoop) return;  //avoid lag with player open

            SaveButtonUI();

            _endofLoop = false;
        }

       // private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
       //// private void dataGridView1_ColumnHeaderMouseClick(object sender, MouseEventArgs e)
       // {

       //     return;
       //     //if (e.Button == MouseButtons.Right)  //right mouse click doesn't trigger event
       //     //{
       //     //    contextMenuStrip3.Show(dataGridView1, dataGridView1.PointToClient(Cursor.Position));
       //     //}
       //     if (e.Button == MouseButtons.Left)
       //     {

       //         SaveButton();

       //         if (_sort == "desc")
       //         {
       //             _sort = "asc";
       //             dataGridView1.Sort(dataGridView1.Columns[e.ColumnIndex], System.ComponentModel.ListSortDirection.Descending);
       //         }
       //         else
       //         {
       //             _sort = "desc";
       //             dataGridView1.Sort(dataGridView1.Columns[e.ColumnIndex], System.ComponentModel.ListSortDirection.Ascending);
       //         }

       //         dt = dt.DefaultView.ToTable(); // The Sorted View converted to DataTable and then assigned to table object.
       //         dt = dt.DefaultView.ToTable("IPTV");

       //         //#25 rebind after sort
       //         dataGridView1.DataSource = dt;
       //         dataGridView1.Refresh();

       //        // SetHeaderContextMenu();

       //         if (_linkchecked) PaintRows();  //#41
       //     }
       // }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.Programmatic;
            }
        }


        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (!Settings.Default.dclick) return;  //disable

            if (IsControlKey)
            {
                playToolStripMenuItem.PerformClick();
            }
            else
            {
                if (IsDataGrid && !string.IsNullOrEmpty(vlcpath))
                    button_vlc.PerformClick();
            }
        }

        /// <summary>
        /// event handler for popup window close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormP_Closed(object sender, FormClosedEventArgs e)
        {
            button_check.Enabled = true;
            PaintRows();
            tokenSource.Cancel();
        }

        /// <summary>
        /// Check if Link responds
        /// /// </summary>
        /// <param name="token"></param>
        /// <param name="progress"></param>
        /// <returns></returns>
        private async Task RunStreamCheck2(CancellationToken token, IProgress<string> progress)
        {
            checkList.Clear();

            string maxrows = dataGridView1.Rows.Count.ToString();

            checkList.Add(new CheckList
            {
                Url = maxrows
            });

            SemaphoreSlim semaphoreObject = new SemaphoreSlim(Settings.Default.maxthread, Settings.Default.maxthread);
            Check streamcheck = new Check();

            List<Task> trackedTasks = new List<Task>();

            foreach (DataGridViewRow item in dataGridView1.Rows)
            {
                if (token.IsCancellationRequested)
                {
                   // button_check.Enabled = true;
                   // PaintRows();
                    break;
                }

                var iLink = dt.Rows[item.Index].Field<string>("Link");

                if (iLink.StartsWith("plugin") || iLink.StartsWith("ud")/* || iLink.Contains("|User")*/)   //plugin will not be checked
                {
                    dataGridView1.Rows[item.Index].Cells["Link"].Style.BackColor = SystemColors.InactiveCaption; //Color.LightGray;
                    dataGridView1.FirstDisplayedScrollingRowIndex = item.Index;
                    continue;
                }

                if (iLink.StartsWith("pipe"))
                    iLink = ClassHelp.CleanTVHeadendLink(iLink);

                await semaphoreObject.WaitAsync();
                trackedTasks.Add(Task.Run(() =>
                {
                    try { streamcheck.streamchk(iLink); }
                    catch (Exception) { semaphoreObject.Release(); }
                    finally { semaphoreObject.Release(); }
                }));

                progress.Report(checkList.Count.ToString() + " / " + maxrows);
            }

            await Task.WhenAll(trackedTasks);  //wait for all tasks to finish

#if DEBUG
            Console.WriteLine("All Task finished");
#endif

        }

        private void UndoButton_Click(object sender, EventArgs e)
        {
            if (dt.Rows.Count == 0) return;
            //int Index = dataGridView1.CurrentCell.RowIndex;

            Cursor.Current = Cursors.WaitCursor;


            if (redoStack.Count == 0 || redoStack.LoadItem(dataGridView1))
            {
                redoStack.Push(dataGridView1.Rows.Cast<DataGridViewRow>()
                    .Where(r => !r.IsNewRow).Select(r => r.Cells.Cast<DataGridViewCell>()
                    .Select(c => c.Value).ToArray()).ToArray());
            }

            if (undoStack.Count > 0)
            {
                object[][] gridrows = undoStack.Pop();
                while (gridrows.ItemEquals(dataGridView1.Rows.Cast<DataGridViewRow>().Where(r => !r.IsNewRow).ToArray()))
                {
                    {
                        try
                        {
                            gridrows = undoStack.Pop();
                        }
                        catch (Exception) { }
                    }
                }
                ignore = true;

                dt.Clear();  // row clear

                for (int x = 0; x <= gridrows.GetUpperBound(0); x++)
                {
                    dt.Rows.Add(gridrows[x]);
                }

                ignore = false;

                ShowReUnDo(0);

                //try
                //{
                //    dataGridView1.CurrentCell = dataGridView1[0, Index];
                //}
                //catch (Exception)
                //{
                //    try
                //    {
                //        dataGridView1.CurrentCell = dataGridView1[0, Index - 1];

                //    }
                //    catch (Exception)
                //    {

                //        dataGridView1.CurrentCell = dataGridView1[dataGridView1.ColumnCount -1, 0];
                //    }
                //}

            }
            Cursor.Current = Cursors.Default;

        }

        private void RedoButton_Click(object sender, EventArgs e)
        {
            if (dt.Rows.Count == 0) return;

            Cursor.Current = Cursors.WaitCursor;

            if (undoStack.Count == 0 || undoStack.LoadItem(dataGridView1))
            {
                undoStack.Push(dataGridView1.Rows.Cast<DataGridViewRow>()
                    .Where(r => !r.IsNewRow).Select(r => r.Cells.Cast<DataGridViewCell>()
                    .Select(c => c.Value).ToArray()).ToArray());
            }
            if (redoStack.Count > 0)
            {
                object[][] gridrows = redoStack.Pop();

                while (gridrows.ItemEquals(dataGridView1.Rows.Cast<DataGridViewRow>().Where(r => !r.IsNewRow).ToArray()))
                {
                    gridrows = redoStack.Pop();
                }
                ignore = true;
                dt.Clear();
                for (int x = 0; x <= gridrows.GetUpperBound(0); x++)
                {
                    dt.Rows.Add(gridrows[x]);
                }

                ignore = false;

                ShowReUnDo(0);
            }

            Cursor.Current = Cursors.Default;

        }

        private void DataGridView1_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (ignore) { return; }
            if (undoStack.LoadItem(dataGridView1))
            {
                undoStack.Push(dataGridView1.Rows.Cast<DataGridViewRow>()
                    .Where(r => !r.IsNewRow)
                    .Select(r => r.Cells.Cast<DataGridViewCell>()
                    .Select(c => c.Value).ToArray()).ToArray());
            }
            ShowReUnDo(1);
        }

        private void ShowReUnDo(int x)
        {
            if (undoStack.Count > x)
            {
                UndoButton.Enabled = true;
                UndoButton.Image = Resources.undo_r;
            }
            else
            {
                UndoButton.Enabled = false;
                UndoButton.Image = Resources.undo_fade_r;
            }
            if (redoStack.Count > x)
            {
                RedoButton.Enabled = true;
                RedoButton.Image = Resources.redo_r;
            }
            else
            {
                RedoButton.Enabled = false;
                RedoButton.Image = Resources.redo_fade_r;
            }
        }

        private void cm1_hideColumn_Click(object sender, EventArgs e)
        { // #11
            foreach (DataGridViewCell cell in dataGridView1.SelectedCells)
            {
                dataGridView1.Columns[dataGridView1.Columns[cell.ColumnIndex].HeaderText].Visible = false;
            }
            dataGridView1.ClearSelection();  //to avoid right click error afterwards

            Form1_SizeChanged(sender, EventArgs.Empty);

        }

        private void cm1_showColumns_Click(object sender, EventArgs e)
        { // #11
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                dataGridView1.Columns[dataGridView1.Columns[i].HeaderText].Visible = true;
            }
        }

        /// <summary>
        /// Event Handler of player combobox.
        /// Gets Combobox entry and plays on vlc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Combo_Changed(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;

            var channel = combo.SelectedIndex;

#if DEBUG
            //  MessageBox.Show("channel: " + channel);
#endif

            if (channel < 0) return;

            dataGridView1.CurrentCell = dataGridView1.Rows[channel].Cells["Name2"];
            dataGridView1.Rows[channel].Selected = true;

            _isSingle = true;

            //string param = dataGridView1.CurrentRow.Cells["Link"].Value.ToString();


           m_ClassPlayer.RunPlayer(dataGridView1);    

           player.Opacity = Settings.Default.opacity;
        }

        private ClassPlayer m_ClassPlayer;

        /// <summary>
        /// shows or create player form
        /// </summary>
        private void CreatePlayer()
        {
            // if the form is not closed, show it
            if (player == null)
            {
                player = new player();
                player.comboBox1.SelectedIndexChanged += new EventHandler(Combo_Changed);  //combo changed
                player.FormClosed += new FormClosedEventHandler(player_FormClosed);  //form closed

                player.Dgv = this.dataGridView1;

                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    player.comboBox1.Items.Add(dt.Rows[i]["Name2"]);
                }

                if (Settings.Default.F1Location.X == 0 && Settings.Default.F1Location.Y == 0)
                {
                    // first start
                    player.Location = new Point(10, 10);
                }
                else
                {
                    player.Location = Settings.Default.F1Location;
                }
                player.StartPosition = FormStartPosition.Manual;
                // attach the handler
                player.FormClosed += ChildFormClosed;
            }

            // show it
            player.Show();
        }

        private void player_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClassPlayer.KillPlayer();
        }

        private void ChildFormClosed(object sender, FormClosedEventArgs args)
        {
            // detach the handler
            player.FormClosed -= ChildFormClosed;

            // let GC collect it (and this way we can tell if it's closed)
            player = null;
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (rowIndexFromMouseDown == -1 ) e.Cancel= true;  //mouse over col header

            if (NoDataRow)  //empty grid
            {
                for (int i = 0; i < contextMenuStrip1.Items.Count; i++)  //0,1 enabled
                {
                    contextMenuStrip1.Items[i].Enabled = false;
                }
                if (!string.IsNullOrEmpty(fullRowContent))  //for paste to new window
                    contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = true;  //paste add
                else if (string.IsNullOrEmpty(fullRowContent) && CheckClipboard())
                    contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = true;  //paste add
                else
                    contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = false;

                //if (!string.IsNullOrEmpty(fullRowContent)
                //    || (string.IsNullOrEmpty(fullRowContent) && CheckClipboard(columnNames)))  //TODO
                //    contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = true;  //paste add
                //else
                //    contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = false;

                contextMenuStrip1.Items["cms1NewWindow"].Enabled = true;
            }
            else if (dataGridView1.SelectedCells.Count > 0) //open menu not on header
            {
                string[] itemsNList = new string[] { "toolStripCopy", "playToolStripMenuItem", "cm1NewColumn",
                    "cm1_hideColumn", "cm1_showColumns", "fontSIzeToolStripMenuItem", "cm1_UserAgent", "cm1_TVHeadend"};

                for (int i = 0; i < itemsNList.Length; i++)
                {
                    contextMenuStrip1.Items[itemsNList[i]].Enabled = true;
                }

                if (dataGridView1.SelectedRows.Count > 0)
                {
                    contextMenuStrip1.Items["cutRowMenuItem"].Enabled = true;  //cut
                }
                else
                {
                    contextMenuStrip1.Items["cutRowMenuItem"].Enabled = false;
                }

                if (colIndexFromMouseDown != dataGridView1.ColumnCount - 1)
                {
                    contextMenuStrip1.Items["cm1_UserAgent"].Enabled = false;  //user agent on link col
                    contextMenuStrip1.Items["cm1_TVHeadend"].Enabled = false;  //user agent on link col

                }


                //ffprobe only when found  //TODO
                //if (_ffprobefound)
                //{
                //    if (dataGridView1.SelectedCells.Cast<DataGridViewCell>().Select(c => c.ColumnIndex).Distinct().Count() == 1)

                //        contextMenuStrip1.Items["cms1GetName"].Visible = true;
                //    else
                //        contextMenuStrip1.Items["cms1GetName"].Visible = false;

                //}

                //Numbering only in rows
                if (dataGridView1.SelectedCells.Count > 1)
                {
                    Int32 selectedCellCount = dataGridView1.GetCellCount(DataGridViewElementStates.Selected);

                    int minCol = dataGridView1.CurrentCell.ColumnIndex;
                    int maxCol = 0;
                    int y;   //columns

                    for (int i = 0; i < selectedCellCount; i++)
                    {
                        y = dataGridView1.SelectedCells[i].ColumnIndex;
                        if (y < minCol) minCol = y;
                        if (y > maxCol) maxCol = y;
                    }

                    if (minCol == maxCol)
                        contextMenuStrip1.Items["cms1Number"].Enabled = true;
                    else
                        contextMenuStrip1.Items["cms1Number"].Enabled = false;
                }
                else
                    contextMenuStrip1.Items["cms1Number"].Enabled = false;

                if (Clipboard.ContainsText())
                {
                    contextMenuStrip1.Items["toolStripPaste"].Enabled = true;  //paste
                }

                if (!string.IsNullOrEmpty(fullRowContent))  //for paste to new window  //issue #18 CB
                    contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = true;  //paste add
                else if (string.IsNullOrEmpty(fullRowContent) && CheckClipboard())
                    contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = true;  //paste add
                else
                    contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = false;

                //if (!string.IsNullOrEmpty(fullRowContent))  //for paste to new window
                //    contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = true;  //paste add
                //else if (string.IsNullOrEmpty(fullRowContent) && CheckClipboard(columnNames))
                //    contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = true;  //paste add
                //else
                //    contextMenuStrip1.Items["pasteRowMenuItem"].Enabled = false;
            }
            else if (dataGridView1.SelectedCells.Count == 0)
            {
                string[] itemsSList = new string[] { "toolStripCopy", "toolStripPaste", "playToolStripMenuItem",
                     "cm1_hideColumn"};

                for (int i = 0; i < itemsSList.Length; i++)
                {
                    contextMenuStrip1.Items[itemsSList[i]].Enabled = false;
                }

            }

        }


        private void button_import_Click(object sender, EventArgs e)  //TODO
        {
            if (IsDataGrid)
            {
                MessageBox.Show(Mess.Import_only_on_empty);
                return;
            }

            dt.TableName = "IPTV";

            checkList.Clear();

            dataGridView1.DataSource = dt;

            DataObject o = (DataObject)Clipboard.GetDataObject();

            if (Clipboard.ContainsText())
            {
                using (StringReader playlistFile = new StringReader(o.GetData(DataFormats.UnicodeText).ToString()))
                {
                    Cursor.Current = Cursors.WaitCursor;
                   
                    string fullTxt = playlistFile.ReadToEnd();  //read rest of file
                    string[] fileRows = fullTxt.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

                    if (fileRows[0].StartsWith("#EXTM3U"))
                    {
                        fileHeader = fileRows[0];
                    }

                    CreateDataTable(SeekFileElements(fullTxt));
                    columnNames1 = ClassImportData.GetColumnNames(dt);
                    dt = ClassImportData.ImportData(dt, fileRows);

                    Cursor.Current = Cursors.Default;
                }
                label_central.SendToBack();

                SaveButtonUI();
            }

            if (dt.Rows.Count == 0)
            {
                MessageBox.Show(Mess.Wrong_input, Mess.File_Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void button_kodi_Click(object sender, EventArgs e)
        {
            playToolStripMenuItem.PerformClick();
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.ContextMenuStrip = contextMenuStrip2;
        }

        private void editCellCopy_Click(object sender, EventArgs e)
        {
            if (dataGridView1.EditingControl is TextBox textBox)
            {
                //  TextBox textBox = (TextBox)dataGridView1.EditingControl;

                if (!string.IsNullOrEmpty(textBox.SelectedText))
                    Clipboard.SetText(textBox.SelectedText);
            }
        }

        private void editCellPaste_Click(object sender, EventArgs e)
        {
            string s = Clipboard.GetText();
            if (dataGridView1.EditingControl is TextBox)
            {
                TextBox textBox = (TextBox)dataGridView1.EditingControl;
                textBox.SelectedText = s;
            }
        }

        private void DataGridStyle(DataGridView dgv)
        {
            dgv.EnableHeadersVisualStyles = false;  //to make header style take effect

            DataGridViewCellStyle column_header_cell_style = new DataGridViewCellStyle();
            column_header_cell_style.BackColor = SystemColors.ControlLight;
            column_header_cell_style.ForeColor = Color.Black;
            //column_header_cell_style.SelectionBackColor = Color.Chocolate;
            //column_header_cell_style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //column_header_cell_style.Font = new Font("Tahoma", 12, FontStyle.Bold, GraphicsUnit.Pixel);  //set in ZoomGrid


            dgv.ColumnHeadersDefaultCellStyle = column_header_cell_style;
        }

        private void cm1_NewColumn_Click(object sender, EventArgs e)
        {
            cm1ColCombo.DroppedDown = false;
        }


        private void cm5_Duplicate_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (NoDataRow) return;

            cm5ColumNames.Items.Clear();

            foreach (string s in columnNames1)
            {
                cm5ColumNames.Items.Add(s);
            }

            try     { cm5ColumNames.SelectedIndex = Settings.Default.colDupli2; }
            catch   { cm5ColumNames.SelectedIndex = 1;}

        }

        private void cm5_ColumNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Default.colDupli2 = cm5ColumNames.SelectedIndex;
            Settings.Default.Save();
        }

        private void cm5_StartSearchDupli_Click(object sender, EventArgs e)
        {
            if (NoDataRow) return;

            button_dup.BackColor = Color.LightSalmon;

            FindDuplicates();
        }

        private void cm4_EditFIleHeader_Click(object sender, EventArgs e)
        {
            using (EditHeader h = new EditHeader(fileHeader))
            {
                var result = h.ShowDialog();

                if (result == DialogResult.OK)
                {
                    fileHeader = h.headerText + "\n"; // Environment.NewLine;
                }
            }

        }

        private void cm6_TVHeadend_Click(object sender, EventArgs e)
        {
            using (EditTVHead h = new EditTVHead(tvhead1, tvhead2))
            {
                var result = h.ShowDialog();

                if (result == DialogResult.OK)
                {
                    tvhead1 = h.TVheader1.Trim() + " "; 
                    tvhead2 = " " + h.TVheader2.Trim(); 



                    //convert link to TVHeadend

                    //for (int row = 0; row < dataGridView1.Rows.Count; row++)
                    //{
                    //    string linkvalue = dataGridView1.Rows[row].Cells["Link"].Value.ToString();
                    //    dataGridView1.Rows[row].Cells["Link"].Value = tvhead1 + linkvalue + tvhead2;
                    //}
                    //SaveButtonUI();
                }
            }

        }


        private void cm1_AddColumn_Click(object sender, EventArgs e)
        {
            string addColumns = cm1ColCombo.Text;
            if (string.IsNullOrEmpty(addColumns)) return;
            dt.Columns.Add(addColumns).SetOrdinal(0);

            //reorder the Columns
            foreach (DataColumn c in dt.Columns)
            {
                dataGridView1.Columns[c.ColumnName].DisplayIndex = dt.Columns.IndexOf(c);
            }

            cm1ColCombo.Items.Clear();
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            lblNoRows.Text = dataGridView1.RowCount.ToString();

        }

        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            lblNoRows.Text = dataGridView1.RowCount.ToString();

        }

        private void cm1_ColCombo_Click(object sender, EventArgs e)
        {
            cm1ColCombo.Items.Clear();
            cm1ColCombo.DroppedDown = false;

            List<string> droplist = new List<string>();

            var myClass = new ClassDataset();


            droplist.AddRange(ColTypes);
            droplist.AddRange(myClass.KodiPropTypes);
            droplist.AddRange(myClass.KodiReferrerTypes);

            List<string> colExists = new List<string>();

            foreach (DataColumn c in dt.Columns)
            {
                colExists.Add(c.ColumnName);
            }

            var result = droplist.Except(colExists);

            foreach (string s in result)
            {
                cm1ColCombo.Items.Add(s);
            }

            cm1ColCombo.DroppedDown = true;

        }

        private void btnSelectList_Click(object sender, EventArgs e)
        {
            if (!IsDupChecked && !IsLinkChecked && !IsCompareChecked) return;

            if (colorList[0].Count == 0 && colorList[1].Count == 0) {
            
             //   return; 
            }

            if (IsLinkChecked) 
            { 
                SwitchErrorLists();

                if (_dlistERR >= 3) _dlistERR = 0;
                return;
            }

            try
            {
                SwitchDupLists();

                _dlistAB++;

                if (_dlistAB == 3) _dlistAB = 0;

            }
            catch (Exception)
            {
                NotificationBox.Show(Mess.Table_changed, 3000, NotificationMsg.ERROR);
            }




        }

        private void SwitchErrorLists()
        {

            if (_dlistERR == 0)
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (dataGridView1.Rows[row.Index].Cells[0].Style.BackColor == Color.LightSalmon)
                    {
                        dataGridView1.Rows[row.Index].Selected = true;
                    }
                }

            }
            else if (_dlistERR == 1)
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (dataGridView1.Rows[row.Index].Cells[0].Style.BackColor == SystemColors.InactiveCaption
                        || dataGridView1.Rows[row.Index].Cells[0].Style.BackColor == Settings.Default.Error403) //SystemColors.ControlLight) //   Color.LightGray)
                    {
                        dataGridView1.Rows[row.Index].Selected = true;
                    }
                }

            }
            else if ( _dlistERR == 2)
            {
                dataGridView1.ClearSelection();
            }
            _dlistERR++;
        }

        private void DefineCellColors()
        {
            paintDupCells.Add(new PaintDupCells
            {
                Select1 = false,
                Select2 = true,
                Color1 = Color.LightCoral,
                Color2 = Color.LightGreen
            }
            );
            paintDupCells.Add(new PaintDupCells
            {
                Select1 = true,
                Select2 = true,
                Color1 = Color.LightCoral,
                Color2 = Color.LightGreen
            }
            );
            paintDupCells.Add(new PaintDupCells
            {
                Select1 = true,
                Select2 = false,
                Color1 = Color.LightCoral,
                Color2 = Color.LightGreen
            }
            );
            paintDupCells.Add(new PaintDupCells
            {
                Select1 = false,
                Select2 = false,
                Color1 = Color.Empty,
                Color2 = Color.Empty
            }
            );


        }

        private void cm6_Save_Click(object sender, EventArgs e)
        {
            SaveFile(true);
        }

        private void cm6_SaveAs_Click(object sender, EventArgs e)
        {
            SaveFile(false);
        }

        private void SwitchDupLists()
        {
            string scolID = "";
            try
            {
                scolID = columnNames1[Settings.Default.colDupli2];
            }
            catch
            {
                scolID = columnNames1[0];
            }

            foreach (var item in colorList[0])
            {
                dataGridView1.Rows[item.Index].Selected = paintDupCells[_dlistAB].Select1;
                dataGridView1.Rows[item.Index].Cells[scolID].Style.BackColor = paintDupCells[_dlistAB].Color1;

            }

            foreach (var item in colorList[1])
            {
                dataGridView1.Rows[item.Index].Selected = paintDupCells[_dlistAB].Select2;
                dataGridView1.Rows[item.Index].Cells[scolID].Style.BackColor = paintDupCells[_dlistAB].Color2;

            }

        }





        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!Settings.Default.dclick) return;  //disable

            if (IsControlKey)
            {
                playToolStripMenuItem.PerformClick();
            }
            else
            {
                if (IsDataGrid && !string.IsNullOrEmpty(vlcpath))
                    button_vlc_Click(sender, e);
                // button_vlc.PerformClick();
            }

        }


        private void editCellCut_Click(object sender, EventArgs e)
        {
            if (dataGridView1.EditingControl is TextBox)
            {
                TextBox textBox = (TextBox)dataGridView1.EditingControl;
                if (textBox.SelectedText != "") Clipboard.SetText(textBox.SelectedText);
                textBox.SelectedText = "";
            }
        }

        private void cm2_addUseragentCell_Click(object sender, EventArgs e)
        {
            DataGridViewCell oCell;

            foreach (DataGridViewCell cell in dataGridView1.InvSelectedCells())
            {
                oCell = dataGridView1[cell.ColumnIndex, cell.RowIndex];
                string oCellText = oCell.Value.ToString();

                if (oCellText.EndsWith("m3u") || oCellText.EndsWith("m3u8"))
                    oCell.Value = oCellText + "|User-Agent=" + Settings.Default.user_agent;
            }

       //     SaveButtonUI();

            //if (dataGridView1.EditingControl is TextBox)
            //{
            //    TextBox textBox = (TextBox)dataGridView1.EditingControl;
            //    if (textBox.Text.EndsWith("m3u8") || textBox.Text.EndsWith("m3u"))
            //    { //#18
            //        textBox.Text += "|User-Agent=" + Settings.Default.user_agent;
            //        // textBox.Text += "|User-Agent=Mozilla/5.0 (X11; Linux i686; rv:42.0) Gecko/20100101 Firefox/42.0 Iceweasel/42.0";
            //    }
            //}
        }
        private void addUseragentCell_ClickN(object sender, EventArgs e)
        {
            if (dataGridView1.EditingControl is TextBox)
            {
                TextBox textBox = (TextBox)dataGridView1.EditingControl;
                if (textBox.Text.EndsWith("m3u8") || textBox.Text.EndsWith("m3u"))
                { //#18
                    textBox.Text += "|User-Agent=" + Settings.Default.user_agent;
                    // textBox.Text += "|User-Agent=Mozilla/5.0 (X11; Linux i686; rv:42.0) Gecko/20100101 Firefox/42.0 Iceweasel/42.0";
                }
            }
        }

        private void cm7_Open_Click(object sender, EventArgs e)
        {
            OpenFile(false);
        }

        private void cm7_Append_Click(object sender, EventArgs e)
        {
            OpenFile(true);
        }

        private Point MenuPosition(Button button)
        {
            var btt = button.PointToScreen(new Point());
            var att = dataGridView1.Location;
            var ctt = button.Location;
            var size = button.Size;
            ctt.Y += size.Height; ctt.X = 9; ctt.Y = 52;
            return ctt;
        }

        private void cm7_NewWindow_Click(object sender, EventArgs e)
        {
            Settings.Default.nostart = true;
            Settings.Default.Save();
            var deffile = new ProcessStartInfo(Application.ExecutablePath);
            Process.Start(deffile);

        }

        private void cm7_Filterlist_Click(object sender, EventArgs e)
        {
#if DEBUG
            //test  //if template already there, what todo?
            if (tabControl1.TabPages.Contains(tabPage2))
            {
                DialogResult dialogSave = MessageBox.Show(Mess.Do_you_want_to_overwrite_your_current_template,
                    Mess.Save_Playlist, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogSave == DialogResult.Yes)
                {
                    /// delete tabpage2
                    tabControl1.TabPages.Remove(tabPage2);
                    dt2.Clear();

                }
                if (dialogSave == DialogResult.No) return;

            }

            if (ComposeFilterList())
            {
                tabControl1.TabPages.Add(tabPage2);

                dataGridView2.DataSource = dt2;
                columnNames2 = ClassImportData.GetColumnNames(dt2);

            }

            Form1_SizeChanged(sender, EventArgs.Empty);

            return;

#endif

            MessageBox.Show("Not implemented yet, use append \n\n " +
                "How to:\n" +
                "1. Save a file as filter list\n" +
                "2. Open file to apply the filter list on\n" +
                "3. Click **Open** and append filter list\n" +
                "4. Click on **Find Duplicates** button\n" +
                "5. Click **Search Duplicates**\n" +
                "6. with switch list button (NEW) select list you want to have", "Information", MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        private void contextMenuStrip2_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {  //opens when edit cell active
            return;

            if (dataGridView1.EditingControl is TextBox)
            {
                TextBox textBox = (TextBox)dataGridView1.EditingControl;
                if (textBox.Text.EndsWith("m3u8") || textBox.Text.EndsWith("m3u"))
                {
                    contextMenuStrip2.Items["addUseragentCell"].Enabled = true;
                }
                else
                {
                    contextMenuStrip2.Items["addUseragentCell"].Enabled = false;
                }
            }
        }

        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // if (Debugger.IsAttached) return;  //#37 test

            if (e.RowIndex >= 0 && e.ColumnIndex >= 0 /*& IsSelected*/)
            {
                e.Handled = true;
                e.PaintBackground(e.CellBounds, true);

                string[] _search = textBox_find.Text.ToLower().Split(' ');
                string sw = _search[0].Trim();

                if (!string.IsNullOrEmpty(sw))
                {
                    for (int i = 0; i < _search.Length; i++)
                    {
                        sw = _search[i].Trim();
                        PaintCells(sw, i);
                    }
                }
                e.PaintContent(e.CellBounds);
            }

            void PaintCells(string sw, int s_length)
            {
                Color[] colors = new Color[] { Color.Orange, Color.Yellow, Color.GreenYellow };

                string val = (string)e.FormattedValue;
                int sindx = val.ToLower().IndexOf(sw.ToLower());

                if (sindx >= 0)
                {
                    Rectangle hl_rect = new Rectangle();
                    hl_rect.Y = e.CellBounds.Y + 2;
                    hl_rect.Height = e.CellBounds.Height - 5;

                    string sBefore = val.Substring(0, sindx);
                    string sWord = val.Substring(sindx, sw.Length);
                    Size s1 = TextRenderer.MeasureText(e.Graphics, sBefore, e.CellStyle.Font, e.CellBounds.Size);
                    Size s2 = TextRenderer.MeasureText(e.Graphics, sWord, e.CellStyle.Font, e.CellBounds.Size);

                    if (s1.Width > 5)
                    {
                        hl_rect.X = e.CellBounds.X + s1.Width - 5;
                        hl_rect.Width = s2.Width - 6;
                    }
                    else
                    {
                        hl_rect.X = e.CellBounds.X + 2;
                        hl_rect.Width = s2.Width - 6;
                    }

                    SolidBrush hl_brush = default(SolidBrush);
                    if ((e.State & DataGridViewElementStates.Selected) != DataGridViewElementStates.None)
                    {
                        hl_brush = new SolidBrush(Color.DarkGoldenrod);
                    }
                    else if (s_length < 3)
                    {
                        hl_brush = new SolidBrush(colors[s_length]);
                    }
                    else
                    {
                        hl_brush = new SolidBrush(Color.Yellow);
                    }

                    e.Graphics.FillRectangle(hl_brush, hl_rect);

                    hl_brush.Dispose();
                }
            }
        }

        private void button_refind_Click(object sender, EventArgs e)
        {
            textBox_find_TextChange(sender, e);
        }


        private void textBox_find_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == (Char)27)
            //{
            //    textBox_find.Visible = false;
            //    button_clearfind.Visible = false; lblRowCheck.Visible = false; lblColCheck.Visible = false;
            //    button_refind.Visible = false;
            //}
        }

        private void btnTemplate_Click(object sender, EventArgs e)
        {
            if (!IsTemplate) cm7_Filterlist_Click(sender, e);
            if (IsLinkChecked) button_check.PerformClick();

            if (IsCompareChecked)
            {
                btnTemplate.BackColor = Color.MidnightBlue;
                clearCellColor(dataGridView1);

            }
            else
            cm8_Template.Show(btnTemplate, new Point(0, 41));
            //todo bug compare columns of both tables
        }

        private void cm8_UseTemplate_Click(object sender, EventArgs e)
        {
            //compare template with m3u and mark same 
            int colD; string scolID = "";
            try
            {
                colD = Settings.Default.colDupli2;
                scolID = columnNames1[colD];
            }
            catch
            {
                colD = 0;
                scolID = columnNames1[colD];
            }

            if (colorList[0].Count > 0)
            {
                clearCellColor(dataGridView1);
                colorList[0].Clear(); colorList[1].Clear();
            }
            if (duplicateList[0].Count > 0)
            {
                clearCellColor(dataGridView1);
                duplicateList[0].Clear(); duplicateList[1].Clear();
            }

            btnTemplate.BackColor = Color.LightSalmon;


            dataGridView1.ClearSelection();
            int rowcount = dataGridView1.RowCount;
            int rowcount2 = dataGridView2.RowCount;

            if (rowcount > 0)
            {
                Cursor.Current = Cursors.WaitCursor;

                for (int row = 0; row < rowcount; row++)
                {
                    string mastercell = dt.Rows[row].Field<string>(scolID);
                    if (!string.IsNullOrEmpty(mastercell))
                    {
                        for (int a = 0; a < rowcount2 /*- row*/; a++)
                        {
                            // string slavecell = dt.Rows[row + a].Field<string>(scolID);
                            if (mastercell == dt2.Rows[/*row + */a].Field<string>(scolID))  //compare dt 2 times faster
                            {
                                dataGridView1.Rows[row /*+ a*/].Selected = true;

                                //duplicateList[0].Add(new DupList { CellName = mastercell, ColName = scolID, Master = true });
                                //duplicateList[1].Add(new DupList { CellName = slavecell, ColName = scolID, Master = false });

                                colorList[0].Add(dataGridView1.Rows[row]);

                               // colorList[1].Add(dataGridView1.Rows[row + a]);

                            }
                        }
                    }
                }
                Cursor.Current = Cursors.Default;
            }
            DefineCellColors();
            _dlistAB = 2;
            SwitchDupLists();
            _dlistAB = 1;
        }

        private void cm8_ComboTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.colDupli2 = cm8ComboTemplate.SelectedIndex;
            Settings.Default.Save();

        }

        ToolTip toolTip;

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex == -1) return;
            var cell = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];

            if (cell.Value != null)
            {
                dataGridView1.ShowCellToolTips = false;

                //  toolTip = new System.Windows.Forms.ToolTip();
                //  toolTip.InitialDelay = 3000;
                //  toolTip.SetToolTip(dataGridView1, cell.Value.ToString());
            }

        }

        private void dataGridView1_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (toolTip != null) toolTip.Dispose();

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                var cell = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];

                if (cell.Value != null)
                {
                   // int VisibleColCount = 0;

                    //foreach (DataColumn c in dt.Columns)
                    //    if (dataGridView1.Columns[c.ColumnName].Visible) VisibleColCount++;

                    //foreach (DataColumn c in dt.Columns)
                    //{
                    //        dataGridView1.Columns[c.ColumnName].AutoSizeMode = DataGridViewAutoSizeColumnMode.None; //1
                    //                                                                                                //  dataGridView1.Columns[c.ColumnName].Width = 60;
                    //        dataGridView1.Columns[c.ColumnName].Width =
                    //            (dataGridView1.Width - dataGridView1.RowHeadersWidth - SystemInformation.VerticalScrollBarWidth) 
                    //            / VisibleColCount/*dt.Columns.Count*/;

                    //}

                    dataGridView1.ShowCellToolTips = false;

                    toolTip = new System.Windows.Forms.ToolTip();
                    toolTip.InitialDelay = 1000;
                    // toolTip.AutoPopDelay = 5000;
                    toolTip.SetToolTip(dataGridView1, cell.Value.ToString());  //test
                }

            }

        }

        private void cm3_ResizeColumns_Click(object sender, EventArgs e)
        {
            Form1_SizeChanged(sender, e);

        }

        private void cm3_SortRows_Click(object sender, EventArgs e)
        {
            SortRows();

        }

        private void cm8_Template_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //fill combobox
            cm8ComboTemplate.Items.Clear();

            foreach (string s in columnNames1)
            {
                cm8ComboTemplate.Items.Add(s);
            }

           // try     { cm8ComboTemplate.SelectedIndex = cm8ComboTemplate.Items.Count - 2;}
            try     { cm8ComboTemplate.SelectedIndex = Settings.Default.colDupli2;}
            catch   { cm5ColumNames.SelectedIndex = 1; }
        }



        private void cm3_HideColumn_Click(object sender, EventArgs e)
        {
           // foreach (DataGridViewCell cell in dataGridView1.SelectedCells)
            {
                dataGridView1.Columns[dataGridView1.Columns[colIndexFromMouseDown].HeaderText].Visible = false;
            }
            dataGridView1.ClearSelection();  //to avoid right click error afterwards

            Form1_SizeChanged(sender, EventArgs.Empty);

        }

        private void cm3_ShowAllColumns_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                dataGridView1.Columns[dataGridView1.Columns[i].HeaderText].Visible = true;
            }
            Form1_SizeChanged(sender, EventArgs.Empty);

        }

        private void cm3_AddColumn_Click(object sender, EventArgs e)
        {
            string addColumns = cm3_ColCombo.Text;
            if (string.IsNullOrEmpty(addColumns)) return;
            dt.Columns.Add(addColumns).SetOrdinal(0);

            //reorder the Columns
            foreach (DataColumn c in dt.Columns)
            {
                dataGridView1.Columns[c.ColumnName].DisplayIndex = dt.Columns.IndexOf(c);
            }

            cm3_ColCombo.Items.Clear();

        }

        private void cm3_ColCombo_Click(object sender, EventArgs e)
        {
            cm3_ColCombo.Items.Clear();
            cm3_ColCombo.DroppedDown = false;

            List<string> droplist = new List<string>();

            var myClass = new ClassDataset();


            droplist.AddRange(ColTypes);
            droplist.AddRange(myClass.KodiPropTypes);
            droplist.AddRange(myClass.KodiReferrerTypes);

            List<string> colExists = new List<string>();

            foreach (DataColumn c in dt.Columns)
            {
                colExists.Add(c.ColumnName);
            }

            var result = droplist.Except(colExists);

            foreach (string s in result)
            {
                cm3_ColCombo.Items.Add(s);
            }

            cm3_ColCombo.DroppedDown = true;

        }

        private void cm1_addUserAgent_Click(object sender, EventArgs e)
        {
            if (colIndexFromMouseDown == dataGridView1.ColumnCount - 1)
            {
                DataGridViewCell oCell;

                foreach (DataGridViewCell cell in dataGridView1.InvSelectedCells())
                {
                    oCell = dataGridView1[cell.ColumnIndex, cell.RowIndex];
                    string oCellText = oCell.Value.ToString();

                    if ((oCellText.EndsWith("m3u") || oCellText.EndsWith("m3u8")) && !oCellText.Contains("|User-Agent"))
                        oCell.Value = oCellText + "|User-Agent=" + Settings.Default.user_agent;
                }
                SaveButtonUI();

            }

        }

        private void cm1_removeUserAgent_Click(object sender, EventArgs e)
        {
            if (colIndexFromMouseDown == dataGridView1.ColumnCount - 1)
            {
                DataGridViewCell oCell;

                foreach (DataGridViewCell cell in dataGridView1.InvSelectedCells())
                {
                    oCell = dataGridView1[cell.ColumnIndex, cell.RowIndex];
                    string oCellText = oCell.Value.ToString();

                    if (oCellText.Contains("|User-Agent"))
                        oCell.Value = oCellText.Split('|').First();
                }
                SaveButtonUI();
            }

        }

        private void cm1_addTVHeadend_Click(object sender, EventArgs e)
        {
            if (colIndexFromMouseDown == dataGridView1.ColumnCount - 1)
            {
                DataGridViewCell oCell;

                foreach (DataGridViewCell cell in dataGridView1.InvSelectedCells())
                {
                    oCell = dataGridView1[cell.ColumnIndex, cell.RowIndex];
                    string oCellText = oCell.Value.ToString();

                    if (!oCellText.StartsWith("pipe"))  //no double change
                        oCell.Value = tvhead1 + oCellText + tvhead2;
                }
                SaveButtonUI();
            }

        }

        private void cm1_removeTVHeadend_Click(object sender, EventArgs e)
        {
            if (colIndexFromMouseDown == dataGridView1.ColumnCount - 1)
            {
                DataGridViewCell oCell;

                foreach (DataGridViewCell cell in dataGridView1.InvSelectedCells())
                {
                    oCell = dataGridView1[cell.ColumnIndex, cell.RowIndex];
                    string oCellText = oCell.Value.ToString();

                    if (oCellText.StartsWith("pipe"))  //no double change
                        oCell.Value = CleanTVHeadendLink(oCell.Value.ToString());
                }
                SaveButtonUI();
            }

        }

        private void cm_LabelVlc_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            cmMultiview.Checked = Settings.Default.vlcMultiplayer;
            cmFullscreen.Checked = Settings.Default.vlcFullscreen;
           // Settings.Default.Save();

        }

        private void cm_Multiview_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.vlcMultiplayer = cmMultiview.Checked;
            Settings.Default.Save();
        }

        private void cm_Fullscreen_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.vlcFullscreen = cmFullscreen.Checked;
            Settings.Default.Save();
        }

        private void cms1_Smaller_Click(object sender, EventArgs e)
        {
            zoomf -= 0.1F;
            ZoomGrid(zoomf);
        }

        private void cms1_Bigger_Click(object sender, EventArgs e)
        {
            zoomf += 0.1F;
            ZoomGrid(zoomf);
        }

        private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (Cursor.Current == Cursors.SizeWE) return;
            if (NoDataRow) return;
            // Get the index of the item the mouse is below.
            rowIndexFromMouseDown = dataGridView1.HitTest(e.X, e.Y).RowIndex;
            colIndexFromMouseDown = dataGridView1.HitTest(e.X, e.Y).ColumnIndex;

            if (e.Button == MouseButtons.Right && rowIndexFromMouseDown == -1)  
            {
                //click on Column Header
                contextMenuStrip3.Show(dataGridView1, dataGridView1.PointToClient(Cursor.Position));
            }
            if (e.Button == MouseButtons.Left)
            {
                Cursor.Current = Cursors.WaitCursor;

                if (rowIndexFromMouseDown != -1)
                {
                    /// Remember the point where the mouse down occurred. 
                    /// The DragSize indicates the size that the mouse can move 
                    /// before a drag event should be started.                
                    Size dragSize = SystemInformation.DragSize;

                    /// Create a rectangle using the DragSize, with the mouse position being
                    /// at the center of the rectangle.
                    dragBoxFromMouseDown = new Rectangle(
                        new Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);
                }
                else if (colIndexFromMouseDown != -1) 
                {
                    //select full column
                    dataGridView1.ClearSelection();
                    for (int row = 0; row < dataGridView1.Rows.Count; row++)
                    {
                        dataGridView1.Rows[row].Cells[colIndexFromMouseDown].Selected = true;
                    }


                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void SortRows()
        {
            // Reset the rectangle if the mouse is not over an item in the ListBox.
            dragBoxFromMouseDown = Rectangle.Empty;

            SaveButtonUI();

            if (_sort == "desc")
            {
                _sort = "asc";
                dataGridView1.Sort(dataGridView1.Columns[colIndexFromMouseDown], System.ComponentModel.ListSortDirection.Descending);
            }
            else
            {
                _sort = "desc";
                dataGridView1.Sort(dataGridView1.Columns[colIndexFromMouseDown], System.ComponentModel.ListSortDirection.Ascending);
            }

            dt = dt.DefaultView.ToTable(); // The Sorted View converted to DataTable and then assigned to table object.
            dt = dt.DefaultView.ToTable("IPTV");

            //#25 rebind after sort
            dataGridView1.DataSource = dt;
            dataGridView1.Refresh();

            if (IsLinkChecked) PaintRows();  //#41
            if (IsDupChecked) button_dup.PerformClick();

            dataGridView1.ClearSelection();

        }

        private void dataGridView1_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Right) == MouseButtons.Right)
            {
                /// If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty &&
                    !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    /// Proceed with the drag and drop, passing in the list item.
                    DragDropEffects dropEffect = dataGridView1.DoDragDrop(
                    dataGridView1.Rows[rowIndexFromMouseDown],
                    DragDropEffects.Move);
                }
            }
        }

        private void dataGridView1_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            SaveButtonUI();
        }


        private DataTable CreateDataTableNew(DataTable data, List<ColList> elements)
        {
            if (data.Rows.Count == 0)
            {
                data.Clear();  // row clear
                data.Columns.Clear();  // col clear

                foreach (ColList name in elements)
                {
                    data.Columns.Add(name.ColName.ToString());
                }

                data.Columns.Add("Name2");
                data.Columns.Add("Link");
            }
            else  //Add new Column
            {
                if (data.Columns.Count - 2 < elements.Count)
                {
                    string allheaders = "";

                    for (int j = 0; j < data.Columns.Count - 2; j++)
                    {
                        allheaders += data.Columns[j].ToString();
                    }

                    foreach (ColList name in elements)
                    {
                        if (!allheaders.Contains(name.ColName.ToString()))
                        {
                            data.Columns.Add(name.ColName.ToString()).SetOrdinal(0);

                        }
                    }

                    //reorder the Columns
                    foreach (DataColumn c in data.Columns)
                    {
                        dataGridView1.Columns[c.ColumnName].DisplayIndex = data.Columns.IndexOf(c);
                    }


                }
            }

            columnNames1.Clear();

            foreach (DataColumn c in data.Columns)
            {
                columnNames1.Add(c.ColumnName);
            }

            return data;
        }
        private void CreateDataTable(List<ColList> elements)
        {
            if (NoDataRow)
            {
                dt.Clear();  // row clear
                dt.Columns.Clear();  // col clear

                foreach (ColList name in elements)
                {
                    dt.Columns.Add(name.ColName.ToString());
                }

                dt.Columns.Add("Name2");
                dt.Columns.Add("Link");

                foreach (DataColumn c in dt.Columns)
                {
                    dataGridView1.Columns[c.ColumnName].AutoSizeMode = DataGridViewAutoSizeColumnMode.None; //
                    //  dataGridView1.Columns[c.ColumnName].Width = 60;
                    dataGridView1.Columns[c.ColumnName].Width =
                        (dataGridView1.Width - dataGridView1.RowHeadersWidth - SystemInformation.VerticalScrollBarWidth) / dt.Columns.Count;

                }

            }
            else  //Add new Column
            {
                if (dataGridView1.ColumnCount - 2 < elements.Count)
                {
                    string allheaders = "";

                    for (int j = 0; j < dataGridView1.ColumnCount - 2; j++)
                    {
                        allheaders += dataGridView1.Columns[j].ToString();
                    }

                    foreach (ColList name in elements)
                    {
                        if (!allheaders.Contains(name.ColName.ToString()))
                        {
                            dt.Columns.Add(name.ColName.ToString()).SetOrdinal(0);

                        }
                    }

                    //reorder the Columns
                    foreach (DataColumn c in dt.Columns)
                    {
                        dataGridView1.Columns[c.ColumnName].DisplayIndex = dt.Columns.IndexOf(c);
                    }


                }
            }

            //columnNames.Clear();

            //foreach (DataColumn c in dt.Columns)
            //{
            //    columnNames.Add(c.ColumnName);
            //}

        }

        /// <summary>
        /// set flags for special lines
        /// </summary>
        private List<ColList> SetColumnFlags()
        {
            var myClass = new ClassDataset();

            List<ColList> colList = new List<ColList>();
            int linetype = 0;

            for (int j = 0; j < dt.Columns.Count; j++)
            {
                linetype = 0;
                string header = dt.Columns[j].ToString();


                for (int k = 0; k < myClass.KodiPropTypes.Length; k++)
                {
                    if (header.Equals(myClass.KodiPropTypes[k].ToString()))
                    {
                        IsKodiPropCol = true;
                        linetype = 2;
                        break;
                    }
                }

                for (int k = 0; k < myClass.KodiReferrerTypes.Length; k++)
                {
                    if (header.Equals(myClass.KodiReferrerTypes[k].ToString()))
                    {
                        IsReferrerCol = true;
                        linetype = 1;
                          break;
                    }

                }



                colList.Add(new ColList
                {
                    ColName = dt.Columns[j].ToString(),
                    Visible = dataGridView1.Columns[j].Visible,
                    LineType = linetype,
                    ColNumber = j
                });

            }



            return colList;

        }



        private bool ComposeFilterList()
        {
            ///string[] -> dt2 -> dgv

            if (tabControl1.TabPages.Contains(tabPage2)) return false;

            dt2 = new DataTable();
            var filename = ClassOpenFile.OpenFileUI(Settings.Default.openpath);
            if (filename == null) return false;

            Cursor.Current = Cursors.WaitCursor;

            if (filename != null)
            {
                var filterfile = File.ReadAllText(filename);
                var filterRows = filterfile.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                
               // filterRows = filterRows[..^1]; //C# 8.0
               // filterRows = filterRows.Reverse().Skip(1).Reverse().ToArray();
                dt2 = ClassImportData.CreateTableColumns(SeekFileElements(filterfile));  //possible ERROR no column width 

                dt2 = ClassImportData.ImportData(dt2,filterRows);

            }
            Cursor.Current = Cursors.Default;

            return true;
        }
       ///here new methods

    }


    }



/// <summary>
/// class for streamcheck semaphore
/// </summary>
internal class Check
{
    public void streamchk(string ipUrl)
    {
        CheckIPTVStream(ipUrl);

        return;
    }
}


/// <summary>
/// DataGridView Method extensions
/// </summary>
public static class ExtensionMethods
{
    /// <summary>
    /// Get all Cotrols of Form
    /// </summary>
    /// <param name="root"></param>
    /// <returns></returns>
    public static IEnumerable<Control> GetAllChildren(this Control root)
    {
        var stack = new Stack<Control>();
        stack.Push(root);

        while (stack.Any())
        {
            var next = stack.Pop();
            foreach (Control child in next.Controls)
                stack.Push(child);
            yield return next;
        }
    }

    /// <summary>
    /// double buffer on for large files speed up
    /// </summary>
    /// <param name="dgv"></param>
    /// <param name="setting"></param>
    public static void DoubleBuffered(this DataGridView dgv, bool setting)
    {
        //http://bitmatic.com/c/fixing-a-slow-scrolling-datagridview

        Type dgvType = dgv.GetType();
        PropertyInfo pi = dgvType.GetProperty("DoubleBuffered",
            BindingFlags.Instance | BindingFlags.NonPublic);
        pi.SetValue(dgv, setting, null);
    }

    /// <summary>
    /// reverse order of selected rows for foreach
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static IEnumerable<DataGridViewRow> InvSelectedRows(this DataGridView source)
    {
        for (int i = source.SelectedRows.Count - 1; i >= 0; i--)
            yield return source.SelectedRows[i];
    }

    /// <summary>
    /// inverse order of selected cells for foreach
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static IEnumerable<DataGridViewCell> InvSelectedCells(this DataGridView source)
    {
        for (int i = source.SelectedCells.Count - 1; i >= 0; i--)
            yield return source.SelectedCells[i];
    }

    /// <summary>
    /// inverse order of rows for foreach
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static IEnumerable<DataGridViewRow> InvRows(this DataGridView source)
    {
        for (int i = source.Rows.Count - 1; i >= 0; i--)
            yield return source.Rows[i];
    }

    /// <summary>
    /// inverse order of rows for foreach
    /// </summary>
    /// <param name="source">string</param>
    /// <returns>string</returns>
    public static IEnumerable<string> InvRows(this string[] source)  //#44
    {
        for (int i = source.Length - 1; i >= 0; i--)
            yield return source[i];
    }

    /// <summary>
    /// removes first lines of string
    /// </summary>
    /// <param name="text"></param>
    /// <param name="linesCount"></param>
    /// <returns></returns>
    public static string RemoveFirstLines(this string text, int linesCount)
    {
        var lines = Regex.Split(text, "\r\n|\r|\n").Skip(linesCount);
        return string.Join(Environment.NewLine, lines.ToArray());
    }
}