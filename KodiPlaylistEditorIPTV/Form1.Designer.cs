﻿namespace PlaylistEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.cutRowMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteRowMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.playToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cm1_hideColumn = new System.Windows.Forms.ToolStripMenuItem();
            this.cm1_showColumns = new System.Windows.Forms.ToolStripMenuItem();
            this.cm1NewColumn = new System.Windows.Forms.ToolStripMenuItem();
            this.cm1ColCombo = new System.Windows.Forms.ToolStripComboBox();
            this.cm1AddColumn = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.cms1NewWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.cms1Number = new System.Windows.Forms.ToolStripMenuItem();
            this.fontSIzeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cms1Bigger = new System.Windows.Forms.ToolStripMenuItem();
            this.cms1Smaller = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.cm1_UserAgent = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cm1_removeUserAgent = new System.Windows.Forms.ToolStripMenuItem();
            this.editStringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cm1_TVHeadend = new System.Windows.Forms.ToolStripMenuItem();
            this.cm1_addTVHeadend = new System.Windows.Forms.ToolStripMenuItem();
            this.cm1_removeTVHeadend = new System.Windows.Forms.ToolStripMenuItem();
            this.editStringToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnSelectList = new System.Windows.Forms.Button();
            this.textBox_find = new System.Windows.Forms.TextBox();
            this.btnTemplate = new PlaylistEditor.MyButton();
            this.cm5_Duplicate = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cm5ColumNames = new System.Windows.Forms.ToolStripComboBox();
            this.cm5StartSearchDupli = new System.Windows.Forms.ToolStripMenuItem();
            this.button_revert = new PlaylistEditor.MyButton();
            this.button_kodi = new PlaylistEditor.MyButton();
            this.button_import = new PlaylistEditor.MyButton();
            this.RedoButton = new PlaylistEditor.MyButton();
            this.UndoButton = new PlaylistEditor.MyButton();
            this.button_check = new PlaylistEditor.MyButton();
            this.button_vlc = new PlaylistEditor.MyButton();
            this.cmLabelVlc = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.cmFullscreen = new System.Windows.Forms.ToolStripMenuItem();
            this.cmMultiview = new System.Windows.Forms.ToolStripMenuItem();
            this.button_dup = new PlaylistEditor.MyButton();
            this.button_search = new PlaylistEditor.MyButton();
            this.button_moveDown = new RepeatingButton();
            this.button_moveUp = new RepeatingButton();
            this.button_del_all = new PlaylistEditor.MyButton();
            this.button_settings = new PlaylistEditor.MyButton();
            this.button_add = new PlaylistEditor.MyButton();
            this.button_Info = new PlaylistEditor.MyButton();
            this.button_delLine = new PlaylistEditor.MyButton();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cm2_addUseragentCell = new System.Windows.Forms.ToolStripMenuItem();
            this.editCellCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.editCellPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.editCellCut = new System.Windows.Forms.ToolStripMenuItem();
            this.label_central = new System.Windows.Forms.Label();
            this.contextMenuStrip3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cm3_SortRows = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cm3_HideColumn = new System.Windows.Forms.ToolStripMenuItem();
            this.cm3_ShowAllColumns = new System.Windows.Forms.ToolStripMenuItem();
            this.cm3_ResizeColumns = new System.Windows.Forms.ToolStripMenuItem();
            this.newColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cm3_ColCombo = new System.Windows.Forms.ToolStripComboBox();
            this.cm3_AddColumn = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.cm3Scrollbar = new System.Windows.Forms.ToolStripMenuItem();
            this.cm3EditF2 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip4 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cm4EditFIleHeader = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblEncoding = new System.Windows.Forms.Label();
            this.panel_Find = new System.Windows.Forms.Panel();
            this.button_clearfind = new PlaylistEditor.MyButton();
            this.button_refind = new PlaylistEditor.MyButton();
            this.lblColCheck = new System.Windows.Forms.Label();
            this.lblRowCheck = new System.Windows.Forms.Label();
            this.plabel_Filename = new PathLabel();
            this.lblNoRows = new System.Windows.Forms.Label();
            this.cm6_Save = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cm6Save = new System.Windows.Forms.ToolStripMenuItem();
            this.cm6SaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.cm6EditFileHeader = new System.Windows.Forms.ToolStripMenuItem();
            this.cm7_Open = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cm7Open = new System.Windows.Forms.ToolStripMenuItem();
            this.cm7Append = new System.Windows.Forms.ToolStripMenuItem();
            this.cm7NewWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.cm7LoadTemplate = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.cm8_Template = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cm8ComboTemplate = new System.Windows.Forms.ToolStripComboBox();
            this.cm8UseTemplate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.cm8_CloseCompare = new System.Windows.Forms.ToolStripMenuItem();
            this.button_save = new PlaylistEditor.MyButton();
            this.button_open = new PlaylistEditor.MyButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.cm5_Duplicate.SuspendLayout();
            this.cmLabelVlc.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.contextMenuStrip3.SuspendLayout();
            this.contextMenuStrip4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel_Find.SuspendLayout();
            this.cm6_Save.SuspendLayout();
            this.cm7_Open.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.cm8_Template.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowDrop = true;
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.Gray;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            resources.ApplyResources(this.dataGridView1, "dataGridView1");
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDoubleClick);
            this.dataGridView1.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseEnter);
            this.dataGridView1.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseLeave);
            this.dataGridView1.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridView1_CellPainting);
            this.dataGridView1.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellValidated);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView1_DataBindingComplete);
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            this.dataGridView1.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView1_RowsAdded);
            this.dataGridView1.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView1_RowsRemoved);
            this.dataGridView1.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragDrop);
            this.dataGridView1.DragEnter += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragEnter);
            this.dataGridView1.DragOver += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragOver);
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseDown);
            this.dataGridView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseMove);
            // 
            // contextMenuStrip1
            // 
            resources.ApplyResources(this.contextMenuStrip1, "contextMenuStrip1");
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripCopy,
            this.toolStripPaste,
            this.cutRowMenuItem,
            this.pasteRowMenuItem,
            this.toolStripSeparator2,
            this.playToolStripMenuItem,
            this.toolStripSeparator3,
            this.cm1_hideColumn,
            this.cm1_showColumns,
            this.cm1NewColumn,
            this.toolStripSeparator4,
            this.cms1NewWindow,
            this.cms1Number,
            this.fontSIzeToolStripMenuItem,
            this.toolStripSeparator10,
            this.cm1_UserAgent,
            this.cm1_TVHeadend});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // toolStripCopy
            // 
            resources.ApplyResources(this.toolStripCopy, "toolStripCopy");
            this.toolStripCopy.Name = "toolStripCopy";
            this.toolStripCopy.Click += new System.EventHandler(this.toolStripCopy_Click);
            // 
            // toolStripPaste
            // 
            resources.ApplyResources(this.toolStripPaste, "toolStripPaste");
            this.toolStripPaste.Name = "toolStripPaste";
            this.toolStripPaste.Click += new System.EventHandler(this.toolStripPaste_Click);
            // 
            // cutRowMenuItem
            // 
            resources.ApplyResources(this.cutRowMenuItem, "cutRowMenuItem");
            this.cutRowMenuItem.Name = "cutRowMenuItem";
            this.cutRowMenuItem.Click += new System.EventHandler(this.cutRowMenuItem_Click);
            // 
            // pasteRowMenuItem
            // 
            resources.ApplyResources(this.pasteRowMenuItem, "pasteRowMenuItem");
            this.pasteRowMenuItem.Name = "pasteRowMenuItem";
            this.pasteRowMenuItem.Click += new System.EventHandler(this.pasteRowMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            // 
            // playToolStripMenuItem
            // 
            this.playToolStripMenuItem.Name = "playToolStripMenuItem";
            resources.ApplyResources(this.playToolStripMenuItem, "playToolStripMenuItem");
            this.playToolStripMenuItem.Click += new System.EventHandler(this.playToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            // 
            // cm1_hideColumn
            // 
            resources.ApplyResources(this.cm1_hideColumn, "cm1_hideColumn");
            this.cm1_hideColumn.Name = "cm1_hideColumn";
            this.cm1_hideColumn.Click += new System.EventHandler(this.cm1_hideColumn_Click);
            // 
            // cm1_showColumns
            // 
            resources.ApplyResources(this.cm1_showColumns, "cm1_showColumns");
            this.cm1_showColumns.Name = "cm1_showColumns";
            this.cm1_showColumns.Click += new System.EventHandler(this.cm1_showColumns_Click);
            // 
            // cm1NewColumn
            // 
            this.cm1NewColumn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cm1ColCombo,
            this.cm1AddColumn});
            this.cm1NewColumn.Name = "cm1NewColumn";
            resources.ApplyResources(this.cm1NewColumn, "cm1NewColumn");
            // 
            // cm1ColCombo
            // 
            this.cm1ColCombo.DropDownHeight = 200;
            this.cm1ColCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cm1ColCombo, "cm1ColCombo");
            this.cm1ColCombo.Name = "cm1ColCombo";
            this.cm1ColCombo.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.cm1ColCombo.Click += new System.EventHandler(this.cm1_ColCombo_Click);
            // 
            // cm1AddColumn
            // 
            resources.ApplyResources(this.cm1AddColumn, "cm1AddColumn");
            this.cm1AddColumn.Name = "cm1AddColumn";
            this.cm1AddColumn.Click += new System.EventHandler(this.cm1_AddColumn_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            // 
            // cms1NewWindow
            // 
            this.cms1NewWindow.Name = "cms1NewWindow";
            resources.ApplyResources(this.cms1NewWindow, "cms1NewWindow");
            this.cms1NewWindow.Click += new System.EventHandler(this.newWindowToolStripMenuItem_Click);
            // 
            // cms1Number
            // 
            resources.ApplyResources(this.cms1Number, "cms1Number");
            this.cms1Number.Name = "cms1Number";
            this.cms1Number.Click += new System.EventHandler(this.cm1_Number_Click);
            // 
            // fontSIzeToolStripMenuItem
            // 
            this.fontSIzeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cms1Bigger,
            this.cms1Smaller});
            resources.ApplyResources(this.fontSIzeToolStripMenuItem, "fontSIzeToolStripMenuItem");
            this.fontSIzeToolStripMenuItem.Name = "fontSIzeToolStripMenuItem";
            // 
            // cms1Bigger
            // 
            resources.ApplyResources(this.cms1Bigger, "cms1Bigger");
            this.cms1Bigger.Name = "cms1Bigger";
            this.cms1Bigger.Click += new System.EventHandler(this.cms1_Bigger_Click);
            // 
            // cms1Smaller
            // 
            resources.ApplyResources(this.cms1Smaller, "cms1Smaller");
            this.cms1Smaller.Name = "cms1Smaller";
            this.cms1Smaller.Click += new System.EventHandler(this.cms1_Smaller_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            resources.ApplyResources(this.toolStripSeparator10, "toolStripSeparator10");
            // 
            // cm1_UserAgent
            // 
            this.cm1_UserAgent.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.cm1_removeUserAgent,
            this.editStringToolStripMenuItem});
            resources.ApplyResources(this.cm1_UserAgent, "cm1_UserAgent");
            this.cm1_UserAgent.Name = "cm1_UserAgent";
            this.cm1_UserAgent.Click += new System.EventHandler(this.cm1_addUserAgent_Click);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            resources.ApplyResources(this.addToolStripMenuItem, "addToolStripMenuItem");
            this.addToolStripMenuItem.Click += new System.EventHandler(this.cm1_addUserAgent_Click);
            // 
            // cm1_removeUserAgent
            // 
            this.cm1_removeUserAgent.Name = "cm1_removeUserAgent";
            resources.ApplyResources(this.cm1_removeUserAgent, "cm1_removeUserAgent");
            this.cm1_removeUserAgent.Click += new System.EventHandler(this.cm1_removeUserAgent_Click);
            // 
            // editStringToolStripMenuItem
            // 
            this.editStringToolStripMenuItem.Name = "editStringToolStripMenuItem";
            resources.ApplyResources(this.editStringToolStripMenuItem, "editStringToolStripMenuItem");
            this.editStringToolStripMenuItem.Click += new System.EventHandler(this.cm6_TVHeadend_Click);
            // 
            // cm1_TVHeadend
            // 
            this.cm1_TVHeadend.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cm1_addTVHeadend,
            this.cm1_removeTVHeadend,
            this.editStringToolStripMenuItem1});
            resources.ApplyResources(this.cm1_TVHeadend, "cm1_TVHeadend");
            this.cm1_TVHeadend.Name = "cm1_TVHeadend";
            this.cm1_TVHeadend.Click += new System.EventHandler(this.cm1_addTVHeadend_Click);
            // 
            // cm1_addTVHeadend
            // 
            this.cm1_addTVHeadend.Name = "cm1_addTVHeadend";
            resources.ApplyResources(this.cm1_addTVHeadend, "cm1_addTVHeadend");
            this.cm1_addTVHeadend.Click += new System.EventHandler(this.cm1_addTVHeadend_Click);
            // 
            // cm1_removeTVHeadend
            // 
            this.cm1_removeTVHeadend.Name = "cm1_removeTVHeadend";
            resources.ApplyResources(this.cm1_removeTVHeadend, "cm1_removeTVHeadend");
            this.cm1_removeTVHeadend.Click += new System.EventHandler(this.cm1_removeTVHeadend_Click);
            // 
            // editStringToolStripMenuItem1
            // 
            this.editStringToolStripMenuItem1.Name = "editStringToolStripMenuItem1";
            resources.ApplyResources(this.editStringToolStripMenuItem1, "editStringToolStripMenuItem1");
            this.editStringToolStripMenuItem1.Click += new System.EventHandler(this.cm6_TVHeadend_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "m3u";
            resources.ApplyResources(this.openFileDialog1, "openFileDialog1");
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "m3u";
            resources.ApplyResources(this.saveFileDialog1, "saveFileDialog1");
            // 
            // btnSelectList
            // 
            resources.ApplyResources(this.btnSelectList, "btnSelectList");
            this.btnSelectList.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSelectList.FlatAppearance.BorderSize = 0;
            this.btnSelectList.Name = "btnSelectList";
            this.toolTip1.SetToolTip(this.btnSelectList, resources.GetString("btnSelectList.ToolTip"));
            this.btnSelectList.UseVisualStyleBackColor = false;
            this.btnSelectList.Click += new System.EventHandler(this.btnSelectList_Click);
            // 
            // textBox_find
            // 
            this.textBox_find.BackColor = System.Drawing.SystemColors.Menu;
            resources.ApplyResources(this.textBox_find, "textBox_find");
            this.textBox_find.Name = "textBox_find";
            this.toolTip1.SetToolTip(this.textBox_find, resources.GetString("textBox_find.ToolTip"));
            this.textBox_find.TextChanged += new System.EventHandler(this.textBox_find_TextChange);
            this.textBox_find.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_find_KeyPress);
            // 
            // btnTemplate
            // 
            resources.ApplyResources(this.btnTemplate, "btnTemplate");
            this.btnTemplate.BackColor = System.Drawing.Color.MidnightBlue;
            this.btnTemplate.ContextMenuStrip = this.cm5_Duplicate;
            this.btnTemplate.FlatAppearance.BorderSize = 0;
            this.btnTemplate.Name = "btnTemplate";
            this.toolTip1.SetToolTip(this.btnTemplate, resources.GetString("btnTemplate.ToolTip"));
            this.btnTemplate.UseVisualStyleBackColor = true;
            this.btnTemplate.Click += new System.EventHandler(this.btnTemplate_Click);
            // 
            // cm5_Duplicate
            // 
            this.cm5_Duplicate.BackColor = System.Drawing.Color.MidnightBlue;
            this.cm5_Duplicate.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cm5_Duplicate.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cm5ColumNames,
            this.cm5StartSearchDupli});
            this.cm5_Duplicate.Name = "contextMenuStrip5";
            this.cm5_Duplicate.ShowImageMargin = false;
            resources.ApplyResources(this.cm5_Duplicate, "cm5_Duplicate");
            this.cm5_Duplicate.Opening += new System.ComponentModel.CancelEventHandler(this.cm5_Duplicate_Opening);
            // 
            // cm5ColumNames
            // 
            this.cm5ColumNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cm5ColumNames, "cm5ColumNames");
            this.cm5ColumNames.Name = "cm5ColumNames";
            this.cm5ColumNames.SelectedIndexChanged += new System.EventHandler(this.cm5_ColumNames_SelectedIndexChanged);
            // 
            // cm5StartSearchDupli
            // 
            resources.ApplyResources(this.cm5StartSearchDupli, "cm5StartSearchDupli");
            this.cm5StartSearchDupli.ForeColor = System.Drawing.SystemColors.Window;
            this.cm5StartSearchDupli.Name = "cm5StartSearchDupli";
            this.cm5StartSearchDupli.Click += new System.EventHandler(this.cm5_StartSearchDupli_Click);
            // 
            // button_revert
            // 
            resources.ApplyResources(this.button_revert, "button_revert");
            this.button_revert.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button_revert.FlatAppearance.BorderSize = 0;
            this.button_revert.Name = "button_revert";
            this.toolTip1.SetToolTip(this.button_revert, resources.GetString("button_revert.ToolTip"));
            this.button_revert.UseVisualStyleBackColor = false;
            this.button_revert.Click += new System.EventHandler(this.button_revert_Click);
            // 
            // button_kodi
            // 
            resources.ApplyResources(this.button_kodi, "button_kodi");
            this.button_kodi.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_kodi.FlatAppearance.BorderSize = 0;
            this.button_kodi.Image = global::PlaylistEditor.Properties.Resources.arrow_right_circle_outline_2_r;
            this.button_kodi.Name = "button_kodi";
            this.toolTip1.SetToolTip(this.button_kodi, resources.GetString("button_kodi.ToolTip"));
            this.button_kodi.UseVisualStyleBackColor = true;
            this.button_kodi.Click += new System.EventHandler(this.button_kodi_Click);
            // 
            // button_import
            // 
            resources.ApplyResources(this.button_import, "button_import");
            this.button_import.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_import.FlatAppearance.BorderSize = 0;
            this.button_import.Image = global::PlaylistEditor.Properties.Resources.import_r;
            this.button_import.Name = "button_import";
            this.toolTip1.SetToolTip(this.button_import, resources.GetString("button_import.ToolTip"));
            this.button_import.UseVisualStyleBackColor = true;
            this.button_import.Click += new System.EventHandler(this.button_import_Click);
            // 
            // RedoButton
            // 
            this.RedoButton.BackColor = System.Drawing.Color.MidnightBlue;
            resources.ApplyResources(this.RedoButton, "RedoButton");
            this.RedoButton.FlatAppearance.BorderSize = 0;
            this.RedoButton.Image = global::PlaylistEditor.Properties.Resources.redo_r;
            this.RedoButton.Name = "RedoButton";
            this.toolTip1.SetToolTip(this.RedoButton, resources.GetString("RedoButton.ToolTip"));
            this.RedoButton.UseVisualStyleBackColor = true;
            this.RedoButton.Click += new System.EventHandler(this.RedoButton_Click);
            // 
            // UndoButton
            // 
            this.UndoButton.BackColor = System.Drawing.Color.MidnightBlue;
            resources.ApplyResources(this.UndoButton, "UndoButton");
            this.UndoButton.FlatAppearance.BorderSize = 0;
            this.UndoButton.Image = global::PlaylistEditor.Properties.Resources.undo_r;
            this.UndoButton.Name = "UndoButton";
            this.toolTip1.SetToolTip(this.UndoButton, resources.GetString("UndoButton.ToolTip"));
            this.UndoButton.UseVisualStyleBackColor = true;
            this.UndoButton.Click += new System.EventHandler(this.UndoButton_Click);
            // 
            // button_check
            // 
            resources.ApplyResources(this.button_check, "button_check");
            this.button_check.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_check.FlatAppearance.BorderSize = 0;
            this.button_check.Image = global::PlaylistEditor.Properties.Resources.playlist_check_r;
            this.button_check.Name = "button_check";
            this.toolTip1.SetToolTip(this.button_check, resources.GetString("button_check.ToolTip"));
            this.button_check.UseVisualStyleBackColor = true;
            this.button_check.Click += new System.EventHandler(this.button_check_Click);
            // 
            // button_vlc
            // 
            resources.ApplyResources(this.button_vlc, "button_vlc");
            this.button_vlc.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_vlc.ContextMenuStrip = this.cmLabelVlc;
            this.button_vlc.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_vlc.FlatAppearance.BorderSize = 0;
            this.button_vlc.Image = global::PlaylistEditor.Properties.Resources.arrow_right_drop_circle_outline_r;
            this.button_vlc.Name = "button_vlc";
            this.toolTip1.SetToolTip(this.button_vlc, resources.GetString("button_vlc.ToolTip"));
            this.button_vlc.UseVisualStyleBackColor = true;
            this.button_vlc.Click += new System.EventHandler(this.button_vlc_Click);
            // 
            // cmLabelVlc
            // 
            resources.ApplyResources(this.cmLabelVlc, "cmLabelVlc");
            this.cmLabelVlc.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.cmLabelVlc.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripSeparator7,
            this.cmFullscreen,
            this.cmMultiview});
            this.cmLabelVlc.Name = "cmLabelVlc";
            this.cmLabelVlc.ShowCheckMargin = true;
            this.cmLabelVlc.ShowImageMargin = false;
            this.cmLabelVlc.Opening += new System.ComponentModel.CancelEventHandler(this.cm_LabelVlc_Opening);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            resources.ApplyResources(this.toolStripSeparator7, "toolStripSeparator7");
            // 
            // cmFullscreen
            // 
            this.cmFullscreen.CheckOnClick = true;
            this.cmFullscreen.Name = "cmFullscreen";
            resources.ApplyResources(this.cmFullscreen, "cmFullscreen");
            this.cmFullscreen.CheckedChanged += new System.EventHandler(this.cm_Fullscreen_CheckedChanged);
            // 
            // cmMultiview
            // 
            this.cmMultiview.CheckOnClick = true;
            this.cmMultiview.Name = "cmMultiview";
            resources.ApplyResources(this.cmMultiview, "cmMultiview");
            this.cmMultiview.CheckedChanged += new System.EventHandler(this.cm_Multiview_CheckedChanged);
            // 
            // button_dup
            // 
            resources.ApplyResources(this.button_dup, "button_dup");
            this.button_dup.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_dup.FlatAppearance.BorderSize = 0;
            this.button_dup.Image = global::PlaylistEditor.Properties.Resources.content_duplicate_r;
            this.button_dup.Name = "button_dup";
            this.toolTip1.SetToolTip(this.button_dup, resources.GetString("button_dup.ToolTip"));
            this.button_dup.UseVisualStyleBackColor = true;
            this.button_dup.Click += new System.EventHandler(this.button_dup_Click);
            // 
            // button_search
            // 
            resources.ApplyResources(this.button_search, "button_search");
            this.button_search.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_search.FlatAppearance.BorderSize = 0;
            this.button_search.Image = global::PlaylistEditor.Properties.Resources.table_search_r;
            this.button_search.Name = "button_search";
            this.toolTip1.SetToolTip(this.button_search, resources.GetString("button_search.ToolTip"));
            this.button_search.UseVisualStyleBackColor = true;
            this.button_search.Click += new System.EventHandler(this.button_search_Click);
            // 
            // button_moveDown
            // 
            resources.ApplyResources(this.button_moveDown, "button_moveDown");
            this.button_moveDown.FlatAppearance.BorderSize = 0;
            this.button_moveDown.Image = global::PlaylistEditor.Properties.Resources.arrow_down_bold_r;
            this.button_moveDown.Name = "button_moveDown";
            this.toolTip1.SetToolTip(this.button_moveDown, resources.GetString("button_moveDown.ToolTip"));
            this.button_moveDown.UseVisualStyleBackColor = true;
            this.button_moveDown.Click += new System.EventHandler(this.button_moveDown_Click);
            // 
            // button_moveUp
            // 
            resources.ApplyResources(this.button_moveUp, "button_moveUp");
            this.button_moveUp.FlatAppearance.BorderSize = 0;
            this.button_moveUp.Image = global::PlaylistEditor.Properties.Resources.arrow_up_bold_r;
            this.button_moveUp.Name = "button_moveUp";
            this.toolTip1.SetToolTip(this.button_moveUp, resources.GetString("button_moveUp.ToolTip"));
            this.button_moveUp.UseVisualStyleBackColor = true;
            this.button_moveUp.Click += new System.EventHandler(this.button_moveUp_Click);
            // 
            // button_del_all
            // 
            resources.ApplyResources(this.button_del_all, "button_del_all");
            this.button_del_all.FlatAppearance.BorderSize = 0;
            this.button_del_all.Image = global::PlaylistEditor.Properties.Resources.delete_forever_outline_r;
            this.button_del_all.Name = "button_del_all";
            this.toolTip1.SetToolTip(this.button_del_all, resources.GetString("button_del_all.ToolTip"));
            this.button_del_all.UseVisualStyleBackColor = true;
            this.button_del_all.Click += new System.EventHandler(this.button_del_all_Click);
            // 
            // button_settings
            // 
            resources.ApplyResources(this.button_settings, "button_settings");
            this.button_settings.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_settings.FlatAppearance.BorderSize = 0;
            this.button_settings.Image = global::PlaylistEditor.Properties.Resources.settings_outline_r;
            this.button_settings.Name = "button_settings";
            this.toolTip1.SetToolTip(this.button_settings, resources.GetString("button_settings.ToolTip"));
            this.button_settings.UseVisualStyleBackColor = true;
            this.button_settings.Click += new System.EventHandler(this.button_settings_Click);
            // 
            // button_add
            // 
            resources.ApplyResources(this.button_add, "button_add");
            this.button_add.FlatAppearance.BorderSize = 0;
            this.button_add.Image = global::PlaylistEditor.Properties.Resources.playlist_plus_r;
            this.button_add.Name = "button_add";
            this.toolTip1.SetToolTip(this.button_add, resources.GetString("button_add.ToolTip"));
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // button_Info
            // 
            resources.ApplyResources(this.button_Info, "button_Info");
            this.button_Info.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_Info.FlatAppearance.BorderSize = 0;
            this.button_Info.Image = global::PlaylistEditor.Properties.Resources.information_outline_r;
            this.button_Info.Name = "button_Info";
            this.toolTip1.SetToolTip(this.button_Info, resources.GetString("button_Info.ToolTip"));
            this.button_Info.UseVisualStyleBackColor = true;
            this.button_Info.Click += new System.EventHandler(this.button_Info_Click);
            // 
            // button_delLine
            // 
            resources.ApplyResources(this.button_delLine, "button_delLine");
            this.button_delLine.FlatAppearance.BorderSize = 0;
            this.button_delLine.Image = global::PlaylistEditor.Properties.Resources.close_box_outline_r;
            this.button_delLine.Name = "button_delLine";
            this.toolTip1.SetToolTip(this.button_delLine, resources.GetString("button_delLine.ToolTip"));
            this.button_delLine.UseVisualStyleBackColor = true;
            this.button_delLine.Click += new System.EventHandler(this.button_delLine_Click);
            // 
            // contextMenuStrip2
            // 
            resources.ApplyResources(this.contextMenuStrip2, "contextMenuStrip2");
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cm2_addUseragentCell,
            this.editCellCopy,
            this.editCellPaste,
            this.editCellCut});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip2_Opening);
            // 
            // cm2_addUseragentCell
            // 
            this.cm2_addUseragentCell.Name = "cm2_addUseragentCell";
            resources.ApplyResources(this.cm2_addUseragentCell, "cm2_addUseragentCell");
            this.cm2_addUseragentCell.Click += new System.EventHandler(this.cm2_addUseragentCell_Click);
            // 
            // editCellCopy
            // 
            this.editCellCopy.Name = "editCellCopy";
            resources.ApplyResources(this.editCellCopy, "editCellCopy");
            this.editCellCopy.Click += new System.EventHandler(this.editCellCopy_Click);
            // 
            // editCellPaste
            // 
            this.editCellPaste.Name = "editCellPaste";
            resources.ApplyResources(this.editCellPaste, "editCellPaste");
            this.editCellPaste.Click += new System.EventHandler(this.editCellPaste_Click);
            // 
            // editCellCut
            // 
            this.editCellCut.Name = "editCellCut";
            resources.ApplyResources(this.editCellCut, "editCellCut");
            this.editCellCut.Click += new System.EventHandler(this.editCellCut_Click);
            // 
            // label_central
            // 
            resources.ApplyResources(this.label_central, "label_central");
            this.label_central.BackColor = System.Drawing.Color.Gray;
            this.label_central.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_central.Name = "label_central";
            // 
            // contextMenuStrip3
            // 
            resources.ApplyResources(this.contextMenuStrip3, "contextMenuStrip3");
            this.contextMenuStrip3.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.contextMenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cm3_SortRows,
            this.toolStripSeparator1,
            this.cm3_HideColumn,
            this.cm3_ShowAllColumns,
            this.cm3_ResizeColumns,
            this.newColumnToolStripMenuItem,
            this.toolStripSeparator9,
            this.cm3Scrollbar,
            this.cm3EditF2});
            this.contextMenuStrip3.Name = "contextMenuStrip3";
            // 
            // cm3_SortRows
            // 
            this.cm3_SortRows.Name = "cm3_SortRows";
            resources.ApplyResources(this.cm3_SortRows, "cm3_SortRows");
            this.cm3_SortRows.Click += new System.EventHandler(this.cm3_SortRows_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // cm3_HideColumn
            // 
            resources.ApplyResources(this.cm3_HideColumn, "cm3_HideColumn");
            this.cm3_HideColumn.Name = "cm3_HideColumn";
            this.cm3_HideColumn.Click += new System.EventHandler(this.cm3_HideColumn_Click);
            // 
            // cm3_ShowAllColumns
            // 
            resources.ApplyResources(this.cm3_ShowAllColumns, "cm3_ShowAllColumns");
            this.cm3_ShowAllColumns.Name = "cm3_ShowAllColumns";
            this.cm3_ShowAllColumns.Click += new System.EventHandler(this.cm3_ShowAllColumns_Click);
            // 
            // cm3_ResizeColumns
            // 
            this.cm3_ResizeColumns.Name = "cm3_ResizeColumns";
            resources.ApplyResources(this.cm3_ResizeColumns, "cm3_ResizeColumns");
            this.cm3_ResizeColumns.Click += new System.EventHandler(this.cm3_ResizeColumns_Click);
            // 
            // newColumnToolStripMenuItem
            // 
            this.newColumnToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cm3_ColCombo,
            this.cm3_AddColumn});
            this.newColumnToolStripMenuItem.Name = "newColumnToolStripMenuItem";
            resources.ApplyResources(this.newColumnToolStripMenuItem, "newColumnToolStripMenuItem");
            // 
            // cm3_ColCombo
            // 
            resources.ApplyResources(this.cm3_ColCombo, "cm3_ColCombo");
            this.cm3_ColCombo.Name = "cm3_ColCombo";
            this.cm3_ColCombo.Click += new System.EventHandler(this.cm3_ColCombo_Click);
            // 
            // cm3_AddColumn
            // 
            this.cm3_AddColumn.Name = "cm3_AddColumn";
            resources.ApplyResources(this.cm3_AddColumn, "cm3_AddColumn");
            this.cm3_AddColumn.Click += new System.EventHandler(this.cm3_AddColumn_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            resources.ApplyResources(this.toolStripSeparator9, "toolStripSeparator9");
            // 
            // cm3Scrollbar
            // 
            this.cm3Scrollbar.Checked = true;
            this.cm3Scrollbar.CheckOnClick = true;
            this.cm3Scrollbar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cm3Scrollbar.Name = "cm3Scrollbar";
            resources.ApplyResources(this.cm3Scrollbar, "cm3Scrollbar");
            this.cm3Scrollbar.CheckStateChanged += new System.EventHandler(this.cm3_Scrollbar_CheckStateChanged);
            // 
            // cm3EditF2
            // 
            this.cm3EditF2.Checked = true;
            this.cm3EditF2.CheckOnClick = true;
            this.cm3EditF2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cm3EditF2.Name = "cm3EditF2";
            resources.ApplyResources(this.cm3EditF2, "cm3EditF2");
            this.cm3EditF2.CheckStateChanged += new System.EventHandler(this.cm3_EditF2_CheckStateChanged);
            // 
            // contextMenuStrip4
            // 
            this.contextMenuStrip4.BackColor = System.Drawing.Color.MidnightBlue;
            this.contextMenuStrip4.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.contextMenuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cm4EditFIleHeader});
            this.contextMenuStrip4.Name = "contextMenuStrip4";
            this.contextMenuStrip4.ShowImageMargin = false;
            resources.ApplyResources(this.contextMenuStrip4, "contextMenuStrip4");
            // 
            // cm4EditFIleHeader
            // 
            resources.ApplyResources(this.cm4EditFIleHeader, "cm4EditFIleHeader");
            this.cm4EditFIleHeader.ForeColor = System.Drawing.SystemColors.Control;
            this.cm4EditFIleHeader.Name = "cm4EditFIleHeader";
            this.cm4EditFIleHeader.Click += new System.EventHandler(this.cm4_EditFIleHeader_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ControlLight;
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.lblEncoding, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel_Find, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSelectList, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_revert, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.plabel_Filename, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNoRows, 3, 0);
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // lblEncoding
            // 
            resources.ApplyResources(this.lblEncoding, "lblEncoding");
            this.lblEncoding.Name = "lblEncoding";
            // 
            // panel_Find
            // 
            resources.ApplyResources(this.panel_Find, "panel_Find");
            this.panel_Find.Controls.Add(this.textBox_find);
            this.panel_Find.Controls.Add(this.button_clearfind);
            this.panel_Find.Controls.Add(this.button_refind);
            this.panel_Find.Controls.Add(this.lblColCheck);
            this.panel_Find.Controls.Add(this.lblRowCheck);
            this.panel_Find.Name = "panel_Find";
            // 
            // button_clearfind
            // 
            this.button_clearfind.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.button_clearfind, "button_clearfind");
            this.button_clearfind.FlatAppearance.BorderSize = 0;
            this.button_clearfind.ForeColor = System.Drawing.SystemColors.Control;
            this.button_clearfind.Name = "button_clearfind";
            this.button_clearfind.UseVisualStyleBackColor = false;
            this.button_clearfind.Click += new System.EventHandler(this.button_clearfind_Click);
            // 
            // button_refind
            // 
            this.button_refind.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.button_refind, "button_refind");
            this.button_refind.FlatAppearance.BorderSize = 0;
            this.button_refind.ForeColor = System.Drawing.Color.Black;
            this.button_refind.Name = "button_refind";
            this.button_refind.UseVisualStyleBackColor = false;
            this.button_refind.Click += new System.EventHandler(this.button_refind_Click);
            // 
            // lblColCheck
            // 
            this.lblColCheck.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.lblColCheck, "lblColCheck");
            this.lblColCheck.ForeColor = System.Drawing.Color.Black;
            this.lblColCheck.Name = "lblColCheck";
            this.lblColCheck.Click += new System.EventHandler(this.label_click);
            // 
            // lblRowCheck
            // 
            this.lblRowCheck.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.lblRowCheck, "lblRowCheck");
            this.lblRowCheck.ForeColor = System.Drawing.Color.Black;
            this.lblRowCheck.Name = "lblRowCheck";
            this.lblRowCheck.Click += new System.EventHandler(this.label_click);
            // 
            // plabel_Filename
            // 
            resources.ApplyResources(this.plabel_Filename, "plabel_Filename");
            this.plabel_Filename.BackColor = System.Drawing.Color.LightGray;
            this.plabel_Filename.ContextMenuStrip = this.contextMenuStrip4;
            this.plabel_Filename.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.plabel_Filename.Name = "plabel_Filename";
            // 
            // lblNoRows
            // 
            resources.ApplyResources(this.lblNoRows, "lblNoRows");
            this.lblNoRows.Name = "lblNoRows";
            // 
            // cm6_Save
            // 
            this.cm6_Save.BackColor = System.Drawing.Color.MidnightBlue;
            this.cm6_Save.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cm6_Save.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cm6Save,
            this.cm6SaveAs,
            this.toolStripSeparator5,
            this.cm6EditFileHeader});
            this.cm6_Save.Name = "cm6_save";
            this.cm6_Save.ShowImageMargin = false;
            resources.ApplyResources(this.cm6_Save, "cm6_Save");
            // 
            // cm6Save
            // 
            resources.ApplyResources(this.cm6Save, "cm6Save");
            this.cm6Save.ForeColor = System.Drawing.SystemColors.Control;
            this.cm6Save.Name = "cm6Save";
            this.cm6Save.Click += new System.EventHandler(this.cm6_Save_Click);
            // 
            // cm6SaveAs
            // 
            resources.ApplyResources(this.cm6SaveAs, "cm6SaveAs");
            this.cm6SaveAs.ForeColor = System.Drawing.SystemColors.Control;
            this.cm6SaveAs.Name = "cm6SaveAs";
            this.cm6SaveAs.Click += new System.EventHandler(this.cm6_SaveAs_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            resources.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
            // 
            // cm6EditFileHeader
            // 
            resources.ApplyResources(this.cm6EditFileHeader, "cm6EditFileHeader");
            this.cm6EditFileHeader.ForeColor = System.Drawing.SystemColors.Control;
            this.cm6EditFileHeader.Name = "cm6EditFileHeader";
            this.cm6EditFileHeader.Click += new System.EventHandler(this.cm4_EditFIleHeader_Click);
            // 
            // cm7_Open
            // 
            this.cm7_Open.BackColor = System.Drawing.Color.MidnightBlue;
            this.cm7_Open.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cm7_Open.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cm7Open,
            this.cm7Append,
            this.cm7NewWindow,
            this.toolStripSeparator6,
            this.cm7LoadTemplate});
            this.cm7_Open.Name = "cm7_Open";
            this.cm7_Open.ShowImageMargin = false;
            resources.ApplyResources(this.cm7_Open, "cm7_Open");
            // 
            // cm7Open
            // 
            resources.ApplyResources(this.cm7Open, "cm7Open");
            this.cm7Open.ForeColor = System.Drawing.SystemColors.Control;
            this.cm7Open.Name = "cm7Open";
            this.cm7Open.Click += new System.EventHandler(this.cm7_Open_Click);
            // 
            // cm7Append
            // 
            resources.ApplyResources(this.cm7Append, "cm7Append");
            this.cm7Append.ForeColor = System.Drawing.SystemColors.Control;
            this.cm7Append.Name = "cm7Append";
            this.cm7Append.Click += new System.EventHandler(this.cm7_Append_Click);
            // 
            // cm7NewWindow
            // 
            resources.ApplyResources(this.cm7NewWindow, "cm7NewWindow");
            this.cm7NewWindow.ForeColor = System.Drawing.SystemColors.Control;
            this.cm7NewWindow.Name = "cm7NewWindow";
            this.cm7NewWindow.Click += new System.EventHandler(this.cm7_NewWindow_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            resources.ApplyResources(this.toolStripSeparator6, "toolStripSeparator6");
            // 
            // cm7LoadTemplate
            // 
            resources.ApplyResources(this.cm7LoadTemplate, "cm7LoadTemplate");
            this.cm7LoadTemplate.ForeColor = System.Drawing.SystemColors.Control;
            this.cm7LoadTemplate.Name = "cm7LoadTemplate";
            this.cm7LoadTemplate.Click += new System.EventHandler(this.cm7_Filterlist_Click);
            // 
            // tabControl1
            // 
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label_central);
            this.tabPage1.Controls.Add(this.dataGridView1);
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            resources.ApplyResources(this.tabPage2, "tabPage2");
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dataGridView2, "dataGridView2");
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowTemplate.Height = 24;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.tabPage3, "tabPage3");
            this.tabPage3.Name = "tabPage3";
            // 
            // cm8_Template
            // 
            this.cm8_Template.BackColor = System.Drawing.Color.MidnightBlue;
            this.cm8_Template.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cm8_Template.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cm8ComboTemplate,
            this.cm8UseTemplate,
            this.toolStripSeparator8,
            this.cm8_CloseCompare});
            this.cm8_Template.Name = "cm8_Template";
            this.cm8_Template.ShowImageMargin = false;
            resources.ApplyResources(this.cm8_Template, "cm8_Template");
            this.cm8_Template.Opening += new System.ComponentModel.CancelEventHandler(this.cm8_Template_Opening);
            // 
            // cm8ComboTemplate
            // 
            this.cm8ComboTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cm8ComboTemplate.DropDownWidth = 121;
            resources.ApplyResources(this.cm8ComboTemplate, "cm8ComboTemplate");
            this.cm8ComboTemplate.Name = "cm8ComboTemplate";
            this.cm8ComboTemplate.SelectedIndexChanged += new System.EventHandler(this.cm8_ComboTemplate_SelectedIndexChanged);
            // 
            // cm8UseTemplate
            // 
            resources.ApplyResources(this.cm8UseTemplate, "cm8UseTemplate");
            this.cm8UseTemplate.ForeColor = System.Drawing.SystemColors.Window;
            this.cm8UseTemplate.Name = "cm8UseTemplate";
            this.cm8UseTemplate.Click += new System.EventHandler(this.cm8_UseTemplate_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            resources.ApplyResources(this.toolStripSeparator8, "toolStripSeparator8");
            // 
            // cm8_CloseCompare
            // 
            resources.ApplyResources(this.cm8_CloseCompare, "cm8_CloseCompare");
            this.cm8_CloseCompare.ForeColor = System.Drawing.SystemColors.Window;
            this.cm8_CloseCompare.Name = "cm8_CloseCompare";
            // 
            // button_save
            // 
            resources.ApplyResources(this.button_save, "button_save");
            this.button_save.FlatAppearance.BorderSize = 0;
            this.button_save.Image = global::PlaylistEditor.Properties.Resources.content_save_r;
            this.button_save.Name = "button_save";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // button_open
            // 
            resources.ApplyResources(this.button_open, "button_open");
            this.button_open.FlatAppearance.BorderSize = 0;
            this.button_open.Image = global::PlaylistEditor.Properties.Resources.open_in_app_r;
            this.button_open.Name = "button_open";
            this.button_open.UseVisualStyleBackColor = true;
            this.button_open.Click += new System.EventHandler(this.button_open_Click);
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.Controls.Add(this.btnTemplate);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.button_kodi);
            this.Controls.Add(this.button_import);
            this.Controls.Add(this.RedoButton);
            this.Controls.Add(this.UndoButton);
            this.Controls.Add(this.button_check);
            this.Controls.Add(this.button_vlc);
            this.Controls.Add(this.button_dup);
            this.Controls.Add(this.button_search);
            this.Controls.Add(this.button_moveDown);
            this.Controls.Add(this.button_moveUp);
            this.Controls.Add(this.button_del_all);
            this.Controls.Add(this.button_settings);
            this.Controls.Add(this.button_add);
            this.Controls.Add(this.button_Info);
            this.Controls.Add(this.button_delLine);
            this.Controls.Add(this.button_save);
            this.Controls.Add(this.button_open);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.cm5_Duplicate.ResumeLayout(false);
            this.cmLabelVlc.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.contextMenuStrip3.ResumeLayout(false);
            this.contextMenuStrip4.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel_Find.ResumeLayout(false);
            this.panel_Find.PerformLayout();
            this.cm6_Save.ResumeLayout(false);
            this.cm7_Open.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.cm8_Template.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private PlaylistEditor.MyButton button_open;
        private PlaylistEditor.MyButton button_save;
        private PlaylistEditor.MyButton button_delLine;
        private PlaylistEditor.MyButton button_Info;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolTip toolTip1;
        private PlaylistEditor.MyButton button_add;
        private PlaylistEditor.MyButton button_settings;
        private PlaylistEditor.MyButton button_del_all;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pasteRowMenuItem;
        private RepeatingButton button_moveUp;
        private RepeatingButton button_moveDown;
        private PlaylistEditor.MyButton button_search;
        private PlaylistEditor.MyButton button_dup;
        private System.Windows.Forms.ToolStripMenuItem toolStripCopy;
        private System.Windows.Forms.ToolStripMenuItem toolStripPaste;
        private System.Windows.Forms.ToolStripMenuItem cutRowMenuItem;
        private PlaylistEditor.MyButton button_vlc;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem playToolStripMenuItem;
        private PlaylistEditor.MyButton button_check;
        private PlaylistEditor.MyButton UndoButton;
        private PlaylistEditor.MyButton RedoButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cm1_hideColumn;
        private System.Windows.Forms.ToolStripMenuItem cm1_showColumns;
        private PlaylistEditor.MyButton button_import;
        private PlaylistEditor.MyButton button_kodi;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem cm2_addUseragentCell;
        private System.Windows.Forms.ToolStripMenuItem editCellCopy;
        private System.Windows.Forms.ToolStripMenuItem editCellPaste;
        private System.Windows.Forms.ToolStripMenuItem editCellCut;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label_central;
        private System.Windows.Forms.ToolStripMenuItem cms1NewWindow;
        private System.Windows.Forms.ToolStripMenuItem cms1Number;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem cm3EditF2;
        private System.Windows.Forms.ToolStripMenuItem cm3Scrollbar;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip3;
        private System.Windows.Forms.ToolStripMenuItem cm1NewColumn;
        private System.Windows.Forms.ToolStripComboBox cm1ColCombo;
        private System.Windows.Forms.ToolStripMenuItem cm1AddColumn;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip4;
        private System.Windows.Forms.ToolStripMenuItem cm4EditFIleHeader;
        private System.Windows.Forms.ContextMenuStrip cm5_Duplicate;
        private System.Windows.Forms.ToolStripMenuItem cm5StartSearchDupli;
        private System.Windows.Forms.ToolStripComboBox cm5ColumNames;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MyButton button_revert;
        private PathLabel plabel_Filename;
        private System.Windows.Forms.Label lblNoRows;
        private System.Windows.Forms.Button btnSelectList;
        private System.Windows.Forms.Panel panel_Find;
        private System.Windows.Forms.TextBox textBox_find;
        private MyButton button_clearfind;
        private MyButton button_refind;
        private System.Windows.Forms.Label lblColCheck;
        private System.Windows.Forms.Label lblRowCheck;
        private System.Windows.Forms.ContextMenuStrip cm6_Save;
        private System.Windows.Forms.ToolStripMenuItem cm6Save;
        private System.Windows.Forms.ToolStripMenuItem cm6SaveAs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem cm6EditFileHeader;
        private System.Windows.Forms.ContextMenuStrip cm7_Open;
        private System.Windows.Forms.ToolStripMenuItem cm7Open;
        private System.Windows.Forms.ToolStripMenuItem cm7Append;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem cm7LoadTemplate;
        private System.Windows.Forms.ToolStripMenuItem cm7NewWindow;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private MyButton btnTemplate;
        private System.Windows.Forms.ContextMenuStrip cm8_Template;
        private System.Windows.Forms.ToolStripComboBox cm8ComboTemplate;
        private System.Windows.Forms.ToolStripMenuItem cm8UseTemplate;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ContextMenuStrip cmLabelVlc;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem cmMultiview;
        private System.Windows.Forms.ToolStripMenuItem cmFullscreen;
        private System.Windows.Forms.ToolStripMenuItem fontSIzeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cms1Bigger;
        private System.Windows.Forms.ToolStripMenuItem cms1Smaller;
        private System.Windows.Forms.ToolStripMenuItem cm3_ResizeColumns;
        private System.Windows.Forms.ToolStripMenuItem cm8_CloseCompare;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.Label lblEncoding;
        private System.Windows.Forms.ToolStripMenuItem cm3_SortRows;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem cm1_UserAgent;
        private System.Windows.Forms.ToolStripMenuItem cm1_TVHeadend;
        private System.Windows.Forms.ToolStripMenuItem cm3_HideColumn;
        private System.Windows.Forms.ToolStripMenuItem cm3_ShowAllColumns;
        private System.Windows.Forms.ToolStripMenuItem newColumnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cm3_AddColumn;
        private System.Windows.Forms.ToolStripComboBox cm3_ColCombo;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cm1_removeUserAgent;
        private System.Windows.Forms.ToolStripMenuItem cm1_addTVHeadend;
        private System.Windows.Forms.ToolStripMenuItem cm1_removeTVHeadend;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem editStringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editStringToolStripMenuItem1;
    }
}

