﻿using PlaylistEditor.Properties;
using System;
using System.Windows.Forms;

namespace PlaylistEditor
{
    public partial class EditTVHead : Form
    {
        public string TVheader1 { get; set; }
        public string TVheader2 { get; set; }

        public EditTVHead(string tvh1, string tvh2)
        {
            InitializeComponent();

            textBox1.Text = tvh1;
            textBox2.Text = tvh2;
            textBox3.Text = Settings.Default.user_agent;
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            TVheader1 = textBox1.Text + " ";
            TVheader2 = " " + textBox2.Text;
            Settings.Default.user_agent = textBox3.Text;

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            this.Close();

        }
    }
}
