﻿using System.Collections.Generic;
using System.Drawing;
using System.Net.Http;
using System.Text;

namespace PlaylistEditor
{
    public class ClassDataset
    {
        public static HttpClient _Client = new HttpClient();
        public static readonly string USERAGENT = "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36";

        public static Point MENUPOINT = new Point(9, 52);

        public enum NeedSave { Yes, No, Reset }

        private static string[] _coltypes;
        public static string[] ColTypes
        {
            get
            {
                return _coltypes = new[] {
                    "tvg-name", "tvg-id", "tvg-title", "tvg-logo", "tvg-chno", "tvg-shift",
                    "group-title", "radio", "catchup", "catchup-source", "catchup-days", "catchup-correction",
                    "provider", "provider-type", "provider-logo", "provider-countries", "provider-languages",
                    "media", "media-dir", "media-size", "dvb-reference"};

            }
        }

        private string[] _kodiproptypes;
        public string[] KodiPropTypes
        {
            get
            {
                return _kodiproptypes = new[] {
                    "inputstream=", "manifest_type=", "license_type=", "license_key=", "manifest_config=",
                    "inputstreamclass=", "inputstreamaddon=", "rtsp_transport=", "key="
                };

            }
        }

        private Dictionary<string, string> _WriteExtraLines;

        public Dictionary<string, string> WriteExtraLines
        {
            get
            {
                return _WriteExtraLines = new Dictionary<string, string>()
                {
                    { "inputstream=", "#KODIPROP:inputstream=" },
                    { "manifest_type=", "#KODIPROP:inputstream.adaptive.manifest_type=" },
                    { "manifest_config=", "#KODIPROP:inputstream.adaptive.manifest_config=" },
                    { "license_type=", "#KODIPROP:inputstream.adaptive.license_type=" },
                    { "license_key=", "#KODIPROP:inputstream.adaptive.license_key=" },
                    { "inputstreamclass=", "#KODIPROP:inputstreamclass=" },
                    { "inputstreamaddon=", "#KODIPROP:inputstreamaddon=" },
                    { "rtsp_transport=", "#KODIPROP:rtsp_transport=" },
                    { "key=", "#KODIPROP:key=" },
                    { ":http-referrer", "#EXTVLCOPT:http-referrer=" },
                    { ":http-user-agent", "#EXTVLCOPT:http-user-agent=" }

                };
            }
        }



        private string[] _kodireferrertypes;
        public string[] KodiReferrerTypes
        {
            get
            {
                return _kodireferrertypes = new[]
                { ":http-referrer", ":http-user-agent"};

            }
        }

        public Color ColorError1 = Color.LightSalmon;
        public Color ColorDup1 = Color.LightCoral;
        public Color ColorDup2 = Color.LightGreen;
        public class PaintDupCells
        {
            public bool Select1 { get; set; }
            public bool Select2 { get; set; }
            public Color Color1 { get; set; }
            public Color Color2 { get; set; }

        }

        public class CheckList
        {
            public string Url { get; set; }
            public int ErrorCode { get; set; }
        }
        public class ColList
        {
            public string ColName { get; set; }
            public bool Visible { get; set; }
            public int LineType { get; set; }  //0: normal 1:REFERRER 2:KODIPROP
            public int ColNumber { get; set; }  
        }

        public class DupList
        {
            public string CellName { get; set; }
            public bool Master { get; set; }
            public string ColName { get; set; }
        }


        public string _vlcpath;
        public static string vlcpath { get; set; }

        public string _smppath;
        public static string smppath { get; set; }

        public string _workpath;
        public static string Workpath { get; set; }

        public string _slingerpath;
        public static string Slingerpath { get; set; }



    }
}
