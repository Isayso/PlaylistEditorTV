﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using static PlaylistEditor.ClassDataset;

namespace PlaylistEditor
{
    internal class ClassPlayer
    {
        private static DataGridView dgv = new DataGridView();

        public VLCRemoteController m_vlcControl;
        private static bool m_running = false;

      //  private static ClassVlcRemote m_vlcRemote;

        /// <summary>
        /// runs video player with link from current row
        /// </summary>
        /// <param name="dgv_imp"></param>
        public void RunPlayer(DataGridView dgv_imp)
        {
            dgv = dgv_imp;

           // m_vlcControl = new VLCRemoteController();  //only for remote control
           // m_vlcRemote = new ClassVlcRemote();

            //KillPlayer();
            //PlayOnSlinger(); return;
            //check whitch player and run it
            if (IsVlcPlayer)
              //  RunVlcRemote();
                PlayOnVlc();

            else if (IsSmPlayer)
                PlayOnSMPlay();

        }

        private void RunVlcRemote()
        {
            string param; bool ok;
            param = dgv.CurrentRow.Cells["Link"].Value.ToString();

            if (!m_running)
            {
                VLCRemoteStart();
                m_running = true;
            }
            
            m_vlcControl.connect("127.0.0.1", 4444);
            m_vlcControl.sendCustomCommand("add " + param);
            m_vlcControl.reciveAnswer();

        }
        private static void PlayOnSlinger()
        {

                string param = dgv.CurrentRow.Cells["Link"].Value.ToString();
                Slingerpath = Slingerpath + "\\";

                ProcessStartInfo ps = new ProcessStartInfo();
                ps.FileName = Path.Combine(Slingerpath, "SlingerPlayer.exe ");  //  ps.FileName = vlcpath + "\\" + "vlc.exe";

                ps.ErrorDialog = false;

                ps.Arguments = param;

                ps.CreateNoWindow = true;
                ps.UseShellExecute = false;

                ps.RedirectStandardOutput = true;
                ps.WindowStyle = ProcessWindowStyle.Hidden;

                using (Process proc = new Process())
                {
                    proc.StartInfo = ps;

                    proc.Start();
                }
        }
        private void PlayOnSMPlay()
        {
                string param = ClassHelp.CleanTVHeadendLink(dgv.CurrentRow.Cells["Link"].Value.ToString());
                smppath = smppath + "\\";

                ProcessStartInfo ps = new ProcessStartInfo();
                ps.FileName = Path.Combine(smppath, "smplayer.exe");  //  ps.FileName = vlcpath + "\\" + "vlc.exe";

                ps.ErrorDialog = false;

                if (/*_isSingle && */Properties.Settings.Default.vlcFullscreen)
                    ps.Arguments = " -minigui -fullscreen " + "\"" + param + "\"";

                //else if (_isSingle && !Settings.Default.vlc_fullsreen)
                //    ps.Arguments = " -minigui " + "\"" + param + "\"";//+ param;

                else ps.Arguments = " -minigui " + param;

                ps.CreateNoWindow = true;
                ps.UseShellExecute = false;

                ps.RedirectStandardOutput = true;
                ps.WindowStyle = ProcessWindowStyle.Hidden;

                using (Process proc = new Process())
                {
                    proc.StartInfo = ps;

                    proc.Start();
                }
        }
        private static void PlayOnVlc()
        {
           // if (IsDataGrid && !string.IsNullOrEmpty(vlcpath))
            {
                // Set cursor as hourglass
            //    Cursor.Current = Cursors.WaitCursor;

                string param =  ClassHelp.CleanTVHeadendLink(dgv.CurrentRow.Cells["Link"].Value.ToString());
                vlcpath = vlcpath + "\\";

                ProcessStartInfo ps = new ProcessStartInfo();
                ps.FileName = Path.Combine(vlcpath, "vlc.exe");  //  ps.FileName = vlcpath + "\\" + "vlc.exe";

                ps.ErrorDialog = false;
                
                if (!Properties.Settings.Default.vlcMultiplayer)
                    ps.Arguments = " --one-instance ";

                ps.Arguments += " --no-video-title-show ";

                if (Properties.Settings.Default.vlcFullscreen)
                    ps.Arguments += " --fullscreen";

                //if (/*_isSingle &&*/ Properties.Settings.Default.vlc_fullsreen)
                //    ps.Arguments += " --fullscreen " + "\"" + param + "\"";

                //else if (/*_isSingle && */!Properties.Settings.Default.vlc_fullsreen)
                //    ps.Arguments +=  "\"" + param + "\"";//+ param;

                //else ps.Arguments = " --no-video-title-show " + param;

                ps.Arguments += "\"" + param + "\"";//+ param;

                ps.Arguments += " --no-qt-error-dialogs";

                if (IsAgentCol)
                    if (dgv.Columns[":http-user-agent"].Visible &&
                        !string.IsNullOrEmpty(dgv.CurrentRow.Cells[":http-user-agent"].Value.ToString()))
                        ps.Arguments += " :http-user-agent=" + "\"" + dgv.CurrentRow.Cells[":http-user-agent"].Value.ToString() + "\"";

                if (IsReferrerCol)
                    if (dgv.Columns[":http-referrer"].Visible &&
                        !string.IsNullOrEmpty(dgv.CurrentRow.Cells[":http-referrer"].Value.ToString()))
                        ps.Arguments += " :http-referrer=" + "\"" + dgv.CurrentRow.Cells[":http-referrer"].Value.ToString() + "\"";


#if DEBUG
           //     MessageBox.Show("param: " + ps.Arguments.ToString());
#endif

                ps.CreateNoWindow = true;
                ps.UseShellExecute = false;

                ps.RedirectStandardOutput = true;
                ps.WindowStyle = ProcessWindowStyle.Hidden;

                using (Process proc = new Process())
                {
                    proc.StartInfo = ps;

                    proc.Start();
                }
                // Set cursor as default arrow
              //  Cursor.Current = Cursors.Default;
              //  _isSingle = false;
            }
        }

        public static void KillPlayer()
        {
            if (IsVlcPlayer)
            {
                try
                {
                    Process[] processes = null;
                    processes = Process.GetProcessesByName("vlc");
                    foreach (Process process in processes)
                    {
                        process.Kill();
                    }
                }
                catch (ArgumentException)
                {
                }

            }

            if (IsSmPlayer)
            {
                //close SMP
                try
                {
                    Process[] processes = null;
                    processes = Process.GetProcessesByName("smplayer");
                    foreach (Process process in processes)
                    {
                        process.Kill();
                    }
                }
                catch (ArgumentException)
                {
                }

            }

            //try
            //{
            //    Process[] processes = null;
            //    processes = Process.GetProcessesByName("slingerplayer");
            //    foreach (Process process in processes)
            //    {
            //        process.Kill();
            //    }
            //}
            //catch (ArgumentException)
            //{
            //}

        }

        private static bool IsReferrerCol
        {
            get
            {
                foreach (DataGridViewColumn c in dgv.Columns)
                {
                    if (c.HeaderText.Equals(":http - referrer")) return true;
                }
                return false;
            }
        }
        public static bool IsAgentCol
        {
            get
            {
                foreach (DataGridViewColumn c in dgv.Columns)
                {
                    if (c.HeaderText.Equals(":http-user-agent")) return true;
                }
                return false;
            }
        }

        private static bool IsVlcPlayer
        {
            get { return Properties.Settings.Default.vlcplayer; }
        }
        private static bool IsSmPlayer
        {
            get { return Properties.Settings.Default.smplayer; }
        }


        public static void VLCRemoteStart()
        {

            string exePath = ClassCheckPlayer.GetVlcPath() + "\\" + "vlc.exe";
            if (!String.IsNullOrEmpty(exePath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = exePath;
                startInfo.Arguments = @" --control=rc --rc-host 127.0.0.1:4444 --rc-quiet";
               // startInfo.Arguments += "--no-video-title-show ";
                startInfo.Arguments += " --no-qt-error-dialogs";
                if (Properties.Settings.Default.vlcFullscreen)
                    startInfo.Arguments += " --fullscreen";

                    Process.Start(startInfo);
            }
            else
            {
                MessageBox.Show("VLC is not found on PC. Please ran it from command line:\r\nvlc.exe --control=rc --rc-host 127.0.0.1:4444", "Cannot find VLC",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
            return;

            vlcpath = vlcpath + "\\";

            ProcessStartInfo ps = new ProcessStartInfo();
            ps.FileName = Path.Combine(vlcpath, "vlc.exe");  //  ps.FileName = vlcpath + "\\" + "vlc.exe";
            ps.Arguments = @"--control=rc --rc-host 127.0.0.1:4444 --rc-quiet";
           
            using (Process proc = new Process())
            {
                proc.StartInfo = ps;

                proc.Start();
            }
        }



    }
}
