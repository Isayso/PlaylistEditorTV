﻿using System;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace PlaylistEditor
{
    internal class ClassUpdate
    {
        public static bool CheckUpdate(string url)
        {
            string active_version = Assembly.GetExecutingAssembly().GetName().Version.ToString().Substring(0, 5);

            string latest_version = GetLatestCodeberg(url);

            var version1 = new Version(active_version);
            var version2 = new Version(latest_version);

            var result = version1.CompareTo(version2);

            if (result < 0)
            {
                return true;
            }
            else
            {
                NotificationBox.Show("No Update avaliable", 3000, NotificationMsg.ERROR);
            }

            return false;

        }

        private static string GetLatestCodeberg(string url)
        {
            try
            {
                string source = "";

                Task.Run(async () =>
                {
                    source = await InetTask(url);
                }).Wait();

                return ClassHelp.ScrapHtml(source, "/tag/(.*?)\"").Replace("v", "");

            }
            catch
            {
            }

            return "0.0.0";
        }

        public static async Task<string> InetTask(string url)
        {
            // Call asynchronous network methods in a try/catch block to handle exceptions.
            string responseBody = "";

            try
            {
                // client.DefaultRequestHeaders.Accept.Clear();
                ClassDataset._Client.DefaultRequestHeaders.UserAgent.TryParseAdd(ClassDataset.USERAGENT);

                HttpResponseMessage response = await ClassDataset._Client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                responseBody = await response.Content.ReadAsStringAsync();
                //  IEnumerable<string> cookies = response.Headers.SingleOrDefault(header => header.Key == "Set-Cookie").Value;

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);

            }

            return responseBody;

        }

    }
}
