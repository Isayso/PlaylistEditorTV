﻿using Microsoft.Win32;
using System.IO;
using static PlaylistEditor.ClassDataset;
using static PlaylistEditor.ClassHelp;

namespace PlaylistEditor
{
    internal class ClassCheckPlayer
    {

        public static void SetPlayerPath()
        {
            vlcpath = Properties.Settings.Default.vlcpath;
            smppath = Properties.Settings.Default.smppath;

            if (!MyFileExists(smppath + "\\" + "smplayer.exe", 5000))
            {
                smppath = GetSMPPath();
            }

            if (!MyFileExists(vlcpath + "\\" + "vlc.exe", 5000))  // vlcpath + "\\" + "vlc.exe";
            {
                vlcpath = GetVlcPath();
            }

            //Slingerpath = System.AppDomain.CurrentDomain.BaseDirectory + "SlingerPlayer";
            //Workpath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PlaylistEditorTV";  //write to %AppData%
            //System.IO.Directory.CreateDirectory(Workpath);


        }

        public static bool CheckSMP()
        {
            smppath = Properties.Settings.Default.smppath;

            bool b_found = !string.IsNullOrEmpty(smppath) ? true : false;

            if (b_found)
            {
                //check for smplayer.exe
                if (!MyFileExists(smppath + "\\" + "smplayer.exe", 5000))  // vlcpath + "\\" + "vlc.exe";
                {
                    smppath = GetSMPPath();
                    if (string.IsNullOrEmpty(smppath))
                    {
                        Properties.Settings.Default.smppath = "";
                        Properties.Settings.Default.Save();
                        return false;
                    }
                }
            }
            else if (!b_found)  //first run
            {
                smppath = GetSMPPath();
                if (string.IsNullOrEmpty(smppath))
                {
                    b_found = false;
                }
                else { b_found = true; }
            }

            return b_found;
        }
        public static bool CheckVLC()
        {
            vlcpath = Properties.Settings.Default.vlcpath;

            bool b_found = !string.IsNullOrEmpty(vlcpath) ? true : false;

            if (b_found)
            {
                //check for vlc.exe
                if (!MyFileExists(vlcpath + "\\" + "vlc.exe", 5000))  // vlcpath + "\\" + "vlc.exe";
                {
                    vlcpath = GetVlcPath();
                    if (string.IsNullOrEmpty(vlcpath))
                    {
                        Properties.Settings.Default.vlcpath = "";
                        Properties.Settings.Default.Save();
                        return false;
                    }
                }
            }
            else if (!b_found)  //first run
            {
                vlcpath = GetVlcPath();
                if (string.IsNullOrEmpty(vlcpath))
                {
                    b_found = false;
                }
                else { b_found = true; }
            }

            return b_found;
        }


        public static string GetSMPPath()
        {
            object line;
            string[] registry_key = { @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall",
                            @"SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall" };
            try  //issue #58
            {
                using (var baseKey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                {
                    for (int i = 0; i < 2; i++)
                    {
                        using (var key = baseKey.OpenSubKey(registry_key[i]))
                        {
                            foreach (string subkey_name in key.GetSubKeyNames())
                            {
                                using (var subKey = key.OpenSubKey(subkey_name))
                                {
                                    line = subKey.GetValue("DisplayName");
                                    if (line != null && (line.ToString().StartsWith("SMPlayer")))
                                    {
                                        string SMPPath = subKey.GetValue("DisplayIcon").ToString();
                                        FileInfo fileInfo = new FileInfo(SMPPath);
                                        string directoryFullPath = fileInfo.DirectoryName;
                                        Properties.Settings.Default.smppath = directoryFullPath;
                                        Properties.Settings.Default.Save();
                                        return directoryFullPath;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                return "";  //no vlc found
            }
            return "";  //no vlc found
        }

        public static string GetVlcPath()
        {
            object line;
            string[] registry_key = { @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall",
                            @"SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall" };
            try  //issue #58
            {
                using (var baseKey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                {
                    for (int i = 0; i < 2; i++)
                    {
                        using (var key = baseKey.OpenSubKey(registry_key[i]))
                        {
                            foreach (string subkey_name in key.GetSubKeyNames())
                            {
                                using (var subKey = key.OpenSubKey(subkey_name))
                                {
                                    line = subKey.GetValue("DisplayName");
                                    if (line != null && (line.ToString().ToUpper().Contains("VLC")))
                                    {
                                        string VlcPath = subKey.GetValue("InstallLocation").ToString();

                                        Properties.Settings.Default.vlcpath = VlcPath;
                                        Properties.Settings.Default.Save();
                                        return VlcPath;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                return "";  //no vlc found
            }
            return "";  //no vlc found
        }

    }
}
