﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using static PlaylistEditor.ClassDataset;

namespace PlaylistEditor
{
    internal class ClassImportData
    {
        /// <summary>
        /// Import data from file lines to datatable
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="fileRows"></param>
        /// <returns>DataTable</returns>
        public static DataTable ImportData(DataTable dt, string[] fileRows)
        {
            DataRow dr = null;
            var myClass = new ClassDataset();


            int b = fileRows[0].StartsWith("#EXTM3U") ? 1 : 0;

            for (int i = b; i < fileRows.Length; i++)
            {
                if (fileRows[i].StartsWith("#EXTINF"))
                {
                    dr = dt.NewRow();

                    for (int j = 0; j < dt.Columns.Count - 2; j++)
                    {
                        string header = dt.Columns[j].ToString();
                        var match = Regex.Match(fileRows[i], header + "=\"([^\"]*)\"").Groups[1];

                        if (match.Success)
                        {
                            string udpIP = match.Captures[0].Value;
                            dr[header] = udpIP;
                            continue;
                        }

                    }
                    string name2 = fileRows[i].Split(',').Last().Trim();

                    if (!name2.EndsWith("\""))
                        dr["Name2"] = name2;
                    else
                        dr["Name2"] = "";

                    continue;
                }
                else if (fileRows[i].StartsWith("#EXTVLCOPT"))   //issue #65
                {
                    if (fileRows[i].Contains("user-agent"))
                    {
                        //  dr[":http-user-agent"] = fileRows[i].Replace("#EXTVLCOPT", "").Replace(":http-user-agent=", "");
                        dr[":http-user-agent"] = fileRows[i].Remove(0, 27);
                        continue;
                    }
                    if (fileRows[i].Contains("-referrer"))
                    {
                        //  dr[":http-referrer"] = fileRows[i].Replace("#EXTVLCOPT:http-referrer=", "");
                        dr[":http-referrer"] = fileRows[i].Remove(0, 25);
                        continue;
                    }
                }
                //Issue #14  
                else if (fileRows[i].StartsWith("#KODIPROP"))
                {
                    for (int j = 0; j < myClass.KodiPropTypes.Length; j++)
                    {
                        if (fileRows[i].Contains(myClass.KodiPropTypes[j]))
                        {
                            dr[myClass.KodiPropTypes[j]] = fileRows[i].Split('=').Last().Trim();
                            continue;
                        }

                    }
                }
                else //http line
                {
                    try
                    {
                        dr["Link"] = fileRows[i];
                        dt.Rows.Add(dr);
                    }
                    catch { continue; }
                }
            }


            return dt;
        }


        /// <summary>
        /// creates datatable from list of column names
        /// </summary>
        /// <param name="elements"></param>
        /// <returns>DataTable</returns>
        public static DataTable CreateTableColumns(List<ColList> elements)
        {
            DataTable table = new DataTable();

            foreach (ColList name in elements)
            {
                table.Columns.Add(name.ColName.ToString());
            }

            table.Columns.Add("Name2");
            table.Columns.Add("Link");

            return table;
        }

        /// <summary>
        /// get list of column names from datatable
        /// </summary>
        /// <param name="dt"></param>
        /// <returns>List of strings</returns>
        public static List<string> GetColumnNames(DataTable dt)
        {
            List<string> names = new List<string>();

            foreach (DataColumn c in dt.Columns)
            {
                names.Add(c.ColumnName);
            }
            return names;
        }



}
}
