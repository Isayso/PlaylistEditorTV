﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Management;
using System.Windows.Forms;

namespace PlaylistEditor
{
    internal class ClassCopyPaste
    {
        /// <summary>
        /// copy all selected full rows to string 
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static StringBuilder GetRows(DataGridView dgv, DataTable dt) 
        {

                // get col header
                StringBuilder rowString = new StringBuilder();

                rowString.Append("FULLROW");  //magic string

                foreach (DataColumn c in dt.Columns)
                {
                    rowString.Append("\t");
                    rowString.Append(c.ColumnName).Append("=\"");  // =\" because element detection

                }
                rowString.Append("\r\n");  //end of line 0

                foreach (DataGridViewRow row in dgv.InvSelectedRows())
                {
                    for (int i = 0; i < dgv.ColumnCount - 1; i++)
                    {
                        rowString.Append(dgv[i, row.Index].Value.ToString().Trim()).Append("\t");
                    }
                    rowString.Append(dgv["Link", row.Index].Value.ToString().Trim());  //to avoid \t
                    rowString.Append("\r\n");
                }

            return rowString;
        }

    }
}
