﻿//  MIT License
//  Copyright (c) 2018 github/isayso
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
//  files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy,
//  modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static PlaylistEditor.ClassDataset;

namespace PlaylistEditor
{
    internal static class ClassHelp
    {
        public static List<CheckList> checkList = new List<CheckList>();
        public static List<ColList> columnList = new List<ColList>();

        public static string fileHeader = null;


        /// <summary>
        /// returns string between 2 stings
        /// </summary>
        /// <param name="fullstr"></param>
        /// <param name="startstr"></param>
        /// <param name="endstr"></param>
        /// <returns>substring between 2 strings</returns>
        public static string GetPartString(string fullstr, string startstr, string endstr)
        {
            int start, end;
            if (fullstr.Contains(startstr) && fullstr.Contains(endstr))
            {
                start = fullstr.IndexOf(startstr, 0) + startstr.Length;
                end = fullstr.IndexOf(endstr, start);
                return fullstr.Substring(start, end - start);
            }
            else
            {
                return "";
            }
        }



        /// <summary>
        /// byte to string / string to byte
        /// </summary>
        /// <param name="arr"></param>
        /// <returns>string</returns>
        public static string ByteArrayToString(byte[] arr)
        {
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            return enc.GetString(arr);
        }

        //public static byte[] StringToByteArray(string str)
        //{
        //    System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
        //    return enc.GetBytes(str);
        //}

        /// <summary>
        /// checks if Diectory exists with timeout
        /// </summary>
        /// <param name="openpath">path</param>
        /// <param name="timeout">timeout</param>
        /// <returns>true/false</returns>
        public static bool MyDirectoryExists(string openpath, int timeout)
        {
            //return true;
            //return Directory.Exists(openpath);
            var task = new Task<bool>(() => 
            { 
                var info = new DirectoryInfo(openpath); return info.Exists;
            });
            task.Start();

            return task.Wait(timeout) && task.Result;


        }

        /// <summary>
        /// checks if File Exists with timeout
        /// </summary>
        /// <param name="uri">full filename</param>
        /// <param name="timeout">timeout</param>
        /// <returns></returns>
        public static bool MyFileExists(string uri, int timeout)
        {
            var task = new Task<bool>(() =>
            {
                var fi = new FileInfo(uri);
                return fi.Exists;
            });
            task.Start();

            return task.Wait(timeout) && task.Result;
        }

        /// <summary>
        /// function to get the path of installed vlc
        /// </summary>
        /// <returns>path or empty</returns>

        /// <summary>
        /// check if internet connection is alive
        /// </summary>
        /// <param name="uri">URL to check</param>
        /// <returns>errorcode</returns>
        public static int CheckINetConn(string uri)
        {
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri) as HttpWebRequest;
                req.Timeout = 6000; //set the timeout

                req.ContentType = "application/x-www-form-urlencoded";
                //   req.KeepAlive = true;
                //https://deviceatlas.com/blog/list-smart-tv-user-agent-strings
                //issue #15
                req.UserAgent = USERAGENT;

                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                StreamReader sr = new StreamReader(resp.GetResponseStream());

                char[] buffer = new char[1024];
                int results1 = sr.Read(buffer, 0, 1023);
                if (System.Diagnostics.Debugger.IsAttached)
                    Console.WriteLine("buffer : {0}", results1);

                sr.Close();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    if (System.Diagnostics.Debugger.IsAttached)
                    {
                        Console.WriteLine("Status Code : {0}", (int)((HttpWebResponse)e.Response).StatusCode);
                        Console.WriteLine("Status Description : {0}", ((HttpWebResponse)e.Response).StatusDescription);
                    }

                    return (int)((HttpWebResponse)e.Response).StatusCode;
                }
                return 401;  //Timeout error
            }
            catch (Exception ex)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    Console.WriteLine("ex Code : {0}", ex.Message);
                return 401;
            }

            return 0;
        }

        /// <summary>
        /// check if link is alive and store result in class checkList
        /// </summary>
        /// <param name="uri">link to check</param>
        /// <returns>class checkList</returns>
        public static int CheckIPTVStream(string uri)
        {
            int errorcode = 0;

            if (uri.StartsWith("rt") /*|| uri.StartsWith("ud")*/) errorcode = 410;  //rtmp check not implemented  issue #61
            else   //issue #41
            {
                try
                {
                    ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri) as HttpWebRequest;
                    
                    req.Timeout = Properties.Settings.Default.timeout; //set the timeout #47

                    req.ContentType = "application/x-www-form-urlencoded";
                    //   req.KeepAlive = true;
                    //https://deviceatlas.com/blog/list-smart-tv-user-agent-strings
                    req.UserAgent = USERAGENT;

                    //issue #15

                    if (uri.Contains("|User-Agent") && uri.Contains(".m3u8"))  //#18
                    {
                        req.UserAgent = uri.Split('=').Last();
                    }

                    char[] buffer = new char[1024];
                    int results1;
                    HttpWebResponse resp;

                    using (resp = (HttpWebResponse)req.GetResponse())
                    {
                        // Code here 
                        using (var sr2 = new StreamReader(resp.GetResponseStream()))
                        {
                            results1 = sr2.Read(buffer, 0, 1023);

                            if (System.Diagnostics.Debugger.IsAttached)
                                Console.WriteLine("buffer : {0}", results1);
                        }
                    }

                    //test
                    //HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                    //StreamReader sr = new StreamReader(resp.GetResponseStream());

                    //char[] buffer = new char[1024];
                    //int results1 = sr.Read(buffer, 0, 1023);
                    //if (System.Diagnostics.Debugger.IsAttached)
                    //    Console.WriteLine("buffer : {0}", results1);

                    //sr.Close();
                }
                catch (WebException e)  //#34
                {
                    if (e.Status == WebExceptionStatus.ProtocolError)
                    {
                        if (System.Diagnostics.Debugger.IsAttached)
                        {
                            Console.WriteLine("Status Code : {0}", (int)((HttpWebResponse)e.Response).StatusCode);
                            Console.WriteLine("Status Description : {0}", ((HttpWebResponse)e.Response).StatusDescription);
                        }

                        errorcode = (int)((HttpWebResponse)e.Response).StatusCode;
                    }
                    else errorcode = 401;  //Timeout error
                }
                catch (Exception ex)
                {
                    if (System.Diagnostics.Debugger.IsAttached)
                        Console.WriteLine("ex Code : {0}", ex.Message);
                    errorcode = 401;
                }
            }

            checkList.Add(new CheckList
            {
                Url = uri,
                ErrorCode = errorcode
            });

            return 0;
        }

        /// <summary>
        /// load on undo stack
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="dgv"></param>
        /// <returns></returns>
        public static bool LoadItem(this Stack<object[][]> instance, DataGridView dgv)
        {
            if (instance.Count == 0)
            {
                return true;
            }
            object[][] rows = instance.Peek();
            return !ItemEquals(rows, dgv.Rows.Cast<DataGridViewRow>().Where(r => !r.IsNewRow).ToArray());
        }

        public static bool ItemEquals(this object[][] instance, DataGridViewRow[] dgvRows)
        {
            if (instance.Count() != dgvRows.Count())
            {
                return false;
            }
            return !Enumerable.Range(0, instance.GetLength(0)).Any(x => !instance[x].SequenceEqual(dgvRows[x]
                .Cells.Cast<DataGridViewCell>().Select(c => c.Value).ToArray()));
        }

        /// <summary>
        /// checks if a full row is in clipboard
        /// </summary>
        /// <returns></returns>
        public static bool CheckClipboard()
        {
            try
            {
                DataObject o = (DataObject)Clipboard.GetDataObject();

                if (Clipboard.ContainsText())
                {
                    string content = o.GetData(DataFormats.UnicodeText).ToString();
                    if (content.StartsWith("FULLROW"))
                    {
                        return true;
                    }
                }
                return false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Paste operation failed. (check clip) " + ex.Message, "Copy/Paste", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }


            return false;
        }





       


        public static List<ColList> SeekFileElements(string fullstr)
        {
            columnList.Clear();

            var myClass = new ClassDataset();

            //string[] regArray = { "tvg-name", "tvg-id", "tvg-title", "tvg-logo", "tvg-chno", "tvg-shift",
            //    "group-title", "radio", "catchup", "catchup-source", "catchup-days", "catchup-correction",
            //    "provider", "provider-type", "provider-logo", "provider-countries", "provider-languages",
            //    "media", "media-dir", "media-size"};

            for (int i = 0; i < ColTypes.Length; i++)
            {
                if (fullstr.ContainsElement(ColTypes[i] + "=\"([^\"]*)"))
                {
                    columnList.Add(new ColList
                    {
                        ColName = ColTypes[i],
                        Visible = true,
                        LineType = 0
                    });
                }
            }

           // string[] VlcoptTypes = { ":http-referrer", ":http-user-agent" };

            for (int i = 0; i < myClass.KodiReferrerTypes.Length; i++)
            {
                if (fullstr.ContainsElement(myClass.KodiReferrerTypes[i]))
                {
                    columnList.Add(new ColList
                    {
                        ColName = myClass.KodiReferrerTypes[i],
                        Visible = true,
                        LineType = 1
                    });
                }

            }

            //Issue #14
            //# KODIPROP:inputstream=inputstream.adaptive
            //# KODIPROP:inputstream.adaptive.manifest_type=mpd
            //# KODIPROP:inputstream.adaptive.license_type=com.widevine.alpha
            //# KODIPROP:inputstream.adaptive.license_key=https://drm.ors.at/acq

          //  string[] KodiPropTypes = { "inputstream=", "manifest_type=", "license_type=", "license_key=" };

            for (int i = 0; i < myClass.KodiPropTypes.Length; i++)
            {
                if (fullstr.ContainsElement(myClass.KodiPropTypes[i]))
                {
                    columnList.Add(new ColList
                    {
                        ColName = myClass.KodiPropTypes[i],
                        Visible = true, 
                        LineType = 2
                    });
                }

            }


            return columnList;
        }

        private static bool IsReferrerCol(List<ColList> colList)
        {
            foreach (ColList col in colList)
            {
                if (col.ColName == ":http - referrer" && col.Visible) return true;
            }
            return false;
        }

        private static bool ContainsElement(this string input, string regString)
        {
            var match = Regex.Match(input, regString);

            if (match.Success) return true;

            return false;
        }


        /// <summary>
        /// Search string with Regex
        /// </summary>
        /// <param name="source">string</param>
        /// <param name="regstring">regex</param>
        /// <returns>first result string</returns>
        public static string ScrapHtml(string source, string regstring)
        {
            Regex regex = new Regex(regstring);
            var t = regex.Match(source);
            return t.Groups[1].ToString();
        }

        public static string CleanTVHeadendLink(string link)
        {
            //pipe://ffmpeg -loglevel fatal -i https://mcdn.br.de/br/fs/ard_alpha/hls/de/master.m3u8 -vcodec copy -acodec copy -me

            if (link.StartsWith("pipe"))
            {

                string link1 = ScrapHtml(link, "(https?:\\/\\/.*)");

                link1 = link1.Split(' ').First();

                //string link1 = ClassHelp.GetPartString(link, " http", ".m3u ");

                //if (link1 == null || link1.Length == 0)
                //{
                //    link1 = "http" + ClassHelp.GetPartString(link, " http", ".m3u8 ") + ".m3u8";

                //}
                //else
                //{
                //    link1 = "http" + link1 + ".m3u";
                //}
                return link1;

            }

            return link;

        }

        //here new methods
    }
}