﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using static PlaylistEditor.ClassDataset;

namespace PlaylistEditor
{
    internal class ClassWriteData
    {
        private static bool IsReferrerCol;
        private static bool IsKodiPropCol;

        //loop through all linetypes 0
        //add Name2 -> #EXTINF:-1 
        //loop through linetype 1 -> #EXTVLCOPT
        //loop through linetype 2 -> #KODIPROP
        // add link


        public static void Write(DataTable dt, List<ColList> colList, string filename, string fileHeader, Encoding encoding) 
        {
            var myClass = new ClassDataset();

            Test4SpecialColumns(colList); //set flags for special columns  //test for linetype 1 and 2 

            if ((Path.GetExtension(filename) == ".m3u8" && encoding.BodyName != "utf-8") || encoding == null) 
            {
               // dt = Convert2UTF8(dt);
                encoding = new UTF8Encoding(true); 
            }

            using (StreamWriter file = new StreamWriter(filename, false, encoding))   //false: file ovewrite
            {
                file.NewLine = "\n";  // win: LF
                
                /// file.WriteLine("#EXTM3U");
                file.WriteLine(fileHeader);

                string writestring = ""; 

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    writestring = "#EXTINF:-1 ";

                    for (int j = 0; j < colList.Count - 2; j++)
                    {
                        if (colList[j].LineType == 0 && colList[j].Visible 
                            && !string.IsNullOrEmpty(dt.Rows[i].Field<string>(colList[j].ColName)) )
                        {
                            writestring += " " + colList[j].ColName + "=\"" + dt.Rows[i][j] + "\"";
                        }
                    }

                    ///Field extension method which also supports nullable types:
                    ///write Name2
                    writestring += "," + dt.Rows[i][dt.Columns.Count - 2];  //Name2

                    file.WriteLine(writestring);


                    ///write the Referrer lines
                    if (IsReferrerCol)
                    {
                        for (int j = 0; j < colList.Count - 2; j++)
                        {
                            if (colList[j].LineType == 1 && colList[j].Visible
                                && !string.IsNullOrEmpty(dt.Rows[i].Field<string>(colList[j].ColName)))
                            {
                                file.WriteLine(myClass.WriteExtraLines[colList[j].ColName] + dt.Rows[i][j]);
                            }
                        }

                    }

                    ///write the KODIPROP lines
                    if (IsKodiPropCol)
                    {
                        for (int j = 0; j < colList.Count - 2; j++)
                        {
                            if (colList[j].LineType == 2 && colList[j].Visible
                                && !string.IsNullOrEmpty(dt.Rows[i].Field<string>(colList[j].ColName)))
                            {
                                file.WriteLine(myClass.WriteExtraLines[colList[j].ColName] + dt.Rows[i][j]);
                            }
                        }

                    }

                    ///write link line
                    file.WriteLine(dt.Rows[i][dt.Columns.Count - 1]);


                    //switch (filterIndex)
                    //{
                    //    case 1:
                    //    case 2:
                    //        file.WriteLine(dt.Rows[i][dt.Columns.Count - 1]);
                    //        break;

                    //    case 3:
                    //    case 4:
                    //        file.WriteLine(AddTVHeadendLink(dt.Rows[i][dt.Columns.Count - 1].ToString()));
                    //       // file.WriteLine(ClassHelp.CleanTVHeadendLink(dt.Rows[i][dt.Columns.Count - 1].ToString()));
                    //        break;
                    //}


                }
            }

        }

        /// <summary>
        /// set flags for special lines
        /// </summary>
        /// <param name="colList"></param>
        private static void Test4SpecialColumns(List<ColList> colList)
        {
            foreach (var item in colList)
            {
                if (item.Visible && item.LineType == 1) IsReferrerCol = true;
                if (item.Visible && item.LineType == 2) IsKodiPropCol = true;
            }
           
        }

        private static DataTable Convert2UTF8(DataTable dt)
        {
            foreach (DataColumn column in dt.Columns)
            {
                for (int row = 0; row < dt.Rows.Count; row++)
                {
                    //string mastercell = dt.Rows[row].Field<string>(column);
                    dt.Rows[row][column] = dt.Rows[row].Field<string>(column).ToUTF8();

                    //dt.Rows[row][column]=
                    //Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(dt.Rows[row].Field<string>(column)));

                    //foreach (DataRow row in dt.Rows)
                    //{
                    //  //  string mastercell = dt.Rows[row].Field<string>(column);

                    //}
                }
            }

            return dt;
        }
        private string ConvertStringToUtf8Bom(string source)
        {
            var data = Encoding.UTF8.GetBytes(source);
            var result = Encoding.UTF8.GetPreamble().Concat(data).ToArray();
            var encoder = new UTF8Encoding(true);

            return encoder.GetString(result);
        }

        private static string AddTVHeadendLink(string link)
        {
            return Form1.tvhead1 + link + Form1.tvhead2;
        }


    }

    public static class StringExtensions
    {
        public static string ToUTF8(this string text)
        {
            var data = Encoding.UTF8.GetBytes(text);
            var result = Encoding.UTF8.GetPreamble().Concat(data).ToArray();
            var encoder = new UTF8Encoding(true);

            return encoder.GetString(result);

          //  return Encoding.UTF8.GetString(Encoding.Default.GetBytes(text));
        }
    }


}
