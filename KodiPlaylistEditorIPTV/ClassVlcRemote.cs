﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistEditor
{
    //https://gist.github.com/SamSaffron/101357
    //https://github.com/Igor-Kotv/VlcPlugin
    //https://github.com/UnickSoft/VLCControl
    enum VlcCommand
    {
        Add,
        Enqueue,
        Play,
        Next,
        F,
        Is_Playing,
        Get_Time,
        Seek,
        Pause,
        FastForward,
        Rewind
    }

    //class VLCRemoteController
    //{
    //    public VLCRemoteController()
    //    {
    //        m_client = new TcpClient();
    //    }

    //    ~VLCRemoteController()
    //    {
    //        disconnect();
    //    }

    //    public String getVLCExe()
    //    {
    //        String res = "";
    //        String keyName32 = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\VideoLAN\VLC";
    //        String keyName64 = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\VideoLAN\VLC";

    //        String valueName = "InstallDir";

    //        String location32 = (String)Registry.GetValue(keyName32, valueName, null);
    //        String location64 = (String)Registry.GetValue(keyName64, valueName, null);

    //        if (location32 != null)
    //        {
    //            String vlcexe32 = location32 + "\\vlc.exe";
    //            res = File.Exists(vlcexe32) ? vlcexe32 : "";
    //        }

    //        if (String.IsNullOrEmpty(res) && location64 != null)
    //        {
    //            String vlcexe64 = location64 + "\\vlc.exe";
    //            res = File.Exists(vlcexe64) ? vlcexe64 : "";
    //        }

    //        return res;
    //    }

    //    /**
    //     * Connect to VLC player
    //     * 
    //     * @return: true if success.
    //     * */
    //    public bool connect(String ip, Int32 port)
    //    {
    //        bool res = false;
    //        try
    //        {
    //            m_client = new TcpClient(ip, port);
    //            if (isSocketConnected())
    //            {
    //                m_networkStream = m_client.GetStream();
    //            }
    //        }
    //        catch
    //        {
    //            Console.WriteLine("Error in VLCRemoteController::connect");
    //        }

    //        return res;
    //    }

    //    /**
    //     * Disconnect from VLC player.
    //     * */
    //    public void disconnect()
    //    {
    //        if (isSocketConnected())
    //        {
    //            m_client.Close();
    //            m_networkStream.Close();
    //        }
    //    }

    //    /**
    //     * Send raw command to VLC. This function add \n to the end.
    //     * */
    //    public bool sendCustomCommand(String command)
    //    {
    //        bool res = false;
    //        if (isSocketConnected())
    //        {
    //            Byte[] data = System.Text.Encoding.UTF8.GetBytes(command + "\n");
    //            m_networkStream.Write(data, 0, data.Length);
    //            res = true;
    //        }

    //        return res;
    //    }

    //    /**
    //     * Return current buffer from VLC player.
    //     * */
    //    public String reciveAnswer()
    //    {
    //        String res = String.Empty;
    //        if (isSocketConnected())
    //        {
    //            const int chunkSize = 4048 * 4048 * 4;
    //            Byte[] data = new Byte[chunkSize];

    //            //int readCount;
    //            //while ((readCount = ns.Read(data, 0, client.ReceiveBufferSize)) != 0)
    //            //{
    //            //    dataString.Append(Encoding.UTF8.GetString(data, 0, readCount));
    //            //}

    //            while (m_networkStream.DataAvailable)
    //            {
    //                Int32 readBytes = m_networkStream.Read(data, 0, m_client.ReceiveBufferSize);
    //                if (readBytes > 0)
    //                {
    //                    res = res + System.Text.Encoding.UTF8.GetString(data, 0, readBytes);
    //                }

    //                if (!m_networkStream.DataAvailable)
    //                {
    //                    System.Threading.Thread.Sleep(100);
    //                }
    //            }
    //        }

    //        return res;
    //    }


    //    public bool isSocketConnected()
    //    {
    //        return m_client.Connected;
    //    }

    //    // This is client socket.
    //    private TcpClient m_client;

    //    // Stream for socket.
    //    private NetworkStream m_networkStream;
    //}
    class ClassVlcRemote
    {
        // maximum 2 second wait on results. 
        const int WaitTimeout = 2000;

        static ASCIIEncoding ASCIIEncoding = new ASCIIEncoding();

        Process vlcProcess;
        public TcpClient client;

        static int GetParentProcessId(int Id)
        {
            int parentPid = 0;
            using (ManagementObject mo = new ManagementObject("win32_process.handle='"
            + Id.ToString() + "'"))
            {
                mo.Get();
                parentPid = Convert.ToInt32(mo["ParentProcessId"]);
            }
            return parentPid;
        }


        public void VlcRemote()
        {

            string vlcPath = null;
            var vlcKey = Registry.LocalMachine.OpenSubKey(@"Software\VideoLan\VLC");
            if (vlcKey == null)
            {
                vlcKey = Registry.LocalMachine.OpenSubKey(@"Software\Wow6432Node\VideoLan\VLC");
            }
            if (vlcKey != null)
            {
                vlcPath = vlcKey.GetValue(null) as string;
            }

            if (vlcPath == null)
            {
                throw new ApplicationException("Can not find the VLC executable!");
            }

            var info = new ProcessStartInfo(vlcPath, "-I rc --rc-host=localhost:9876 --rc-quiet");
            vlcProcess = Process.Start(info);
            client = new TcpClient("localhost", 9876);
        }

        public Process VlcPlaybackProcess
        {
            get
            {
                var currentProcessId = Process.GetCurrentProcess().Id;
                Process vlcProcess = null;
                foreach (var process in Process.GetProcessesByName("vlc"))
                {
                    if (GetParentProcessId(process.Id) == currentProcessId)
                    {
                        vlcProcess = process;
                        break;
                    }
                }
                return vlcProcess;
            }
        }

        public void Add(string filename)
        {
            SendCommand(VlcCommand.Add, filename);
        }

        public void Enqueue(string filename)
        {
            SendCommand(VlcCommand.Enqueue, filename);
        }

        public void Play()
        {
            SendCommand(VlcCommand.Play);
        }
        public void Next()
        {
            SendCommand(VlcCommand.Next);
        }

        public void GoToFullScreen()
        {
            SendCommand(VlcCommand.F, "on");
        }

        public bool IsPlaying
        {
            get
            {
                SendCommand(VlcCommand.Is_Playing);
                string result = WaitForResult().Trim();
                return result == "1";
            }
        }

        public int Position
        {
            get
            {
                SendCommand(VlcCommand.Get_Time);
                var result = WaitForResult().Trim();
                return Convert.ToInt32(result);
            }
            set
            {
                SendCommand(VlcCommand.Seek, value.ToString());
            }
        }

        public void FastForward()
        {
            SendCommand(VlcCommand.FastForward);
        }

        public void Rewind()
        {
            SendCommand(VlcCommand.Rewind);
        }

        string WaitForResult()
        {
            string result = "";
            DateTime start = DateTime.Now;
            while ((DateTime.Now - start).TotalMilliseconds < WaitTimeout)
            {
                result = ReadTillEnd();
                if (!string.IsNullOrEmpty(result))
                {
                    break;
                }
            }
            return result;
        }

        void SendCommand(VlcCommand command)
        {
            SendCommand(command, null);
        }

        void SendCommand(VlcCommand command, string param)
        {
            // flush old stuff
            ReadTillEnd();

            string packet = Enum.GetName(typeof(VlcCommand), command).ToLower();
            if (param != null)
            {
                packet += " " + param;
            }
            packet += Environment.NewLine;

            var buffer = ASCIIEncoding.GetBytes(packet);
            client.GetStream().Write(buffer, 0, buffer.Length);
            client.GetStream().Flush();


            Trace.Write(packet);

        }

        public string ReadTillEnd()
        {
            StringBuilder sb = new StringBuilder();
            while (client.GetStream().DataAvailable)
            {
                int b = client.GetStream().ReadByte();
                if (b >= 0)
                {
                    sb.Append((char)b);
                }
                else
                {
                    break;
                }
            }
            return sb.ToString();
        }
    }
}
